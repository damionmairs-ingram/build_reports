					select
						pls3.person_id,
						dbo.future(pls3.end_date) end_date_future
					into #dm_c_period_of_care_ends
					from
						person_legal_statuses pls3
					where
						--CRITERIA: Last Episode in block
						not exists (
							select 'x'
							from
								person_legal_statuses pls4
							where
								pls4.person_id = pls3.person_id
								and
								(pls4.start_date) = (pls3.end_date)
								and
								--CRITERIA: Exclude dodgy data
								-- Some erroneous Legal Statuses start/end the same day: this should not signal a new block
								pls4.id != pls3.id
						)

create index dm_c_period_of_care_ends_idx on #dm_c_period_of_care_ends(person_id, end_date_future)


		select
			details.period_of_care_id,
			details.person_id,
			details.start_date,
			details.end_date,
			details.end_date -- No adjustment as last in block cannot overlap !
		from (
			select
				pls.id period_of_care_id,
				pls.person_id,
				(pls.start_date) start_date,
				(
					-- End date of this block
					-- NB. Cannot use Looked_After_Placements' E% due to dodgy data
					select
						case
							when min(pls3.end_date_future) < dbo.future(null) then
								min(pls3.end_date_future)
						end
					from
						#dm_c_period_of_care_ends pls3
					where
						pls3.person_id = pls.person_id
						and
						--CRITERIA: Ends after current Legal Status started
						pls3.end_date_future > (pls.start_date)
				) end_date
			from
				person_legal_statuses pls
			where
				--CRITERIA: First Episode in block
				not exists (
					select 'x'
					from
						person_legal_statuses pls2
					where
						pls2.person_id = pls.person_id
						and
						(pls2.end_date) = (pls.start_date)
						and
						--CRITERIA: Exclude dodgy data
						-- Some erroneous Legal Statuses start/end the same day: this allows them to start a new block
						pls2.id != pls.id
				)
		) details


