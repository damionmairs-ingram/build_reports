
	exec dbo.drop_object 'pop_child_dm_c_org_relationships', 'procedure'
	go
	create procedure pop_child_dm_c_org_relationships as
	begin
		set nocount on
		
		declare @script varchar(30), @step varchar(30), @error_code int, @row_count int, @error_message varchar(200)
		set @script = 'Populate_Package'
		set @step   = 'dm_c_org_rel_types'
		--
		execute dbo.log_header @script, @step
		--
		-- NB. Process lookup table(s) first
		execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_org_rel_types'
		execute @error_code = dbo.synchronise_table 'dm_c_org_rel_types',
						 	'(select 
							  dbo.append(''REL.'',prt.rel_type) org_rel_type_code
							 ,''FWi Prof Rel'' org_rel_type_source			
							 ,prt.description	description	 			 			
                            				,case when prt.rel_type in (
                                        				select

					                                        rf.mapped_value
					                                        from
					                                        dm_c_filters rf
					                                        where
					                                        rf.filter_name = ''Child Schools''
		                                          	) then ''Y''
		                              			end is_school 
    							,prt.rel_type ref_prof_rel_type_id                             
			                                from prof_relationship_types prt
			                                where org_worker_flag in (''O'', ''B''))'
							
							if @error_code != 0 goto exception

		execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_org_relationships'
		execute @error_code = dbo.synchronise_table 'dm_c_org_relationships',
						 	'(select
							po.id    org_relationship_id 	
			                        	,dbo.no_time(po.start_date) start_date  
			                         	,dbo.no_time(po.end_date) end_date						
							,po.end_date_reason end_date_reason  						
							,po.person_id person_id	  
				       	                ,case when po.organisation_id in 
				                            	( select o.id from organisations o
				                            	inner join worker_roles wr
				                            	on o.id = wr.org_id
				                            	inner join people_workers pw
				                            	on pw.worker_id = wr.worker_id	
				                            	where wr.primary_job = ''Y''
				                            	and pw.type = ''GP'')
				                            then ''Y''			
				                            end is_surgery
							,rt.is_school is_school					 
							,dbo.append(''REL.'',po.type) org_rel_type_code  					       					                         
                            				,po.organisation_id organisation_id		
							from people_organisations po
							inner join dm_c_org_rel_types rt
							on rt.ref_prof_rel_type_id = po.type)'
		
		if @error_code != 0 goto exception
		--
		execute dbo.analyze_tables @script, @step, 'dm_c_org_rel_types, dm_c_org_relationships'
		execute dbo.log_tail @script
		return 0
	exception:
		execute dbo.log_error @SCRIPT, @step, @error_code, @error_message
		-- Abort neatly
		execute dbo.analyze_tables @script, @step, 'dm_c_org_rel_types, dm_c_org_relationships'
		return @error_code
	end
	go
