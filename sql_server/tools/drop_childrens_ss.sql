begin
/*
 * NAME:		drop_childrens_ss.sql
 * DESCRIPTION:	For *DEVELOPER* use only
 *				!!! Do not use on a Customer's database !!!
 * 				Drops all Children's datamart tables, filters, groups and switches
 * HISTORY:     1.0 djm 22/08/2008 FRA-921: initial version
 */
 	set nocount on
	--
	-- Filters -------------------------------------------------------------------
	--
	-- Delete DEV filter mappings
	delete from report_filter_mappings
	where
		report_filter_id in (
			select
				rf.id
			from
				report_filters rf
			where
				upper(rf.description) like '[DEV3.0%'
		)
	--
	-- Delete DEV filters
	delete from report_filters
	where
		upper(description) like '[DEV3.0%'
	--
	-- Delete Filter/Group Mapping views
	delete from report_filter_group_tables
	where
		table_name like 'CHILD%VW'
	--
	-- Groups --------------------------------------------------------------------
	--
	-- Delete DEV group mappings
	delete from report_group_mappings
	where
		report_group_category_id in (
			select
				rgc.id
			from
				report_groups rg
			inner join report_group_categories rgc
			on rgc.report_group_id = rg.id
			where
				upper(rg.description) like '[DEV3.0%'
		)
	--
	-- Delete DEV group categories
	delete from report_group_categories
	where
		report_group_id in (
			select
				rg.id
			from
				report_groups rg
			where
				upper(rg.description) like '[DEV3.0%'
		)
	--
	-- Delete DEV groups
	delete from report_groups
	where
		upper(description) like '[DEV3.0%'
	--
	-- Switches ------------------------------------------------------------------
	--
	-- Switch Mappings
	delete from report_switch_mappings
	where
		switch_name like 'Child %'
	--
	-- Switches
	delete from report_switches
	where
		switch_name like 'Child %'
	--
	-- Objects -------------------------------------------------------------------
	--
	declare c_standard_tables cursor for
		select obj.name
		from
			dbo.sysobjects obj
		where
			substring(obj.name, 1, 5) = 'DM_C_'
			and
			objectproperty(id, N'IsUserTable') = 1
	--
	declare @v_table_name varchar(30)
	--
	open c_standard_tables
	fetch c_standard_tables into @v_table_name
	--
	while @@fetch_status = 0
	begin
		execute('drop table ' + @v_table_name)
		fetch c_standard_tables into @v_table_name
	end
	--
	close c_standard_tables
	deallocate c_standard_tables
	--
	-- Other Objects
	declare c_objects cursor for
		select
			obj.name,
			case
				when objectproperty(id, N'IsUserTable') = 1 then
					'TABLE'
			end object_type
		from
			dbo.sysobjects obj
		where
			(
				obj.name = 'DM_EXTERNAL_EDUCATION' and objectproperty(id, N'IsUserTable') = 1
			)
			or
			(
				obj.name = 'CHILD_EDUCATION_RESULTS_VW' and objectproperty(id, N'IsView') = 1
			)
	--
	declare	@v_object_name	varchar(30),
			@v_object_type	varchar(30)
	--
	open c_objects
	fetch c_objects into @v_object_name, @v_object_type
	--
	while @@fetch_status = 0
	begin
		execute('drop ' + @v_object_type + ' ' + @v_object_name)
		fetch c_objects into @v_object_name, @v_object_type
	end
	--
	close c_objects
	deallocate c_objects
end