exec dbo.drop_object 'p_table_minus', 'procedure'
go
create procedure p_table_minus (
	@p_main_table  varchar(128),
	@p_minus_table varchar(128)) as
/*
 * NAME:		p_table_minus
 * DESCRIPTION:	Returns p_main_table's rows after removing p_minus_table's rows.
 *              Effectively implements 'MINUS' for SQLServer 2000 (SQLServer 2005 has 'EXCEPT' so does not require this)
 *              NB. Tables must have same column names/types
 * HISTORY:		1.0  djm  30/04/2008	initial version
 */
begin
	set nocount on
	--
	-- Holds details of current column
	declare	@v_column_name    varchar(128),
			@v_data_type      varchar(13), -- 'smalldatetime' is currently longest data_type name
			@v_main_nullable  varchar(3),
			@v_minus_nullable varchar(3)
	--
	-- Lists columns used by both tables
	declare c_columns cursor fast_forward for
		select
			col.column_name,
			col.data_type,
			col.is_nullable,
			col2.is_nullable
		from
			information_schema.columns col
		--JOIN: Equivalent columns on second table
		inner join information_schema.columns col2
		on col2.table_name = @p_minus_table
		and col2.column_name = col.column_name
		and col2.data_type = col.data_type
		where
			--CRITERIA: Main table's columns
			col.table_name = @p_main_table --'RAP_REFERRALS'
			and
			--CRITERIA: Restrict to handled data_types (can be extended if required)
			col.data_type in ('char', 'varchar', 'nchar', 'nvarchar', 'datetime', 'smalldatetime', 'numeric', 'int')
		order by
			col.ordinal_position
	--
	declare	@v_whole_sql         varchar(8000),
			-- Builds different parts of the SQL
			@v_select_clause_sql varchar(4000),
			@v_where_clause_sql  varchar(4000),
			-- Temporary SQL to build current expression
			@v_main_expression   varchar(128),
			@v_minus_expression  varchar(128),
			-- Constants holding table prefixes
			@MAIN_PREFIX         varchar(4),
			@MINUS_PREFIX        varchar(4),
			-- Constant for Carriage Return/Linefeed
			@CRLF                  varchar(2)
	set @MAIN_PREFIX  = 't1'
	set @MINUS_PREFIX = 't2'
	set @CRLF = char(13) + char(10)
	--
	-- Build SQL to compare the tables
	open c_columns
	fetch c_columns into
			@v_column_name,
			@v_data_type,
			@v_main_nullable,
			@v_minus_nullable
	--
	while @@fetch_status = 0
	begin
		-- Add column to select clause
		if @v_select_clause_sql is null
			set @v_select_clause_sql = 'select ' + @v_column_name
		else
			set @v_select_clause_sql = @v_select_clause_sql + ',' + @CRLF + @v_column_name
		--
		-- Build main table's expression
		if @v_main_nullable = 'Yes'
		begin
			if @v_data_type in ('char', 'varchar')
				set @v_main_expression = 'coalesce(' + @MAIN_PREFIX + '.' + @v_column_name + ', ''!"�$%^&*()!"�$%^&*()'')'
			else if @v_data_type in ('nchar', 'nvarchar')
				set @v_main_expression = 'coalesce(' + @MAIN_PREFIX + '.' + @v_column_name + ', n''!"�$%^&*()!"�$%^&*()'')'
			else if @v_data_type in ('datetime', 'smalldatetime')
				set @v_main_expression = 'coalesce(' + @MAIN_PREFIX + '.' + @v_column_name + ', getdate())'
			else if @v_data_type in ('numeric', 'int')
				set @v_main_expression = 'coalesce(' + @MAIN_PREFIX + '.' + @v_column_name + ', -12345678901234567890)'
		end
		else
		begin
			set @v_main_expression = @MAIN_PREFIX + '.' + @v_column_name
		end
		--
		-- Build minus table's expression
		if @v_minus_nullable = 'Yes'
		begin
			if @v_data_type in ('char', 'varchar')
				set @v_minus_expression = 'coalesce(' + @MINUS_PREFIX + '.' + @v_column_name + ', ''!"�$%^&*()!"�$%^&*()'')'
			else if @v_data_type in ('nchar', 'nvarchar')
				set @v_minus_expression = 'coalesce(' + @MINUS_PREFIX + '.' + @v_column_name + ', n''!"�$%^&*()!"�$%^&*()'')'
			else if @v_data_type in ('datetime', 'smalldatetime')
				set @v_minus_expression = 'coalesce(' + @MINUS_PREFIX + '.' + @v_column_name + ', getdate())'
			else if @v_data_type in ('numeric', 'int')
				set @v_minus_expression = 'coalesce(' + @MINUS_PREFIX + '.' + @v_column_name + ', -12345678901234567890)'
		end
		else
		begin
			set @v_minus_expression = @MINUS_PREFIX + '.' + @v_column_name
		end
		--
		-- Add column to where clause
		if @v_where_clause_sql is null
		begin
			set @v_where_clause_sql = 'where ' + @v_main_expression + ' = ' + @v_minus_expression + @CRLF
		end
		else
		begin
			set @v_where_clause_sql = @v_where_clause_sql + 'and ' + @v_main_expression + ' = ' + @v_minus_expression + @CRLF
		end
		--
		fetch c_columns into
				@v_column_name,
				@v_data_type,
				@v_main_nullable,
				@v_minus_nullable
	end
	--
	close c_columns
	deallocate c_columns
	--
	-- Move the SQL into a single string, and execute
	set @v_whole_sql = @v_select_clause_sql + ' from ' + @p_main_table + ' ' + @MAIN_PREFIX + ' where not exists (select ''x'' from ' + @p_minus_table + ' ' + @MINUS_PREFIX + ' ' + @v_where_clause_sql + ')'
	set @v_select_clause_sql = null
	set @v_where_clause_sql = null
	exec(@v_whole_sql)
end