-- This is a template of a *simple* SQLServer procedure for Children's Datamart
-- It populates two lookup tables and a detail table
-- Every SQL or procedure call needs to be checked for errors, except for logging
-- Additional steps are required for build tables or external data
--    Build tables should be truncated at the start, populated immediately before the Sync, and then truncated again. They should also be truncated in the Exception handler.
--    External tables typically just require an analyze at the start
-- Examples of both are already available.
--
-- NB. Square-brackets are used to identify bits that definitely need changing
	--
	-- [procedure_name] ------------------------------------------------------
	--
	exec dbo.drop_object 'pop_child_[procedure_name]', 'procedure'
	go
	create procedure pop_child_[procedure_name] as
	begin
		set nocount on
		
		declare @script varchar(30), @step varchar(30), @error_code int, @row_count int, @error_message varchar(200)
		set @script = 'Populate_Package'
		set @step   = '[procedure_name]'
		--
		execute dbo.log_header @script, @step
		execute dbo.drop_table_indexes @script, @step, '[lookup_table_name, lookup_table_name2, detail_table_name]', 'IF_EMPTY'
		--
		-- Example Switch usage, where appropriate
		-- NB. There are various ways to use a Switch: in SQL or if-condition, etc.
		declare	@switch_name  varchar(30),
				@switch_value varchar(30)
		set @switch_name  = '[switch_name]'
		set @switch_value = dbo.switch_mapping(@switch_name)
		execute dbo.log_switch @script, @step, @switch_name, @switch_value
		--
		-- NB. Process lookup table(s) first
		execute dbo.log_line @SCRIPT, @step, 'Synchronise [lookup_table_name]'
		execute @error_code = dbo.synchronise_table '[lookup_table_name]',
						 	'[source_table or (inline_view)]',
							'[filter destination if more than one Sync reqd]',
							'[TEMP only if reqd for performance]'
		if @error_code != 0 goto exception
		--
		execute dbo.log_line @SCRIPT, @step, 'Synchronise [lookup_table_name2]'
		execute @error_code = dbo.synchronise_table '[lookup_table_name2]',
						 	'[source_table or (inline_view)]',
							'[filter destination if more than one Sync reqd]',
							'[TEMP only if reqd for performance]'
		if @error_code != 0 goto exception
		--
		-- NB. Process detail table(s) afterwards, usually using lookups for flags, etc.
		execute dbo.log_line @SCRIPT, @step, 'Synchronise [detail_table_name]'
		execute @error_code = dbo.synchronise_table '[detail_table_name]',
						 	'[source_table or (inline_view)]',
							'[filter destination if more than one Sync reqd]',
							'[TEMP only if reqd for performance]'
		if @error_code != 0 goto exception
		--
		execute dbo.create_table_indexes @script, @step, '[lookup_table_name, lookup_table_name2, detail_table_name]', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, '[lookup_table_name, lookup_table_name2, detail_table_name]'
		execute dbo.log_tail @script
		return 0
	exception:
		execute dbo.log_error @SCRIPT, @step, @error_code, @error_message
		-- Abort neatly
		execute dbo.create_table_indexes @script, @step, '[lookup_table_name, lookup_table_name2, detail_table_name]', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, '[lookup_table_name, lookup_table_name2, detail_table_name]'
		return @error_code
	end
	go

