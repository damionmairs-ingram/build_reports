-- Create dual table
if not exists (select 1 from dbo.sysobjects where id = object_id(N'[dbo].[dual]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	create table dual (dummy varchar(1))
go
if not exists (select 'x' from dual)
	insert into dual (dummy) values ('X')
go
grant select on dbo.dual to fw_role
go
begin
	-- Anonymise_Script_ss.sql
	-- SQLServer Anonymise script
	-- DJM 29/03/2007 Created
	-- DJM 30/03/2007 Only amend where len() > 0
	--                Bugfix: missing 'truncate table' statements
	--                Additional columns to be anonymised
	--                Added dual table
	-- DJM 03/04/2007 Handle Text columns differently
	--                Removed Component_Answers
	-- RA 02/05/2007 further columns added following feedback from Worcs
	-- RA 14/05/2007 further columns added following feedback from Notts
	-- TW 24/07/2008 Amended cursors to only select tables and columns that already exist in database
	--				 Amended to include all DM_ and SSDA903 tables
	--				 Added update to form_answers table
	--				 (NB Documents table anonymisation uses simple update compared to oracle which recreates table. 
	--				  If resource issues that have effected oracle clients start to effect sql server then can amend
	--				  to use oracle method).
	-- MP 02/01/2009 FRA-1952 - Scrambling Personal data from _History Tables
	-- PB 06/04/2009 FRA-3121 - Truncate Report_form_answers_repeat and Lucene_* tables
	--							Disable constraints from and Merge_* tables and Delete
	--							Added '.' delimiter to c_columns cursor
	--							Disabled triggers before Overwriteing columns with '[Text]', since this was causing an error. (re-enable them afterwards)
    -- RW 22/05/2009 FRA-3122 - added table patterns to c_merge cursor: YKAP%, HLP%, FRA%
    --          added DM%, %TEMP, TEMP%, TMP%, %TMP but excluded %template% patterns to c_tables cursor
    --          REG_ENQUIRY_NEW_ENQUIRER, REG_ENQUIRY_SEARCH_CRITERIA and PEOPLE_HISTORY tables added to c_columns cursor
	--          datetime columns (DOB) updated with getdate
	--          fixed c_merge cursor
	-- SF 05/07/2011 FRA-7870: Updating scripts to Frameworki version 4.1.1.1 (numbered script (683)
	-- PB 30/08/2011 FRA-####: Modified to improve performance
	--						   Added additional tables
	--						   split cursor c_columns into 2 since max table limit reached (256 tables in one query)
	-- TW 24/01/2012 FRA-8143: Updating scripts to Frameworki version 4.2.5.0 (numbered script 722)	
	-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	-- !!! Do not run this on a Live database !!!
	-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	--
	
	declare @RowsUpdated bigint, 
			@batchSize bigint, 
			@totalRecords bigint, 
			@BatchCount int, 
			@PrintMessage varchar(4000),
			@Sqlcommand varchar(4000)

	
	-- Remove uploaded documents
	--
	-- Process takes too long in one go, so splitting into seperate sections.
	
	select @totalRecords=Count(1) 
	from documents 
	where image_type != 'Form'	
		and image_data is not null
	--
	if @totalRecords > 0
	begin
		set @batchSize=50000
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Clearing documents, Number of records: ' + cast(@totalRecords as varchar) + ' Batch Size: ' + cast(@batchSize as varchar)
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
		--
		set @RowsUpdated=@batchSize
		set @BatchCount=0
		--
		while  @RowsUpdated = @batchSize
		begin
			begin transaction 
				--
				update top (@batchSize) documents
				set    image_data = null
				where  image_type != 'Form'	
				and 	image_data is not null
				--
				set @RowsUpdated=@@rowcount
				set @BatchCount=@BatchCount+1
				--
			commit;
			set @PrintMessage=cast(getdate() as varchar) + ' Batch ' + cast(@BatchCount as varchar) + ' Completed'
			RAISERROR( @PrintMessage,0,1) WITH NOWAIT
		end
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Documents Cleared'
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
	end
	else
	begin
		--
		set @PrintMessage=cast(getdate() as varchar) + ' No documents to be Deleted.'
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
	end
	--
	-- Tables' columns to be anonymised
	declare c_columns cursor for
		select  obj.name, col.name, typ.name
		from
			sysobjects obj
		--JOIN: Column Details
		inner join syscolumns col
		on col.id = obj.id
		--JOIN: Column Type
		inner join systypes typ
		on typ.xusertype = col.xusertype
		where			
			objectproperty(obj.id, N'IsUserTable') = 1		
			and (obj.name + '.' + col.name) in (
		select 'ADDRESSES' + '.' + 'FLAT_NUMBER' from dual union all
		select 'ADDRESSES' + '.' + 'ACCESS_NOTES' from dual union all
		select 'ADDRESSES' + '.' + 'BUILDING' from dual union all
		select 'ADDRESSES' + '.' + 'STREET' from dual union all
		select 'ADDRESSES' + '.' + 'STREET_NUMBER' from dual union all
		select 'ADDRESSES' + '.' + 'COUNTY' from dual union all
		select 'ADDRESSES' + '.' + 'COUNTRY' from dual union all
		select 'ADDRESSES' + '.' + 'DISTRICT' from dual union all
		select 'ADDRESSES' + '.' + 'TOWN' from dual union all
		select 'ADDRESSES' + '.' + 'POST_CODE' from dual union all
		select 'ADDRESSES' + '.' + 'UPPER_STREET' from dual union all
		select 'ADDRESSES' + '.' + 'UPPER_BUILDING' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'FLAT_NUMBER' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'ACCESS_NOTES' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'BUILDING' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'STREET' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'STREET_NUMBER' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'COUNTY' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'COUNTRY' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'DISTRICT' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'TOWN' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'POST_CODE' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'UPPER_STREET' from dual union all
		select 'ADDRESSES_HISTORY' + '.' + 'UPPER_BUILDING' from dual union all
		select 'ALERTS' + '.' + 'ALERT_TEXT' from dual union all
		select 'CARE_PLAN_TEXT' + '.' + 'LONG_TEXT' from dual union all
		select 'CARERS' + '.' + 'NOTES' from dual union all
		select 'CASE_NOTES' + '.' + 'NOTE' from dual union all
		select 'CASE_NOTES_HISTORY' + '.' + 'NOTE' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS' + '.' + 'COMMENTS' from dual union all
		select 'CONTRACTS_HISTORY' + '.' + 'NAME' from dual union all
		select 'CONTRACTS_HISTORY' + '.' + 'NOTES' from dual union all
		select 'CONTRACTS_HISTORY' + '.' + 'DESCRIPTION' from dual union all
		select 'DOCUMENT_STAGING' + '.' + 'DOCUMENT_FILE' from dual union all				
		select 'EIR2_TEXT' + '.' + 'LONG_TEXT' from dual union all
		select 'EPISODES' + '.' + 'COMMENTS' from dual union all
		select 'EPISODE_PARTICIPANTS' + '.' + 'COMMENTS' from dual union all
		select 'EPISODE_QUESTION_ANSWERS' + '.' + 'TEXT_ANSWER' from dual union all
		select 'EPISODE_REFERRAL_DETAILS' + '.' + 'COMMENTS' from dual union all
		select 'EPISODE_REFERRAL_DETAILS' + '.' + 'REFERRER_ADDRESS' from dual union all
		select 'EPISODE_REFERRAL_DETAILS' + '.' + 'REFERRER_NAME' from dual union all
		select 'EPISODE_TOPICS' + '.' + 'COMMENTS' from dual union all
		select 'EXTERNAL_ACCESS_REQUESTS' + '.' + 'SUBJECT_FIRST_NAME' from dual union all		
		select 'EXTERNAL_ACCESS_REQUESTS' + '.' + 'SUBJECT_LAST_NAME' from dual union all			
		--select 'FORM_ANSWERS' + '.' + 'TEXT_ANSWER' from dual union all
		select 'FORM_ANSWERS_REPEATING' + '.' + 'TEXT_ANSWER' from dual union all
		select 'LONG_TEXTS' + '.' + 'TEXT_DATA' from dual union all
		select 'MERGE_PERSON_FILE_RETENT_INFO' + '.' + 'NOTES' from dual union all		
		select 'ORGANISATIONS' + '.' + 'ORGANISATION_NOTES' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY' + '.' + 'NOTE' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY' + '.' + 'CONSENT_GIVER_NAME' from dual union all
		select 'PEOPLE' + '.' + 'EMAIL_ADDRESS' from dual union all
		select 'PERSON_CAUTIONS' + '.' + 'OFFENCE_TEXT' from dual union all
		select 'PERSON_CAUTIONS' + '.' + 'OUTCOME_TEXT' from dual union all
		select 'PERSON_FILE_RETENTION_INFO' + '.' + 'NOTES' from dual union all		
		select 'PERSON_FILE_RETENTION_HISTORY' + '.' + 'NOTES' from dual union all				
		select 'PERSON_NAMES' + '.' + 'FIRST_NAMES' from dual union all
		select 'PERSON_NAMES' + '.' + 'LAST_NAME' from dual union all
		select 'PERSON_NAMES' + '.' + 'MATCH_FIRST_NAMES' from dual union all
		select 'PERSON_NAMES' + '.' + 'MATCH_LAST_NAME' from dual union all
		select 'PERSON_NOTES' + '.' + 'NOTE' from dual union all
		select 'PERSON_NAMES_HISTORY' + '.' + 'FIRST_NAMES' from dual union all
		select 'PERSON_NAMES_HISTORY' + '.' + 'LAST_NAME' from dual union all
		select 'PERSON_NAMES_HISTORY' + '.' + 'MATCH_FIRST_NAMES' from dual union all
		select 'PERSON_NAMES_HISTORY' + '.' + 'MATCH_LAST_NAME' from dual union all
		select 'REGISTER_ENQUIRIES' + '.' + 'NOTES' from dual union all
		-- added by RA 02/05/2007
		select 'ORGANISATIONS' + '.' + 'NAME' from dual union all
		select 'ORGANISATIONS' + '.' + 'EMAIL_ADDRESS' from dual union all
		select 'ORGANISATIONS' + '.' + 'WEB_ADDRESS' from dual union all
		select 'ORGANISATIONS' + '.' + 'DESCRIPTION' from dual union all
		select 'ORGANISATION_REFERENCES' + '.' + 'REFERENCE' from dual union all
		select 'COST_CODE_NAMES' + '.' + 'DESCRIPTION' from dual union all
		select 'TELEPHONE_NUMBERS' + '.' + 'PHONE_NUMBER' from dual union all
		select 'PERSON_REFERENCES' + '.' + 'REFERENCE' from dual union all
		select 'PERSON_REFERENCES_HISTORY' + '.' + 'REFERENCE' from dual union all
		select 'EPISODE_OUTCOMES' + '.' + 'NOTE' from dual union all
		select 'EPISODE_HISTORY' + '.' + 'NOTES' from dual union all
		select 'WORKERS' + '.' + 'FIRST_NAMES' from dual union all
		select 'WORKERS' + '.' + 'LAST_NAMES' from dual union all
		select 'WORKERS' + '.' + 'EMAIL_ADDRESS' from dual union all
		select 'ELEMENT_DETAILS' + '.' + 'NOTE' from dual union all
		select 'ELEMENT_CHANGES' + '.' + 'NOTE' from dual union all
		select 'PERSON_HEALTH_INTERVENTIONS' + '.' + 'NOTES' from dual union all
		select 'PERSON_HOSPITAL_STAYS' + '.' + 'CONSULTANT' from dual union all
		select 'PERSON_HOSPITAL_STAYS' + '.' + 'REASON_TEXT' from dual union all
		--added by RA 14/05/2007
		select 'CASE_NOTES' + '.' + 'TITLE' from dual union all
		select 'CASE_NOTES_HISTORY' + '.' + 'TITLE' from dual union all
		--added by PB 06/04/2009 FRA-3121
		select 'CHRONOLOGY_FORM_EVENTS' + '.' + 'TITLE' from dual union all
		select 'CHRONOLOGY_SELECTED_EVENTS' + '.' + 'TITLE' from dual union all
		select 'CHRONOLOGY_SELECTED_EVENTS' + '.' + 'TITLE_FORM_EDIT' from dual union all
		select 'CHRONOLOGY_SELECTED_EVENTS' + '.' + 'NOTES_1' from dual union all
		select 'CHRONOLOGY_SELECTED_EVENTS' + '.' + 'NOTES_2' from dual union all
		select 'CHRONOLOGY_SELECTED_EVENTS' + '.' + 'NOTES_3' from dual union all
		select 'EPISODE_CHRONOLOGY' + '.' + 'CHRONOLOGY_TITLE' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'FIRST_NAMES' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'LAST_NAMES' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'LEGACY_REG_ENQ_FULL_NAME' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'TELEPHONE_NUMBER' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'MOBILE_NUMBER' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'FAX_NUMBER' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'EMAIL_ADDRESS' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER' + '.' + 'NOTES' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'PERSON_FIRST_NAMES' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'PERSON_LAST_NAMES' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'PERSON_DOB' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'FLAT_NUMBER' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'HOUSE_NUMBER' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'BUILDING_NAME' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'STREET_NAME' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA' + '.' + 'POST_CODE' from dual union all
        select 'PEOPLE_HISTORY' + '.' + 'EMAIL_ADDRESS' from dual union all
        select 'CONTACT_POINT_AUTHN_IDPS' + '.' + 'NAME' from dual union all
        select 'CONTACT_POINT_AUTHN_IDPS' + '.' + 'ISSUER' from dual union all
        select 'PARTNERSHIP_CONSENT' + '.' + 'NOTE' from dual union all
        select 'EXTERNAL_SYSTEM_NFTADT' + '.' + 'TEXT' from dual union all
        select 'EXTERNAL_SYSTEM_NFTADT_HISTORY' + '.' + 'TEXT' from dual union all
        select 'PARTNERSHIP_CONSENT_HISTORY' + '.' + 'LOCATION' from dual union all
        select 'TOPIC_SUBSCRIBERS' + '.' + 'SUBSCRIBER_NAME' from dual union all
        select 'TOPIC_SUBSCRIBERS' + '.' + 'SUBSCRIBER_DESCRIPTION' from dual union all
        select 'DISCLOSURE_AUTHORITY' + '.' + 'DISCLOSURE_REASON' from dual union all
        select 'PERSON_PERMISSIONS_HISTORY' + '.' + 'REASON' from dual union all
        select 'PACKAGE_BUDGET_ELEMENTS' + '.' + 'NOTE' from dual union all
        select 'PACKAGE_BUDGET_ELEMENT_CHANGES' + '.' + 'NOTE' from dual union all
        select 'EPISODE_TASKS' + '.' + 'ASSIGN_NOTES' from dual union all
        select 'EPISODE_TASKS' + '.' + 'ISSUE_NOTES' from dual union all
        select 'EPISODE_TASKS' + '.' + 'RETURN_NOTES' from dual union all
        select 'PACKAGE_AUTHORISATION_TASKS' + '.' + 'REQUEST_NOTE' from dual union all
        select 'PACKAGE_AUTHORISATION_TASKS' + '.' + 'AUTHORISATION_NOTE' from dual union all
        select 'CONFERENCE_ATTENDEES' + '.' + 'LOCAL_NAME' from dual union all 
        select 'CONFERENCE_ATTENDEES' + '.' + 'LOCAL_ADDRESS' from dual union all
        select 'CONFERENCE_ATTENDEES' + '.' + 'LOCAL_PHONE' from dual union all
        select 'ESAP_ASSESSMENTS_CONTENT' + '.' + 'CONTENT' from dual union all
        select 'SUBJECT_WARNING_FLAG' + '.' + 'CONTACT_PERSON' from dual union all
        select 'SUBJECT_WARNING_FLAG' + '.' + 'CONTACT_PHONE_NUMBER' from dual union all
        select 'SUBJECT_WARNING_FLAG' + '.' + 'CONTACT_EMAIL' from dual union all
        select 'SUBJECT_WARNING_FLAG' + '.' + 'CONTACT_AGENCY' from dual union all
        select 'SUBJECT_WARNING_FLAG' + '.' + 'WARNING_GUIDANCE' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY' + '.' + 'CONTACT_PERSON' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY' + '.' + 'CONTACT_PHONE_NUMBER' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY' + '.' + 'CONTACT_EMAIL' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY' + '.' + 'CONTACT_AGENCY' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY' + '.' + 'WARNING_GUIDANCE' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR' + '.' + 'MESSAGEHEADER1' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR' + '.' + 'MESSAGEHEADER2' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR' + '.' + 'MESSAGEINSTRUCTION' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR' + '.' + 'STANDBYAGENCY' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR' + '.' + 'STANDBYNUMBER' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY' + '.' + 'MESSAGEHEADER1' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY' + '.' + 'MESSAGEHEADER2' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY' + '.' + 'MESSAGEINSTRUCTION' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY' + '.' + 'STANDBYAGENCY' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY' + '.' + 'STANDBYNUMBER' from dual union all
        select 'PARTNERSHIP_PROCESS_LOG' + '.' + 'PROFESSIONAL_NAME' from dual union all
        select 'TELEPHONE_NUMBERS_HISTORY' + '.' + 'PHONE_NUMBER' from dual union all
        -- Additional Fields
        select 'CARER_UNAVAILABILITIES' + '.' + 'NOTES' from dual union all
		select 'CARE_PLANS' + '.' + 'CHILD_ADDRESS1' from dual union all
		select 'CARE_PLANS' + '.' + 'CHILD_ADDRESS2' from dual union all
		select 'CARE_PLANS' + '.' + 'CHILD_ADDRESS3' from dual union all
		select 'CARE_PLANS' + '.' + 'CHILD_ADDRESS4' from dual union all
		select 'CARE_PLANS' + '.' + 'CHILD_PHONE' from dual union all
		select 'CARE_PLANS' + '.' + 'CONTINGENCY_PLAN' from dual union all
		select 'CARE_PLANS' + '.' + 'FAMILY_NAME' from dual union all
		select 'CARE_PLANS' + '.' + 'FORENAMES' from dual union all
		select 'CARE_PLANS' + '.' + 'OTHER_TEXT' from dual union all
		select 'CARE_PLANS' + '.' + 'PRINCIPAL_CARER_NAME' from dual union all
		select 'CARE_PLANS' + '.' + 'PRINCIPAL_CARER_RELATIONSHIP' from dual union all
		select 'CARE_PLANS' + '.' + 'REVIEW_ADDRESS1' from dual union all
		select 'CARE_PLANS' + '.' + 'REVIEW_ADDRESS2' from dual union all
		select 'CARE_PLANS' + '.' + 'REVIEW_ADDRESS3' from dual union all
		select 'CARE_PLANS' + '.' + 'REVIEW_ADDRESS4' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_TEAM_MANAGER_NAME' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_WORKER_ADDRESS1' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_WORKER_ADDRESS2' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_WORKER_ADDRESS3' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_WORKER_ADDRESS4' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_WORKER_NAME' from dual union all
		select 'CARE_PLANS' + '.' + 'SA_WORKER_PHONE' from dual union all
		select 'CARE_PLANS' + '.' + 'TEAM_MANAGER_NAME' from dual union all
		select 'CARE_PLANS' + '.' + 'WORKER_ADDRESS1' from dual union all
		select 'CARE_PLANS' + '.' + 'WORKER_ADDRESS2' from dual union all
		select 'CARE_PLANS' + '.' + 'WORKER_ADDRESS3' from dual union all
		select 'CARE_PLANS' + '.' + 'WORKER_ADDRESS4' from dual union all
		select 'CARE_PLANS' + '.' + 'WORKER_NAME' from dual union all
		select 'CARE_PLANS' + '.' + 'WORKER_PHONE' from dual union all
		select 'CGM_PARTICIPANTS' + '.' + 'COMMENTS' from dual union all
		select 'CGM_PARTICIPANTS' + '.' + 'LOCAL_ADDRESS' from dual union all
		select 'CGM_PARTICIPANTS' + '.' + 'LOCAL_NAME' from dual union all
		select 'CGM_PARTICIPANTS' + '.' + 'LOCAL_PHONE' from dual union all
		select 'CGM_PARTICIPANT_ACTIONS' + '.' + 'FINDINGS' from dual union all
		select 'CGM_PARTICIPANT_ACTIONS' + '.' + 'FURTHER_ACTION' from dual union all
		select 'CONFERENCES' + '.' + 'COMMENTS' from dual union all
		select 'CONFERENCES' + '.' + 'LOCATION' from dual union all
		select 'CONFERENCE_ATTENDEES' + '.' + 'COMMENTS' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS' + '.' + 'LOCAL_ADDRESS' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS' + '.' + 'LOCAL_NAME' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS' + '.' + 'LOCAL_PHONE' from dual union all
		select 'CONTRACTS' + '.' + 'DESCRIPTION' from dual union all
		select 'CONTRACTS' + '.' + 'NOTE' from dual union all
		select 'CONTRACTS' + '.' + 'TITLE' from dual union all
		select 'CORE_GROUP_MEETINGS' + '.' + 'MEETING_SUMMARY' from dual union all
		select 'CORE_GROUP_MEETINGS' + '.' + 'REVIEW_OF_PREVIOUS_MEETING' from dual union all
		select 'CORE_GROUP_VISITS' + '.' + 'ACTION_ARISING' from dual union all
		select 'CORE_GROUP_VISITS' + '.' + 'VISIT_SUMMARY' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS' + '.' + 'COMMENTS' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS' + '.' + 'LOCAL_ADDRESS' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS' + '.' + 'LOCAL_NAME' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS' + '.' + 'LOCAL_PHONE' from dual union all
		select 'CORE_GROUP_VISIT_SUBJECTS' + '.' + 'NOTE' from dual union all
		select 'EPISODE_ACTIONS' + '.' + 'LOCAL_ADDRESS' from dual union all
		select 'EPISODE_ACTIONS' + '.' + 'LOCAL_NAME' from dual union all
		select 'EPISODE_ACTIONS' + '.' + 'LOCAL_PHONE' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS' + '.' + 'DESIRED_OUTCOME' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS' + '.' + 'HOW_MET' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS' + '.' + 'IDENTIFIED_NEED' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS' + '.' + 'SERVICE_PROVIDED' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS' + '.' + 'WHO_RESPONSIBLE' from dual union all
		select 'EPISODE_ACTION_PLAN_REVIEWS' + '.' + 'FURTHER_SERVICES_TEXT' from dual union all
		select 'EPISODE_ACTION_PLAN_REVIEWS' + '.' + 'OUTCOME_TEXT' from dual union all
		select 'EPISODE_CONF_INVITEES' + '.' + 'ADDRESS' from dual union all
		select 'EPISODE_CONF_INVITEES' + '.' + 'NAME' from dual union all
		select 'EPISODE_CONF_INVITEES' + '.' + 'PHONE' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'FAMILY' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'HOME_ADDRESS' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'KEY_WORKER' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'RETURN_EXT' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'RETURN_NAME' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'SOURCE_CONCERNS' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'TEAM' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'VENUE' from dual union all
		select 'EPISODE_CONF_INV_CLISTS' + '.' + 'WORKER_PHONE' from dual 
		)
		
		
		declare c_columns2 cursor for
		select  obj.name, col.name, typ.name
		from
			sysobjects obj
		--JOIN: Column Details
		inner join syscolumns col
		on col.id = obj.id
		--JOIN: Column Type
		inner join systypes typ
		on typ.xusertype = col.xusertype
		where			
			objectproperty(obj.id, N'IsUserTable') = 1		
			and (obj.name + '.' + col.name) in (
		select 'EPISODE_IA_ELEMENTS' + '.' + 'ELEMENT_TEXT' from dual union all
		select 'EPISODE_OUTCOME_RULES' + '.' + 'DESCRIPTION' from dual union all
		select 'EPISODE_OUTCOME_TASKS' + '.' + 'ISSUE_NOTES' from dual union all
		select 'EPISODE_PARTICIPANTS' + '.' + 'LOCAL_PHONE' from dual union all
		select 'EPISODE_PARTICIPANT_ACTIONS' + '.' + 'FINDINGS' from dual union all
		select 'EPISODE_PARTICIPANT_ACTIONS' + '.' + 'FURTHER_ACTION' from dual union all
		select 'EPISODE_PARTICIPANT_PLANS' + '.' + 'ACTUAL_OUTCOME' from dual union all
		select 'EPISODE_PARTICIPANT_PLANS' + '.' + 'PLANNED_OUTCOME' from dual union all
		select 'EPISODE_PARTICIPANT_PLANS' + '.' + 'RESPONSE_TO_NEED' from dual union all
		select 'EPISODE_REFERRAL_DETAILS' + '.' + 'REFERRER_PHONE' from dual union all
		select 'EPISODE_S47_OUTCOMES' + '.' + 'ACTION_REASONS' from dual union all
		select 'EPISODE_S47_OUTCOMES' + '.' + 'DECISION_REASONS' from dual union all
		select 'EPISODE_SERVICE_TASKS' + '.' + 'NOTE' from dual union all
		select 'EPISODE_SERV_TASK_ALLOWANCES' + '.' + 'NOTES' from dual union all
		select 'EPISODE_STEPS' + '.' + 'COMMENTS' from dual union all
		select 'EPISODE_STEP_HISTORY' + '.' + 'NOTES' from dual union all
		select 'EPISODE_STEP_OUTCOMES' + '.' + 'NOTES' from dual union all
		select 'EPISODE_STRATEGY_DISCUSSIONS' + '.' + 'DECISION_REASON' from dual union all
		select 'EPISODE_STRATEGY_DISCUSSIONS' + '.' + 'PURPOSE' from dual union all
		select 'EPISODE_TASK_AUTHS' + '.' + 'BUDGET_CODES' from dual union all
		select 'EPISODE_TASK_AUTHS' + '.' + 'ISSUE_NOTES' from dual union all
		select 'EPISODE_TASK_AUTHS' + '.' + 'RETURN_NOTES' from dual union all
		select 'EPISODE_VISITS' + '.' + 'NOTES' from dual union all
		select 'INVOICES' + '.' + 'NOTE' from dual union all
		select 'LAC_CAUTIONS' + '.' + 'OFFENCE_TEXT' from dual union all
		select 'LAC_CAUTIONS' + '.' + 'ORG_NAME' from dual union all
		select 'LAC_CAUTIONS' + '.' + 'OUTCOME_DESC' from dual union all
		select 'LAC_CAUTIONS' + '.' + 'OUTCOME_TEXT' from dual union all
		select 'LAC_HEALTH' + '.' + 'NOTES' from dual union all
		select 'LAC_PLACEMENT_CHILDREN' + '.' + 'NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_ADDRESS1' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_ADDRESS2' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_ADDRESS3' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_ADDRESS4' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARER_RELATIONSHIP' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CARE_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_1_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_1_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_2_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_2_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_3_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_3_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_ED_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_ED_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_HS_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'CONS_HS_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'DISC_OTHER_TEXT' from dual union all
		select 'LAC_REVIEWS' + '.' + 'FAMILY_NAME' from dual union all
		select 'LAC_REVIEWS' + '.' + 'FORENAMES' from dual union all
		select 'LAC_REVIEWS' + '.' + 'NEXT_ADDRESS1' from dual union all
		select 'LAC_REVIEWS' + '.' + 'NEXT_ADDRESS2' from dual union all
		select 'LAC_REVIEWS' + '.' + 'NEXT_ADDRESS3' from dual union all
		select 'LAC_REVIEWS' + '.' + 'NEXT_ADDRESS4' from dual union all
		select 'LAC_REVIEWS' + '.' + 'NEXT_PHONE' from dual union all
		select 'LAC_REVIEWS' + '.' + 'OVERALL_PLAN' from dual union all
		select 'LAC_REVIEW_CHILD_VISITS' + '.' + 'PERSONS_SEEN' from dual union all
		select 'LAC_REVIEW_INVITEES' + '.' + 'NAME' from dual union all
		select 'LAC_REVIEW_PARENT_VISITS' + '.' + 'OTHER_TEXT' from dual union all
		select 'LAC_REVIEW_TEXT' + '.' + 'LONG_TEXT' from dual union all
		select 'LAC_SCHOOLS' + '.' + 'SCHOOL' from dual union all
		select 'ORGANISATIONS' + '.' + 'DEPARTMENT' from dual union all
		select 'PACKAGE_SERVICE_DETAILS' + '.' + 'NOTE' from dual union all
		select 'PARTNERSHIP_CONSENT' + '.' + 'CONSENT_GIVER_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT' + '.' + 'FORM_FILE_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT' + '.' + 'LOCATION' from dual union all
		select 'PARTNERSHIP_CONSENT' + '.' + 'PROFESSIONAL_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT_FORM' + '.' + 'FORM_DATA' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY' + '.' + 'FORM_FILE_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY' + '.' + 'PROFESSIONAL_NAME' from dual union all
		select 'PAYMENT_CYCLE_REPORTS' + '.' + 'EMAIL_ADDRESS' from dual union all
		select 'PAYMENT_CYCLE_REPORTS' + '.' + 'FAX_NUMBER' from dual union all
		select 'PERSON_NAMES_HISTORY' + '.' + 'SOUNDEX_LAST_NAME' from dual union all
		select 'PERSON_NON_LA_LEGAL_STATUSES' + '.' + 'NOTES' from dual union all
		select 'PERSON_SCHOOL_REPORTS' + '.' + 'NOTES' from dual union all
		select 'PERSON_SERVICE_TASKS' + '.' + 'NOTE' from dual union all
		select 'PERSON_SERV_TASK_ALLOWANCES' + '.' + 'NOTES' from dual union all
		select 'PERSON_VISITS' + '.' + 'NOTES' from dual union all
		select 'QAF_ASSESSMENTS' + '.' + 'NOTES' from dual union all
		select 'QAF_OBJECTIVES' + '.' + 'DESCRIPTION' from dual union all
		select 'QAF_SUBMISSIONS' + '.' + 'NOTES' from dual union all
		select 'RA_ACKNOWLEDGEMENTS' + '.' + 'NOTE' from dual union all
		select 'REGISTER_ENQUIRIES' + '.' + 'LOCAL_ADDRESS' from dual union all
		select 'REGISTER_ENQUIRIES' + '.' + 'LOCAL_NAME' from dual union all
		select 'REGISTER_ENQUIRIES' + '.' + 'LOCAL_PHONE' from dual union all
		select 'SUBJECT_WARNING_FLAG_EXT_ATTR' + '.' + 'STANDBYPHONENUMBER' from dual
)

		
	--
	declare @v_table_name  sysname,
			@v_column_name sysname,
			@v_column_type sysname
	--
	open c_columns
	fetch c_columns into @v_table_name, @v_column_name, @v_column_type
	while @@fetch_status = 0
	begin
		set @PrintMessage=cast(getdate() as varchar) + '  Anonymising ' + 	@v_table_name + '.' + @v_column_name
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
		--
		execute('ALTER TABLE ' + @v_table_name + ' DISABLE TRIGGER ALL')
		--
		if @v_column_type in ('text', 'ntext')
		begin
			-- Overwrite column with '[Text]'
			--print 'update ' + @v_table_name + ' set ' + @v_column_name + ' = ''[Text]'' where ' + @v_column_name + ' is not null'
			execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = ''[Text]'' where ' + @v_column_name + ' is not null')
		end
		else if @v_column_type in ('datetime')
        begin
            execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = getdate() where ' + @v_column_name + ' is not null')
        end
        else if @v_column_type in ('varbinary')
        begin
            execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = null where ' + @v_column_name + ' is not null')
        end
        else
		begin
			-- Overwrite column with last letter of each value
			--print 'update ' + @v_table_name + ' set ' + @v_column_name + ' = ' + ' replicate(upper(substring(' + @v_column_name + '+ len(' + @v_column_name + ')+ 1))+ len(' + @v_column_name + ')) where len(' + @v_column_name + ') > 1'
			execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = ' + ' replicate(upper(substring(' + @v_column_name + ', len(' + @v_column_name + '), 1)), len(' + @v_column_name + ')) where len(' + @v_column_name + ') > 1')
		end
		
		execute('ALTER TABLE ' + @v_table_name + ' ENABLE TRIGGER ALL')
		--
		fetch c_columns into @v_table_name, @v_column_name, @v_column_type
	end
	--
	close c_columns
	deallocate c_columns
	
	--
	
	open c_columns2
	fetch c_columns2 into @v_table_name, @v_column_name, @v_column_type
	while @@fetch_status = 0
	begin
		set @PrintMessage=cast(getdate() as varchar) + '  Anonymising ' + 	@v_table_name + '.' + @v_column_name
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
		--
		execute('ALTER TABLE ' + @v_table_name + ' DISABLE TRIGGER ALL')
		--
		if @v_column_type in ('text', 'ntext')
		begin
			-- Overwrite column with '[Text]'
			--print 'update ' + @v_table_name + ' set ' + @v_column_name + ' = ''[Text]'' where ' + @v_column_name + ' is not null'
			execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = ''[Text]'' where ' + @v_column_name + ' is not null')
		end
		else if @v_column_type in ('datetime')
        begin
            execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = getdate() where ' + @v_column_name + ' is not null')
        end
        else if @v_column_type in ('varbinary')
        begin
            execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = null where ' + @v_column_name + ' is not null')
        end
        else
		begin
			-- Overwrite column with last letter of each value
			--print 'update ' + @v_table_name + ' set ' + @v_column_name + ' = ' + ' replicate(upper(substring(' + @v_column_name + '+ len(' + @v_column_name + ')+ 1))+ len(' + @v_column_name + ')) where len(' + @v_column_name + ') > 1'
			execute('update ' + @v_table_name + ' set ' + @v_column_name + ' = ' + ' replicate(upper(substring(' + @v_column_name + ', len(' + @v_column_name + '), 1)), len(' + @v_column_name + ')) where len(' + @v_column_name + ') > 1')
		end
		
		execute('ALTER TABLE ' + @v_table_name + ' ENABLE TRIGGER ALL')
		--
		fetch c_columns2 into @v_table_name, @v_column_name, @v_column_type
	end
	--
	close c_columns2
	deallocate c_columns2

	set @PrintMessage=cast(getdate() as varchar) + ' Anonymising table form_answers'
	RAISERROR( @PrintMessage,0,1) WITH NOWAIT 


select @totalRecords=Count(1) 
	from form_answers
	where
    		--CRITERIA: Text Answer present
    		text_answer is not null
    		and
    		--CRITERIA: Not based on a lookup Component
    		not exists (
    			select 'x'
    			from   section_items sit
    			--JOIN: Component Items
    			inner join items itm
    			on itm.id = sit.item_id
    			and itm.component_id is not null
    			--JOIN: Lookup Answer
    			inner join component_answers can
    			on can.component_id = itm.component_id
    			where
    				sit.id = form_answers.section_item_id
    		)
    		and
    		--CRITERIA: Not based on a lookup Question
    		not exists (
    			select 'x'
    			from   section_items sit
    			--JOIN: Component Items
    			inner join items itm
    			on itm.id = sit.item_id
    			and itm.question_id is not null
    			--JOIN: Lookup Answer
    			inner join question_answers qan
    			on qan.question_id = itm.question_id
    			where
    				sit.id = form_answers.section_item_id
    		)
	--
	if @totalRecords > 0
	begin
		set @batchSize=50000
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Anonymising table form_answers, Number of records: ' + cast(@totalRecords as varchar) + ' Batch Size: ' + cast(@batchSize as varchar)
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
		--
		set @RowsUpdated=@batchSize
		set @BatchCount=0
		--
		--Add check column
		execute('ALTER TABLE form_answers
		ADD anonymised varchar(1)');
		--
		while  @RowsUpdated = @batchSize
		begin
			begin transaction 
				--
				set @sqlcommand = 'update top (' + cast(@batchSize as varchar)+ ') form_answers
				set
    		text_answer = ''[Text]'',
    		anonymised = ''Y''
    	where
    		--CRITERIA: Text Answer present
    		text_answer is not null
    		and
    		anonymised is null
    		and
    		--CRITERIA: Not based on a lookup Component
    		not exists (
    			select ''x''
    			from   section_items sit
    			--JOIN: Component Items
    			inner join items itm
    			on itm.id = sit.item_id
    			and itm.component_id is not null
    			--JOIN: Lookup Answer
    			inner join component_answers can
    			on can.component_id = itm.component_id
    			where
    				sit.id = form_answers.section_item_id
    		)
    		and
    		--CRITERIA: Not based on a lookup Question
    		not exists (
    			select ''x''
    			from   section_items sit
    			--JOIN: Component Items
    			inner join items itm
    			on itm.id = sit.item_id
    			and itm.question_id is not null
    			--JOIN: Lookup Answer
    			inner join question_answers qan
    			on qan.question_id = itm.question_id
    			where
    				sit.id = form_answers.section_item_id
    		)'
				
				execute(@sqlcommand);

				--
				set @RowsUpdated=@@rowcount
				set @BatchCount=@BatchCount+1
				--
			commit;
			set @PrintMessage=cast(getdate() as varchar) + ' Batch ' + cast(@BatchCount as varchar) + ' Completed'
			RAISERROR( @PrintMessage,0,1) WITH NOWAIT
		end
		--
		execute('ALTER TABLE form_answers
		drop column anonymised');
		--
		set @PrintMessage=cast(getdate() as varchar) + ' form_answers anonymised'
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
	end
	else
	begin
		--
		set @PrintMessage=cast(getdate() as varchar) + ' No form_answers to be updated.'
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT
	end

	if exists (
		select 'x'
		from INFORMATION_SCHEMA.TABLES tab
		where
			tab.TABLE_NAME = 'assessment_form_answers'
		)
		begin
			--
			set @PrintMessage=cast(getdate() as varchar) + ' Anonymising assessment_form_answers'
			RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
			--
			update	assessment_form_answers
			set
				old_answer = '[Text]',
				new_answer = '[Text]';
		end
	else
		begin
			set @PrintMessage=cast(getdate() as varchar) + ' Assessment_form_answers not present in database.  Skipping...';
			RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
		end
	--
	set @PrintMessage=cast(getdate() as varchar) + ' Anonymising datamart tables'
	RAISERROR( @PrintMessage,0,1) WITH NOWAIT 	

	-- Truncate datamart tables with personal details
	declare c_tables cursor for
	select obj.name
	from sysobjects obj
	where objectproperty(obj.id, N'IsUserTable') = 1
	and (name like 'DM%'
	or name like 'SSDA903%'
	or name like 'LUCENE_%'
	or name in ('RAP_CLIENTS'
    	        ,'REPORT_FORM_ANSWERS'
    	        ,'REPORT_FORM_ANSWERS_REPEAT'
                ,'CP_REGISTRATION_CHILDREN'
                ,'LAC_ADDRESSES'
                ,'LAC_CARERS'
                ,'RESIDENCE_ORDER_CHILDREN'
                ,'UASC_CHILDREN'
                ,'CHILD_REFERRAL_CHILDREN'
                ,'CHILD_ASSESSMENT_CHILDREN'
                ,'CHILD_S47_ENQUIRIES_CHILDREN'
                ,'CHILD_STRAT_DISC_CHILDREN'
                ,'CHILD_INITIAL_CP_CONF_CHILDREN'
                ,'CHILD_PF1_CHILDREN' 
                ,'SR1_CLIENTS'
                ,'PSSEX1_XSSM_PEOPLE'
                ,'PSSEX1_ACTIVITY_PEOPLE'
                )
    or name like '%TEMP' or
    name like 'TEMP%' or
    name like 'TMP%' or
    name like '%TMP')
    and name not like '%TEMPLATE%'

	open c_tables
	fetch c_tables into @v_table_name
	while @@fetch_status = 0
	begin
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Anonymising table '+@v_table_name
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
		--
		execute('truncate table '+@v_table_name)
		fetch c_tables into @v_table_name		
	end
	close c_tables
	deallocate c_tables
	--
	set @PrintMessage=cast(getdate() as varchar) + ' Anonymising merge tables'
	RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
	--
	-- Truncate All Merge tables
	declare c_merge cursor for
	select obj.name
	from sysobjects obj
	where objectproperty(obj.id, N'IsUserTable') = 1
	and (name like 'Merge_%' or
    name like 'YKAP%' or
    name like '%\_YKAP\_%' escape '\' or
    name like 'HLP%' or
    name like '%\_HLP\_%' escape '\' or
    name like 'FRA%' or
    name like '%\_FRA\_%' escape '\')

--Turn off check constraints
	open c_merge
	fetch c_merge into @v_table_name
	while @@fetch_status = 0
	begin
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Disable Check constraints on ' + @v_table_name
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
		--
		execute('ALTER TABLE '+@v_table_name+' NOCHECK CONSTRAINT ALL')
		fetch c_merge into @v_table_name		
	end
	close c_merge
	--deallocate c_merge

--Truncate Tables	
	open c_merge
	fetch c_merge into @v_table_name
	while @@fetch_status = 0
	begin
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Anonymising table '+@v_table_name
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
		--
		execute('delete from '+@v_table_name)
		fetch c_merge into @v_table_name		
	end
	close c_merge
	--deallocate c_merge
	
--Turn on Check constraints	
	open c_merge
	fetch c_merge into @v_table_name
	while @@fetch_status = 0
	begin
		--
		set @PrintMessage=cast(getdate() as varchar) + ' Enable Check constraints on ' + @v_table_name
		RAISERROR( @PrintMessage,0,1) WITH NOWAIT 
		--
		execute('ALTER TABLE '+@v_table_name+' CHECK CONSTRAINT ALL')
		fetch c_merge into @v_table_name		
	end
	close c_merge
	deallocate c_merge

	set @PrintMessage=cast(getdate() as varchar) + ' Anonymisation complete'
	RAISERROR( @PrintMessage,0,1) WITH NOWAIT 

end

