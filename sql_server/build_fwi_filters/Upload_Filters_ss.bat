@ECHO OFF
REM Upload_Filters_And_Groups_ss.bat
REM DJM 15/05/2006 SQLServer version
REM DJM 29/06/2006 Extra steps for Repository 2.3
REM DJM 30/06/2006 Redesigned for easier maintenance
REM DJM 04/07/2006 Improved OSQL log files
REM DJM 21/08/2006 Added Report_Views
REM DJM 23/08/2006 Added bcp for importing; Removed call to p_populate_report_tables
REM DJM 23/01/2007 Prompt for connection details
REM DJM 23/03/2007 Added p_calculate_future_payments
REM DJM 27/03/2007 Check db_version for Future Payments
REM KT  16/05/2007 Amended to only upload filters and groups
REM KT  04/04/2008 Amended path for 3.0.1 release, only inlcudes filters
REM DJM 01/05/2008 YKAP 21373: Removed hard-coded paths

REM Parse arguments
SET username=%1
SET password=%2
SET host=%3
SET database=%4

ECHO Upload Filters

REM Prompt for any missing connection details
IF "%username%"=="" (
	set /p username=Please enter username, usually FW: 
)
IF "%password%"=="" (
	set /p password=Please enter password: 
)
IF "%host%"=="" (
	set /p host=Please enter server that hosts the database: 
)
IF "%database%"=="" (
	set /p database=Please enter database: 
)

REM Error if any connection details missing
IF "%username%"=="" (
	ECHO Error: No username supplied
	ECHO Usage: Upload_Filters [username] [password] [host] [database]
	PAUSE
	GOTO :EOF
) ELSE IF "%password%"=="" (
	ECHO Error: No password supplied
	ECHO Usage: Upload_Filters [username] [password] [host] [database]
	PAUSE
	GOTO :EOF
) ELSE IF "%host%"=="" (
	ECHO Error: No host name supplied
	ECHO Usage: Upload_Filters [username] [password] [host] [database]
	PAUSE
	GOTO :EOF
) ELSE IF "%database%"=="" (
	ECHO Error: No database name supplied
	ECHO Usage: Upload_Filters [username] [password] [host] [database]
	PAUSE
	GOTO :EOF
)

REM List and abort if any missing files
SET SUCCESS=TRUE

CALL :CHECK_EXISTS "report_filters_ss.ctl"
CALL :CHECK_EXISTS "report_filters.txt"
CALL :CHECK_EXISTS "p_post_import_report_filters_ss.sql"

IF %SUCCESS%==FALSE (
	ECHO Aborted run due to missing files
	ECHO Please copy the missing files and rerun
	PAUSE
	GOTO :EOF
)

osql -U %username% -P %password% -S %host% -d %database% -Q "DELETE FROM imu_report_filters" >report_filters.log
CALL :IMPORT report_filters IMU_REPORT_FILTERS
CALL :SQL p_post_import_report_filters_ss

GOTO :END

REM Check file/directory exists -----------------------
:CHECK_EXISTS
IF NOT EXIST %1% (
	ECHO Error: %~1% does not exist
	SET SUCCESS=FALSE
) ELSE (
	ECHO Found %~1%
)
GOTO :EOF

REM Run SQL script ------------------------------------
:SQL
ECHO Running %~1% in OSQL
SET SQL_FILE="%~1%.sql"
SET LOG_FILE="%~1%.log"
osql -U %username% -P %password% -S %host% -d %database% -n -w 132 -i %SQL_FILE% >%log_file%
GOTO :EOF

REM Run BCP import ------------------------------------
:IMPORT
ECHO Import %~1% via BCP
SET DAT_FILE="%~1%.txt"
SET CTL_FILE="%~1%_ss.ctl"
SET LOG_FILE="%~1%.log"
SET TABLE=%database%.dbo.%~2%
bcp %TABLE% in %DAT_FILE% -S%host% -U%username% -P%password% -f%CTL_FILE% >>%LOG_FILE%
GOTO :EOF

REM End of Upload script ------------------------------
:END
ECHO Please check the logs for errors !
PAUSE
GOTO :EOF


