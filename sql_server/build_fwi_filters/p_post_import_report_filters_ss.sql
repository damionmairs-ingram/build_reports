/*
 * NAME:		p_post_import_report_filters
 * DESCRIPTION:	This procedure should be executed once the baseline report filter data has beeen imported into 
 *				a customer database.
 *				It will populate the report_filters table with any new filters that have been created since the 
 *				last uplaod of mapping filters on the database.
 *				Any mapping data that has been previously entered in the old rap_filter_values table will be 
 *				updated to the new report_filter_mappings table.
 *				Any mapping data that has been previously entered in the new report_filter_mappings table will 
 *				be unaffected.
 * HISTORY:		1.0  kt  26/5/2005	initial version
 * 				1.1  kt  14/2/2006	added updates, now includes document handler filters
 * 				1.2  kt  24/2/2006	insert into report_filters.id from identity column
 * 				1.3  djm 09/01/2007 last line of file may not end with CRLF
 *				1.4  djm 16/11/2007	ykap 18776: Remove extra character from last row of IMU data-file
 *				1.5  djm 01/05/2008	ykap 21373: Appended trailing 'go'
 */


begin

	--Update Report_Filters table
	insert into report_filters	(
								CONTEXT
								,DESCRIPTION
								,DISPLAY_COLUMN
								,FORM_PAGE_NUMBER
								,FORM_TYPE_ID
								,ITEM_ID
								,NAME
								,QUESTION_ID
								,QUESTIONNAIRE_ID
								,SELECTION_COLUMN
								,SELECTION_VALUE
								,SOURCE_COLUMN
								,SOURCE_TABLE
								)
						select
								RF_CONTEXT
								,dbo.f_convert_from_symbols(RF_DESCRIPTION)
								,RF_DISPLAY_COLUMN
								,RF_FORM_PAGE_NUMBER
								,RF_FORM_TYPE_ID
								,RF_ITEM_ID
								,RF_NAME
								,RF_QUESTION_ID
								,RF_QUESTIONNAIRE_ID
								,RF_SELECTION_COLUMN
								,RF_SELECTION_VALUE
								,RF_SOURCE_COLUMN
								,-- Last line of file ends in CRCRLF instead of CRLF !
								case
									when RF_SOURCE_TABLE like '%^%' then
										-- Remove all characters after delimiter
										substring(RF_SOURCE_TABLE, 1, charindex('^', RF_SOURCE_TABLE) - 1)
									else
										-- This line finishes with CRLF
										RF_SOURCE_TABLE
								end
						from
							imu_report_filters
						where 
							rf_name not in
								(
								select name
								from report_filters
								)

	--update 
	update report_filters
	set description =		(
							select distinct dbo.f_convert_from_symbols(rf1.rf_description)
							from imu_report_filters rf1
							where rf1.rf_name = name
							)
	where
		name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			)

	update report_filters 
	set selection_column =	(
							select distinct rf1.rf_selection_column
							from imu_report_filters rf1
							where rf1.rf_name = name
							)
	where
		name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			)

	update report_filters
	set selection_value =	(
							select distinct rf1.rf_selection_value
							from imu_report_filters rf1
							where rf1.rf_name = name
							)
	where
		name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			)
								
 	--Delete data from import table
 	delete from imu_report_filters

end
go

