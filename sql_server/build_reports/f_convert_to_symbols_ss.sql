SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO






ALTER  FUNCTION dbo.[f_convert_to_symbols] (@p_string varchar(6000)) RETURNS VARCHAR(6000)
AS

/* 
* NAME:		f_convert_to_symbols
* DESCRIPTION:	This function replaces characters and formatting from a text string
*				in order to create a valid text file.
*              The following codes are converted
*                  char(10)     char(100)
*                  char(13)     char(201)
*                  char(39)     char(202)
*                  char(9)      char(203)
*                  char(34)     char(204)
*                  char(32)     char(205)
*					chr(38)		chr(206)	
*					chr(94)		chr(207)
*					chr(96)		chr(208)
* FUNCTIONS:	
* HISTORY:		1.0  mb  15/06/04  initial version
*              1.1  kt  02/11/04  amended
*              1.2  kt  11/01/05  repository version (based on Oracle v1.2)
*              1.3  kt  22/05/05  added chr(38), chr(94), chr(96)
*/

BEGIN

declare @v_return  VARCHAR(6000) 

set @v_return =

replace 
(
replace 
	(
	replace 
		(
		replace 
			(
			replace 
				(
				replace 
					(
					replace 
						(
						replace 
							(
							replace 
								(
								@p_string, char(10), char(200)
								)
							,char(38), char(206)	
							)
						,char(94), char(207)
						)
					,char(96), char(208)
					)
				,char(13), char(201)
				)
			,char(39), char(202)
			)
		,char(9), char(203)
		)
	,char(34), char(204)
	)
,char(32), char(205)
)

RETURN @v_return

end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

