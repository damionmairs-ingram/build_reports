SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE  PROCEDURE p_create_rs_no_symbols
   
AS
BEGIN


	delete from rs_no_symbols

	declare c_report_sections cursor for
		select 
			ris.id
			,dbo.f_convert_to_symbols(substring(ris.sql_text,1,4000))
		from report_instances ri
		inner join report_instance_sections ris
		on ris.report_instance_id = ri.id
		where 
			ri.database_type in ('SQLSERVER', 'BOTH')
			and
			ri.current_version = 'Y'


	declare
		@v_rs_id			numeric
		,@v_rs_sql_text1	varchar(4000)
		,@v_rs_sql_text2	varchar(4000)
		,@v_rs_sql_text3	varchar(4000)
		,@v_rs_sql_text4	varchar(4000)
		,@v_rs_sql_text5	varchar(4000)
		,@ptrval 			varbinary(16)


	open c_report_sections 

		fetch next from c_report_sections into
			@v_rs_id
			,@v_rs_sql_text1
				
		while @@fetch_status = 0

		begin
	
			--insert all columns except SQL_TEXT into report_sections
			insert into rs_no_symbols 	
					(
					rs_id
					,rs_sql_text
					)
			values	
					(
					@v_rs_ID
					,@v_rs_sql_text1
					)

			select 
				@v_rs_sql_text2 = dbo.f_convert_to_symbols(substring(sql_text,4001,4000))
			from report_instance_sections 
			where 
				id = @v_rs_id

			if isnull(@v_rs_sql_text2,'') <> '' 
				begin

					SELECT @ptrval = TEXTPTR(rs_sql_text) 
					FROM 
						rs_no_symbols
					WHERE 
						rs_id = @v_rs_id

					UPDATETEXT rs_no_symbols.rs_sql_text @ptrval 4000 0 @v_rs_sql_text2

				end


			select 
    			@v_rs_sql_text3 = dbo.f_convert_to_symbols(substring(sql_text,8001,4000))
			from 
				report_instance_sections
			where
				id = @v_rs_id

			if isnull(@v_rs_sql_text3,'') <> '' 
				begin

					SELECT @ptrval = TEXTPTR(rs_sql_text) 
					FROM 
						rs_no_symbols
					WHERE 
						rs_id = @v_rs_id

					UPDATETEXT rs_no_symbols.rs_sql_text @ptrval 8000 0 @v_rs_sql_text3

				end


			select 
    			@v_rs_sql_text4 = dbo.f_convert_to_symbols(substring(sql_text,12001,4000))
			from 
				report_instance_sections
			where
				id = @v_rs_id

			if isnull(@v_rs_sql_text4,'') <> '' 
				begin

					SELECT @ptrval = TEXTPTR(rs_sql_text) 
					FROM 
						rs_no_symbols
					WHERE 
						rs_id = @v_rs_id

					UPDATETEXT rs_no_symbols.rs_sql_text @ptrval 12000 0 @v_rs_sql_text4

				end


			select 
    			@v_rs_sql_text5 = dbo.f_convert_to_symbols(substring(sql_text,16001,4000))
			from 
				report_instance_sections
			where
				id = @v_rs_id

			if isnull(@v_rs_sql_text5,'') <> '' 
				begin

					SELECT @ptrval = TEXTPTR(rs_sql_text) 
					FROM 
						rs_no_symbols
					WHERE 
						rs_id = @v_rs_id

					UPDATETEXT rs_no_symbols.rs_sql_text @ptrval 16000 0 @v_rs_sql_text5

				end

			fetch next from c_report_sections into
				@v_rs_id
				,@v_rs_sql_text1

		end

	close c_report_sections
	deallocate c_report_sections
	           
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

