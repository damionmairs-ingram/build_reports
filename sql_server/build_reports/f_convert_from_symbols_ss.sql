SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO






ALTER  FUNCTION dbo.[f_convert_from_symbols] (@p_string varchar(6000)) RETURNS VARCHAR(6000)
AS

/*
* NAME:		f_convert_from_symbols
* DESCRIPTION:	This function replaces characters and formatting that were
*				removed from various fields during the extract of reports
*				from the Corelogic Reporting Repository in order to create a
*				valid text file.
*              The following codes are converted
*                  char(200)    char(10)
*                  char(201)    char(13)
*                  char(202)    char(39)
*                  char(203)    char(9)
*                  char(204)    char(34)
*                  char(205)    char(32)
*					chr(206)	chr(38)		
*					chr(207)	chr(94)	
*					chr(208)	chr(96)		
* FUNCTIONS:	
* HISTORY:		1.0  mb  15/06/2004  initial version
*              1.1  mb  11/01/2005  repository version (based on Oracle function v1.2)
*              1.2  kt  22/05/05  added chr(38), chr(94), chr(96)
*/

BEGIN

declare @v_return  VARCHAR(6000) 

set @v_return =


replace 
	(
	replace 
		(
		replace 
			(
			replace 
				(
				replace 
					(
					replace 
						(
						replace 
							(
							replace 
								(
								replace 
									(
									@p_string, char(200), char(10)
									)
								,char(206), char(38)	
								)
							,char(207), char(94)
							)
						,char(208), char(96)
						)
					,char(201), char(13)
					)
				,char(202), char(39)
				)
			,char(203), char(9)
			)
		,char(204), char(34)
		)
	,char(205), char(32)
	)

RETURN @v_return

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

