
	exec dbo.drop_object 'pop_child_dm_c_others', 'procedure'
	go
	create procedure pop_child_dm_c_others as
	begin
		set nocount on
		--
		declare @script varchar(30), @step varchar(30), @error_code int, @row_count int, @error_message varchar(200)
		--
		set @script = 'Populate_Package'
		set @step   = 'dm_c_others'
		--
		execute dbo.log_header @script, @step
		--
		execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_organisations'
		execute @error_code = dbo.synchronise_table 'dm_c_organisations',
							'(select
								o.id organisation_id,
								o.name,
								coalesce(
									(
										select
											dbo.format_address(a.flat_number, a.building, a.street_number, a.street, a.district, a.town, a.post_code)
										from
											addresses_organisations ao
										inner join addresses a
										on a.id = ao.address_id
										where
											ao.organisation_id = o.id
											and
											dbo.to_weighted_start(dbo.future(ao.start_date), ao.address_id) = (
												select top 1
													dbo.to_weighted_start(dbo.future(ao2.start_date), ao2.address_id)
												from
													addresses_organisations ao2
												where
													ao2.organisation_id = ao.organisation_id
												order by 1 desc
											)
									), ''No Address Info'') address,
								case
									when dbo.to_varchar(o.id) in (
										select
											rf.mapped_value
										from
											dm_c_filters rf
										where
											rf.filter_name = ''Child Unaccompanied Asylum Seeker Teams''
									) then
										''Y''
								end is_uasc_team
								from
									organisations o)'
		if @error_code != 0 goto exception
		--
		execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_worker_roles'
		execute @error_code = dbo.synchronise_table 'dm_c_worker_roles',
							'(select
								dmwr.id worker_role_id,
								dmwr.worker_id,
								dmwr.role description,
								dmwr.start_date,
								dmwr.end_date,
								dmwr.org_id organisation_id
							from
								dm_worker_roles dmwr)'
		if @error_code != 0 goto exception
		--
		-- NB. Process detail table(s) afterwards, usually using lookups for flags, etc.
		execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_workers'
		execute @error_code = dbo.synchronise_table 'dm_c_workers',
                            '(select
								w.id worker_id,
								case
									when w.first_names is null then
										w.last_names
									when w.last_names is null then
										w.first_names
									else
										dbo.append2(w.first_names, '' '', w.last_names)
								end  full_name                   
							from workers w)'
		--
		if @error_code != 0 goto exception
		execute dbo.analyze_tables @script, @step, 'dm_c_organisations, dm_c_worker_roles, dm_c_workers'
		execute dbo.log_tail @script
		return 0
	exception:
		execute dbo.log_error @SCRIPT, @step, @error_code, @error_message
		execute dbo.analyze_tables @script, @step, 'dm_c_organisations, dm_c_worker_roles, dm_c_workers'
		return @error_code
	end
	go

