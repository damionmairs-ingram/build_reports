	exec dbo.drop_object 'pop_child_dm_c_services', 'procedure'
	go
	create procedure pop_child_dm_c_services as
	begin
		set nocount on
		--
		declare @script varchar(30), @step varchar(30), @error_code int, @row_count int, @error_message varchar(200)
		set @script = 'Populate_Package'
		set @step   = 'dm_c_services'
		--
		execute dbo.log_header @script, @step
		execute dbo.drop_table_indexes @script, @step, 'dm_c_service_suppliers, dm_c_service_types, dm_c_services, dm_c_services_episodes', 'IF_EMPTY'
		--
		-- Example Switch usage, where appropriate
		-- NB. There are various ways to use a Switch: in SQL or if-condition, etc.
		declare	@switch_name  varchar(30),
				@switch_value varchar(30)
		set @switch_name  = 'Child Service Setup'
		set @switch_value = dbo.switch_mapping(@switch_name)
		execute dbo.log_switch @script, @step, @switch_name, @switch_value
		--
		if object_id('tempdb..#dm_c_services_tmp') is not null
		begin					
			exec('drop table #dm_c_services_tmp')
		end
		create table #dm_c_services_tmp (
			element_detail_id			numeric(9) 	not null,
			start_date					datetime 	not null,
			end_date					datetime,
			element_start_date	   		datetime 	not null,
			element_end_date			datetime,
			ref_element_id			   	numeric(9) 	not null,
			type_id					  	varchar(26)	not null,
			person_id					varchar(16)	not null,
			supplier_id			  		numeric(9),
			allocated_person_id		  	varchar(16),
			worker_id					numeric(9),
			cin_service_category		varchar(255),
			cin_short_break_category	varchar(255),
			is_direct_payment			varchar(1),
			cin_provider_category		varchar(255),
			is_placement			 	varchar(1),
			constraint dm_c_services_tmp_pk
			primary key (element_detail_id)
		)
		--
		select @error_code = @@error, @row_count = @@rowcount
		if @error_code != 0 goto exception
		--
		if @switch_value = 'Finance'
		begin
			--
			-- NB. Process lookup table(s) first
			execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_service_suppliers (Finance)'
			execute @error_code = dbo.synchronise_table 'dm_c_service_suppliers',
							 	'(select o.id supplier_id
										,o.name supplier_name
										,case when o.sub_sector is null then o.sector else dbo.append2(o.sector,''.'',o.sub_sector) end full_sector_code
										,(select category_value
										  from dm_c_groups
										  where group_name = ''Child CIN Service Providers''
										  and mapped_value = case when o.sub_sector is null then o.sector else dbo.append2(o.sector,''.'',o.sub_sector) end) cin_provider_category
								  from organisations o
								  where exists (select 1
												from element_details ed
												where ed.supplier_id = o.id))'
								--'[filter destination if more than one Sync reqd]',
								--'[TEMP only if reqd for performance]'
			if @error_code != 0 goto exception
			--
			execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_service_types (Finance)'
			execute @error_code = dbo.synchronise_table 'dm_c_service_types',
							 	'(select dbo.append2(dbo.to_varchar(stet.service_type_id),''.'',dbo.to_varchar(stet.element_type_id))	type_id
												,dbo.append2(sty.description,'' -> '',elt.description)	type_description
												,(select category_value
												from dm_c_groups
												where group_name = ''Child CIN Services''
												and mapped_value = dbo.append2(dbo.to_varchar(stet.service_type_id),''.'',dbo.to_varchar(stet.element_type_id)))	cin_service_category
												,(select category_value
												from dm_c_groups
												where group_name = ''Child CIN Short Breaks''
												and mapped_value = dbo.append2(dbo.to_varchar(stet.service_type_id),''.'',dbo.to_varchar(stet.element_type_id)))	cin_short_break_category
												,case 
													when (select 1
															from dm_c_filters
															where filter_name = ''Child Direct Payment''	
															and mapped_value = dbo.append2(dbo.to_varchar(stet.service_type_id),''.'',dbo.to_varchar(stet.element_type_id))) is not null 
													then ''Y'' 
													else ''N'' 
												end is_direct_payment
												,case 
													when (select 1
															from dm_c_filters
															where filter_name = ''Child Placement Services''	
															and mapped_value = dbo.append2(dbo.to_varchar(stet.service_type_id),''.'',dbo.to_varchar(stet.element_type_id))) is not null 
													then ''Y'' 
													else ''N'' 
												end is_placement											
										from 	service_types_element_types stet 	
										inner join service_types sty on sty.id = stet.service_type_id 	
										inner join element_types elt on elt.id = stet.element_type_id)'
								--'[filter destination if more than one Sync reqd]',
								--'[TEMP only if reqd for performance]'
			if @error_code != 0 goto exception
			--
			execute dbo.log_line @SCRIPT, @STEP, 'Create Build table of Services (Finance)'
			insert into #dm_c_services_tmp (
				element_detail_id,
				start_date,
				end_date,
				element_start_date,
				element_end_date,
				ref_element_id,
				type_id,
				person_id,
				supplier_id,
				allocated_person_id,
				worker_id,
				cin_service_category,
				cin_short_break_category,
				is_direct_payment,
				cin_provider_category,
				is_placement
			)
			select
				ed.id element_detail_id
				,ed.start_date
				,ed.end_date
				,dbo.no_time(ed.start_date) element_start_date
				,dbo.no_time(ed.end_date) element_end_date			
				,ed.element_type_id ref_element_id
				,dbo.append2(dbo.to_varchar(pks.service_type_id),'.',dbo.to_varchar(ed.element_type_id)) type_id
				,cpk.person_id person_id
				,ed.supplier_id supplier_id
				,ed.allocated_person_id
				,ed.allocated_worker_id worker_id
				,styp.cin_service_category
				,styp.cin_short_break_category
				,styp.is_direct_payment
				,ssup.cin_provider_category
				,styp.is_placement
			from
				element_details ed
			--JOIN: Find Client ID
			inner join elements els
			on els.id = ed.element_id
			inner join package_services pks
			on pks.id = els.package_service_id
			inner join care_packages cpk
			on cpk.id = pks.care_package_id
			inner join dm_c_children c
			on c.person_id = cpk.person_id															   
			left outer join dm_c_service_suppliers ssup
			on ssup.supplier_id = ed.supplier_id																  							
			inner join dm_c_service_types styp
			on styp.type_id = dbo.append2(dbo.to_varchar(pks.service_type_id),'.',dbo.to_varchar(ed.element_type_id))
			where
				--CRITERIA: Exclude cancelled Element Details
				ed.cancelled_on is null
				and
				--CRITERIA: Exclude services that never took place
				dbo.no_time(ed.start_date) <= dbo.future(ed.end_date)
			--
			select @error_code = @@error, @row_count = @@rowcount
			if @error_code != 0 goto exception
			--
			execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_services (Finance)'
			execute @error_code = dbo.synchronise_table 'dm_c_services', '#dm_c_services_tmp'
			if @error_code != 0 goto exception
			--
			-- NB. Process detail table(s) afterwards, usually using lookups for flags, etc.
			execute dbo.log_line @SCRIPT, @step, 'Synchronise dm_c_services_episodes'
			execute @error_code = dbo.synchronise_table 'dm_c_services_episodes',
									'(select s.element_detail_id
										,epi.id episode_id
									from dm_c_services s
									inner join element_changes elc
									on elc.new_element_detail_id = s.element_detail_id	
									inner join service_changes svc
									on svc.id = elc.service_change_id
									inner join care_package_changes cpc
									on cpc.id = svc.change_id	
									inner join episodes epi
									on epi.id = cpc.episode_id)'
							--'[filter destination if more than one Sync reqd]',
							--'[TEMP only if reqd for performance]'
		end
		else if @switch_value = 'Non-Finance'
		begin
			execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_service_suppliers (Non-Finance)'
			execute @error_code = dbo.synchronise_table 'dm_c_service_suppliers',
									'(select o.id supplier_id
											,o.name supplier_name
											,case when o.sub_sector is null then o.sector else dbo.append2(o.sector,''.'',o.sub_sector) end full_sector_code
											,(select category_value
										  	from dm_c_groups
										  	where group_name = ''Child CIN Service Providers''
										  	and mapped_value = case when o.sub_sector is null then o.sector else dbo.append2(o.sector,''.'',o.sub_sector) end) cin_provider_category
										from organisations o
										where exists (select 1
														from person_service_tasks pst
														where pst.organisation_id = o.id))'
									--'[filter destination if more than one Sync reqd]',
									--'[TEMP only if reqd for performance]'
			if @error_code != 0 goto exception
			--
			execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_service_types (Non-Finance)'
			execute @error_code = dbo.synchronise_table 'dm_c_service_types',							
								  '(select 	dbo.append2(sty.service_code,''.'',dbo.to_varchar(tty.id)) type_id
											,dbo.append2(sty.description,'' -> '',tty.description) type_description 	
											,(select category_value
												from dm_c_groups
												where group_name = ''Child CIN Services''
												and mapped_value = dbo.append2(sty.service_code,''.'',dbo.to_varchar(tty.id))) cin_service_category	
											,(select category_value
												from dm_c_groups
												where group_name = ''Child CIN Short Breaks''
												and mapped_value = dbo.append2(sty.service_code,''.'',dbo.to_varchar(tty.id))) cin_short_break_category	
											,case 
												when (select 1
														from dm_c_filters
														where filter_name = ''Child Direct Payment''	
														and mapped_value = dbo.append2(sty.service_code,''.'',dbo.to_varchar(tty.id))) is not null 
												then ''Y'' 
												else ''N'' 
											end is_direct_payment
											,case 
												when (select 1
														from dm_c_filters
														where filter_name = ''Child Placement Services''	
														and mapped_value = dbo.append2(sty.service_code,''.'',dbo.to_varchar(tty.id))) is not null 
												then ''Y'' 
												else ''N''  
											end is_placement												 
									from services sty 	
									inner join service_tasks tty 
									on tty.service_code = sty.service_code)'
									--'[filter destination if more than one Sync reqd]',
									--'[TEMP only if reqd for performance]'
			if @error_code != 0 goto exception
			--
			execute dbo.log_line @SCRIPT, @STEP, 'Create Build table of Services (Non-Finance)'
			insert into #dm_c_services_tmp (
				element_detail_id,
				start_date,
				end_date,
				element_start_date,
				element_end_date,
				ref_element_id,
				type_id,
				person_id,
				supplier_id,
				allocated_person_id,
				worker_id,
				cin_service_category,
				cin_short_break_category,
				is_direct_payment,
				cin_provider_category,
				is_placement
			)
			select
				pst.id element_detail_id
				,pst.start_date
				,pst.end_date	
				,dbo.no_time(pst.start_date) element_start_date
				,dbo.no_time(pst.end_date) element_end_date
				,pst.task_id ref_element_id
				,dbo.append2(pst.service_code, '.', pst.task_id) type_id
				,pst.person_id
				,ssup.supplier_id
				,pst.allocated_person_id
				,pst.allocated_worker_id
				,styp.cin_service_category
				,styp.cin_short_break_category
				,styp.is_direct_payment
				,ssup.cin_provider_category		
				,styp.is_placement
			from
				person_service_tasks pst
			inner join dm_c_children c
			on c.person_id = pst.person_id
			left outer join dm_c_service_suppliers ssup
			on ssup.supplier_id = pst.organisation_id
			inner join dm_c_service_types styp
			on styp.type_id = dbo.append2(pst.service_code, '.', pst.task_id)
			where
				--CRITERIA: Exclude cancelled Services
				pst.cancelled_on is null
				and
				--CRITERIA: Exclude services that never took place
				dbo.no_time(pst.start_date) <= dbo.future(pst.end_date)
			--
			execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_services (Non-Finance)'		
			execute @error_code = dbo.synchronise_table 'dm_c_services', '#dm_c_services_tmp'	
			if @error_code != 0 goto exception
			--
			execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_services_episodes (Non-Finance)'
			execute @error_code = dbo.synchronise_table 'dm_c_services_episodes',
									'(select s.element_detail_id
													,e.id episode_id
											from dm_c_services s
											inner join episode_service_tasks est
												on est.resulting_pst_id = s.element_detail_id
											inner join episodes e
											on e.id = est.episode_id)'
			if @error_code != 0 goto exception
		end
		else
		begin
			set @error_code = 50001
			set @error_message = 'Unhandled switch value'
			goto exception
		end
		--
		execute dbo.create_table_indexes @script, @step, 'dm_c_service_suppliers, dm_c_service_types, dm_c_services, dm_c_services_episodes', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_service_suppliers, dm_c_service_types, dm_c_services, dm_c_services_episodes'
		execute dbo.log_tail @script
		return 0
	exception:
		execute dbo.log_error @SCRIPT, @step, @error_code, @error_message
		if object_id('tempdb..#dm_c_services_tmp') is not null
		begin
			exec('drop table #dm_c_services_tmp')
		end
		-- Abort neatly
		execute dbo.create_table_indexes @script, @step, 'dm_c_service_suppliers, dm_c_service_types, dm_c_services, dm_c_services_episodes', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_service_suppliers, dm_c_service_types, dm_c_services, dm_c_services_episodes'
		return @error_code
	end
	go

