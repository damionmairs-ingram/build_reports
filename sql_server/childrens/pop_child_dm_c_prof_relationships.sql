exec dbo.drop_object 'pop_child_dm_c_prof_relationships', 'procedure'
go
create procedure pop_child_dm_c_prof_relationships as
begin
	set nocount on
		
	declare @SCRIPT varchar(30), @STEP varchar(30), @error_code int, @row_count int, @error_message varchar(200)
	set @SCRIPT	 = 'Populate_Package'
   	set @STEP	 = 'dm_c_prof_relationships'

	execute dbo.log_header @SCRIPT, @STEP
	execute dbo.drop_table_indexes @SCRIPT, @STEP, 'dm_c_prof_rel_types, dm_c_prof_relationships', 'IF_EMPTY'
    --

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_prof_rel_types'
    execute @error_code = dbo.synchronise_table 'dm_c_prof_rel_types',
							'(select dbo.append(''REL.'',prt.rel_type) prof_rel_type_code
                                		,''FWi Prof Rel'' prof_rel_type_source
                                		,prt.description
                                		,(select ''Y''
                                            from dm_c_filters f
                                            where f.mapped_value = dbo.append(''REL.'',prt.rel_type)
                                            and f.filter_name = ''Child Family Support Worker''
                                          ) is_family_support_worker
                                		 , case when prt.rel_type = ''GP'' then ''Y'' end is_gp	
                                		,(select ''Y''
                                            from dm_c_filters f
                                            where f.mapped_value = dbo.append(''REL.'',prt.rel_type)
                                            and f.filter_name = ''Child Independent Reviewing Officer''
                                          )	is_independent_review_officer
                                		,(select ''Y''
                                            from dm_c_filters f
                                            where f.mapped_value = dbo.append(''REL.'',prt.rel_type)
                                            and f.filter_name = ''Child Personal Advisors''
                                          )	is_personal_advisor	  
                                		 ,prt.rel_type ref_prof_rel_type_id 	 	  
                                from PROF_RELATIONSHIP_TYPES prt
                                where prt.org_worker_flag in (''W'',''B'')
                                and not exists (select 1 
                                                from dm_c_filters f
                                                where f.mapped_value = dbo.append(''REL.'',prt.rel_type)  
                                                and f.filter_name = ''Child Allocated Workers''))'

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_prof_relationships'
    execute @error_code = dbo.synchronise_table 'dm_c_prof_relationships',
							'(select pw.id prof_relationship_id
                                		,dbo.append(''REL.'',pw.type) prof_rel_type_code
                                		,pw.person_id
                                		,pw.organisation_id
                                		,pw.worker_id
                                		,dbo.no_time(pw.start_date) start_date
                                		,dbo.no_time(pw.end_date) end_date
                                        ,(select
                                            -- Allow for dodgy data (multiple open Primary Roles)
                                            max(wro.org_id)
                                        	from
                                            dm_worker_roles wro
                                        	where
                                            --CRITERIA: Same Worker
                                            wro.worker_id = pw.worker_id
                                            and
                                            --CRITERIA: During relationship
                                            wro.start_date <= dbo.future(pw.end_date)
                                            and
                                            dbo.future(wro.end_date) >= pw.start_date
                                            and
                                            --CRITERIA: Latest ending role during relationship
                                            dbo.future(wro.end_date) = (
                                                  select
                                                        max(dbo.future(wro2.end_date))
                                                  from
                                                        dm_worker_roles wro2
                                                  where
                                                        --CRITERIA: Same Worker
                                                        wro2.worker_id = pw.worker_id
                                                        and
                                                        --CRITERIA: During relationship
                                                        wro2.start_date <= dbo.future(pw.end_date)
                                                        and
                                                        dbo.future(wro2.end_date) >= pw.start_date
                                            )
                                        ) latest_team_id
                                		,pw.end_date_reason
                                		,rt.is_family_support_worker
                                		,rt.is_gp
                                		,rt.is_independent_review_officer
                                		,rt.is_personal_advisor
                                from people_workers pw
								inner join dm_c_children c
								on c.person_id = pw.person_id
                                inner join dm_c_prof_rel_types rt
                                on rt.ref_prof_rel_type_id = pw.type )'

	if @error_code != 0 goto exception

    --
	execute dbo.create_table_indexes @SCRIPT, @STEP, 'dm_c_prof_rel_types, dm_c_prof_relationships', 'IF_NOT_EMPTY'		
    execute dbo.analyze_tables @SCRIPT, @STEP, 'dm_c_prof_rel_types, dm_c_prof_relationships'
    --
    execute dbo.log_tail @SCRIPT
	return 0

exception:

		execute dbo.log_error @SCRIPT, @step, @error_code, @error_message
		-- Abort neatly
		execute dbo.create_table_indexes @script, @step, 'dm_c_prof_rel_types, dm_c_prof_relationships', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_prof_rel_types, dm_c_prof_relationships'
		return @error_code
end