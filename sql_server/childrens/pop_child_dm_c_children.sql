exec dbo.drop_object 'pop_child_dm_c_children', 'procedure'
go
create procedure pop_child_dm_c_children as
begin
	set nocount on
		
	declare @SCRIPT varchar(30), @STEP varchar(30), @error_code int, @row_count int, @error_message varchar(200)
	set @SCRIPT	 = 'Populate_Package'
   	set @STEP	 = 'dm_c_children'

	execute dbo.log_header @SCRIPT, @STEP
	execute dbo.drop_table_indexes @SCRIPT, @STEP, 'dm_c_children, dm_c_addresses_home, dm_c_addresses_placement, dm_c_countries_of_birth, dm_c_dental_checks, dm_c_disability_types, dm_c_disabilities, dm_c_ethnicities, dm_c_former_accom_types, dm_c_former_activity_types, dm_c_former_in_touch_types, dm_c_former_looked_after, dm_c_health_assessments, dm_c_immunisation_types, dm_c_immunisations, dm_c_non_la_legal_status_types, dm_c_non_la_legal_statuses, dm_c_regist_category_types, dm_c_registration_categories, dm_c_school_absences, dm_c_serv_user_group_types, dm_c_service_user_groups', 'IF_EMPTY'
    --

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_children'


		if object_id('tempdb..#dm_c_children_tmp') is not null
			begin			
				exec('truncate table #dm_c_children_tmp')	
				exec('drop table #dm_c_children_tmp')
			end

    	select * into #dm_c_children_tmp from dm_c_children where 1 = 2

  	    execute ('alter table #dm_c_children_tmp add constraint dm_c_children_tmp_pk primary key (person_id)')
		
            insert into #dm_c_children_tmp
                (PERSON_ID
                ,SSDA903_ID
                ,UPN_ID
                ,FULL_NAME
                ,LAST_NAME
                ,DATE_OF_BIRTH
                ,DATE_OF_DEATH
                ,GENDER
                ,SSDA903_GENDER
                ,MAIN_ETHNICITY_CODE
                ,RESTRICTED
                ,COUNTRY_OF_BIRTH_CODE
                ,NHS_ID
                ,SCN_ID
                ,FULL_ETHNICITY_CODE
                ,FORMER_UPN_ID)

			select
				p.id person_id
				,(
					select max(pr.reference)
					from
						person_references pr
					where
						pr.person_id = p.id
						and
						pr.reference_type_id = '903REF'
						and
						dbo.len(pr.reference) <= 10
				) ssda903_id
            	,(
					select max(pr.reference)
					from
						person_references pr
					where
						pr.person_id = p.id
						and
						pr.reference_type_id in (
							select f.mapped_value
							from dm_c_filters f
							where f.filter_name = 'Child UPN References'
						)
						and
						dbo.len(pr.reference) <= 13
				) upn_id
				,coalesce(dbo.f_get_person_name(p.id),'No Name') full_name
				,coalesce((
					select upper(pn.last_name)
					from
						person_names pn
					where
						pn.person_id = p.id
						and
						pn.name_class_id = 'MAIN'
						and
						dbo.to_weighted_start(pn.start_date, pn.pna_uid) =	(
							select max(dbo.to_weighted_start(pn2.start_date, pn2.pna_uid))
							from
								person_names pn2
							where
								pn2.person_id = p.id
								and
								pn2.name_class_id = 'MAIN'
						)
				),'No Name') last_name	
            	,dbo.no_time(p.date_of_birth) date_of_birth
            	,dbo.no_time(p.date_of_death) date_of_death
            	,p.gender gender
            	,case
					when p.gender is null
					or p.date_of_birth > dbo.now() then
						-- Not known (means that the gender of the person has not been recorded).
						-- Also covers gender of unborn children
						0
            		when p.gender = 'M' then
						1
            		when p.gender = 'F' then
						2
					when p.gender = 'U' then
						-- Not specified (means indeterminate i.e. unable to be classed as either male or female)
						9
                end ssda903_gender -- need to know 0 and 9 options. they cant both be the same
           		,p.ethnicity main_ethnicity_code
           		,p.restricted
           		,p.country_of_birth country_of_birth_code
           		,(
					select dbo.to_numeric(replace(max(pr.reference),' ',''))
					from
						person_references pr
					where
						pr.person_id = p.id
						and
						pr.reference_type_id = 'NHS'
				) nhs_id	
				, null scn_id --not recorded yet ??																		
				,dbo.append2(p.ethnicity,'.',p.sub_ethnicity) full_ethnicity_code
            	,(
					select max(pr.reference)
					from
						person_references pr
					where
						pr.person_id = p.id
						and
						pr.reference_type_id in (
							select f.mapped_value
							from dm_c_filters f
							where f.filter_name = 'Child Former UPN References'
						)
						and
						dbo.len(pr.reference) <= 13
				) former_upn_id
            from people p
            where p.id in ( 	
            	select person_id

                from looked_after_placements
				--
                union
                -- CIN Regsiters
                select pr.person_id

                from person_registrations pr

                inner join registers r
                on r.id = pr.reg_id

                inner join dm_c_filters f
                on f.mapped_value = r.id
                and f.filter_name = 'Child CIN Registers'

                where exists (select 1
                			from people p
                			where p.context_flag in ('C','B')
                			and p.id = pr.person_id)
                --			
                union 
                -- CP Plan
                select pr.person_id

                from person_registrations pr

                inner join registers r
                on r.id = pr.reg_id

                where r.id = 1
                --
                union				
                -- CIN Episodes
                select e.subject

                from episodes e

                inner join people p
                on p.id = e.subject
                and p.context_flag in ('C','B')

                inner join dm_c_filters f
                on f.mapped_value = e.type
                and f.filter_name = 'Child CIN Episodes'
				--
				union
				--
				select e.subject
				
				from episodes e
				
                inner join people p
                on p.id = e.subject
                and p.context_flag in ('C','B')						
				
                inner join episode_outcomes eo
                on eo.episode_id = e.id

                inner join outcome_types ot
                on ot.outcome_type = eo.code

                inner join dm_c_filters f
                on f.mapped_value = ot.new_episode_type
                and f.filter_name = 'Child CIN Episodes'								
                --
                union
                -- Young Carers
                select psug.person_id

                from people_service_user_groups psug

                inner join dm_c_filters f
                on f.mapped_value = dbo.append2(psug.group_id,'.',psug.sub_group_id)
                and f.filter_name = 'Child Young Carer'
                --
                union
                -- Services (Fin)            
                select p.id
                
                from people p
                
                inner join care_packages cpk
                on cpk.person_id = p.id
                
                inner join package_services pks
                on pks.care_package_id = cpk.id
                and pks.cancelled_on is null
                
                inner join elements els
                on els.package_service_id = pks.id
                and els.cancelled_on is null
                
                inner join element_details ed
                on ed.element_id = els.id				
                
                inner join dm_c_groups g
                on g.mapped_value = dbo.append2(dbo.to_varchar(pks.service_type_id),'.',dbo.to_varchar(ed.element_type_id))
                and g.group_name = 'Child CIN Services'
				
				inner join report_switch_mappings swm
				on swm.switch_name = 'Child Service Setup'
				and swm.switch_value = 'Finance'
                
                where 
					p.context_flag in ('C','B')					
					and
        			--CRITERIA: Exclude cancelled Element Details
        			ed.cancelled_on is null
        			and
        			--CRITERIA: Exclude services that never took place
        			dbo.no_time(ed.start_date) <= dbo.future(ed.end_date)				
                --
                union
                -- Services (Non-Fin)
                select 	pst.person_id
                		
                from person_service_tasks pst
                
                inner join people p
                on p.id = pst.person_id
                
                inner join dm_c_groups g
                on g.mapped_value = dbo.append2(pst.service_code, '.', pst.task_id)
                and g.group_name = 'Child CIN Services'
				
				inner join report_switch_mappings swm
				on swm.switch_name = 'Child Service Setup'
				and swm.switch_value = 'Non-Finance'				
                
                where 
					p.context_flag in ('C','B')
					and
        			--CRITERIA: Exclude cancelled Services
        			pst.cancelled_on is null
        			and
        			--CRITERIA: Exclude services that never took place
        			dbo.no_time(pst.start_date) <= dbo.future(pst.end_date)	
                --
                union
                -- people workers
                select pw.person_id
                
                from people_workers pw
                
                inner join people p
                on p.id = person_id
                and p.context_flag in ('C','B')
				)


	execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_children with Build Table'

    execute @error_code = dbo.synchronise_table 'dm_c_children',	
                          						'#dm_c_children_tmp'	

	if @error_code != 0 goto exception	      
						
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_addresses_home'
    execute @error_code = dbo.synchronise_table 'dm_c_addresses_home',														
								'(select ap.id ref_addresses_people_id
                                		,ap.person_id
                                		,dbo.no_time(ap.start_date) start_date
                                		,dbo.no_time(ap.end_date) end_date
                                		,coalesce(dbo.format_address(a.flat_number
                                                            ,a.building
                                                            ,a.street_number
                                                            ,a.street
                                                            ,a.district
                                                            ,a.town
                                                            ,a.post_code),''No address'') address
                                		,a.post_code post_code
                                		,a.district 
                                		,a.id ref_address_id							
                                		,null is_with_parents
                                		,null is_with_other_family
                                from addresses_people ap
                                inner join addresses a on a.id = ap.address_id
                                inner join dm_c_children c on c.person_id = ap.person_id
                                inner join report_switch_mappings r
                                on r.switch_name = ''Child Home Address''
                                and (
                                		(r.switch_value = ''Contact Address''
                                		and 
                                		ap.contact_address = ''Y''
                                		)
                                	or
                                		(r.switch_value = ''Main Address'' 
                                		and 
                                		ap.address_type = ''MAIN''
                                		)
                                	))'			

	if @error_code != 0 goto exception												
/*
No longer works due to table changes <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_addresses_placement'
    execute @error_code = dbo.synchronise_table 'dm_c_addresses_placement',
							'(select ap.id ref_addresses_people_id
                                		,ap.person_id
                                		,dbo.no_time(ap.start_date) start_date
                                		,dbo.no_time(ap.end_date) end_date
                                		,coalesce(dbo.format_address(a.flat_number
                                                            ,a.building
                                                            ,a.street_number
                                                            ,a.street
                                                            ,a.district
                                                            ,a.town
                                                            ,a.post_code),''No address'') address
                                		,a.post_code post_code
                                		,a.district
                                		,a.id ref_address_id							
                                from addresses_people ap
                                inner join addresses a on a.id = ap.address_id
                                inner join dm_c_children c on c.person_id = ap.person_id
                                where exists (select 1
                                			  from looked_after_placements lap
                                			  where lap.person_id = c.person_id))'																						
*/
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_countries_of_birth'
    execute @error_code = dbo.synchronise_table 'dm_c_countries_of_birth',
							'(select rd.ref_code country_of_birth_code
                                		,rd.ref_description description
                                		,g.category_value place_of_birth_category
                                from reference_data rd
                                left outer join dm_c_groups g
                                on g.mapped_value = rd.ref_code
                                and g.group_name = ''Child Place of Birth''
                                where rd.ref_domain = ''COUNTRY'')'

	if @error_code != 0 goto exception
								
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_dental_checks'
    execute @error_code = dbo.synchronise_table 'dm_c_dental_checks',
							'(select  phi.id dental_id
										,''FWi Dental'' created_from
                                		,phi.person_id
                                		,phi.intervention_date dental_check_date
                                 		,dbo.month_add(phi.intervention_date,12) next_dental_check_due
                                from person_health_interventions phi
                                inner join dm_c_children c on c.person_id = phi.person_id
                                where phi.code = ''DENTAL''
								union all
                                select  -- Ensure Looked After Children have an entry
                                        poc.period_of_care_id dental_id, -- Consistent dummy dental_id
										''FWi Looked After'' created_from,
                                        poc.person_id,
                                        dbo.make_date(null) dental_check_date,
                                        dbo.month_add(poc.start_date, 12) next_dental_check_due
                                  from dm_c_periods_of_care poc
                                  where --CRITERIA: Child has no Dental Check since period_of_care started
                                        not exists (
                                              select
                                                    ''x''
                                              from
                                                    person_health_interventions phi
                                              where
                                                    phi.person_id = poc.person_id
                                                    and
                                                    phi.code = ''DENTAL''
                                                    and
                                                    phi.intervention_date >= poc.start_date
                                        ))'					

	if @error_code != 0 goto exception																		

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_disability_types'
    execute @error_code = dbo.synchronise_table 'dm_c_disability_types',
							'(select rd.ref_code disability_type
                                		,rd.ref_description description
                                		,g.category_value cin_disability_category
                                from reference_data rd
                                left outer join dm_c_groups g
                                on g.mapped_value = rd.ref_code
                                and group_name = ''Child CIN Disabilities''
                                where rd.ref_domain = ''CONDITION'')'	

	if @error_code != 0 goto exception
								
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_disabilities'
    execute @error_code = dbo.synchronise_table 'dm_c_disabilities',
							'(select 	 pcd.id disability_id
                                		,pcd.person_id 
                                		,pcd.code disability_type
                                		,pcd.applicable_flag applicable
                                		,pcd.age_diagnosed
                                		,dt.cin_disability_category
                                from person_conditions_disabilities pcd
                                inner join dm_c_children c
                                on c.person_id = pcd.person_id
                                inner join dm_c_disability_types dt
                                on dt.disability_type = pcd.code)'

	if @error_code != 0 goto exception

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_ethnicities'						

		if object_id('tempdb..#dm_c_ethnicities_tmp') is not null
			begin					
				exec('drop table #dm_c_ethnicities_tmp')
			end

    	select * into #dm_c_ethnicities_tmp from dm_c_ethnicities where 1 = 2

		execute ('alter table #dm_c_ethnicities_tmp add constraint dm_c_ethnicities_tmp_pk primary key (ethnicity_code)')

						insert into #dm_c_ethnicities_tmp(ethnicity_code
									,ethnicity_description
									,sub_ethnicity_description
									,ssda903_ethnicity_code
									,ref_ethnicity_code
									,ref_sub_ethnicity_code
									,ethnicity_category
									,sub_ethnicity_category
									,cin_ethnicity_code
									,scottish_ethnicity_code)

							select rd.ref_code ethnicity_code
                            		,rd.ref_description ethnicity_description
                            		, null sub_ethnicity_description
                            		, null ssda903_ethnicity_code
                            		, rd.ref_code ref_ethnicity_code
                            		, null ref_sub_ethnicity_code
                                    ,(select rgc.category_value
                                        from report_groups rg
                                        inner join report_group_categories rgc
                                        on rgc.report_group_id = rg.id
                                        inner join report_group_mappings rgm
                                        on rgm.report_group_category_id = rgc.id
                                        where rg.name = 'Ethnicity'
                                        and rgm.mapped_value = rd.ref_code) ethnicity_category
                            		, null sub_ethnicity_category
                            		, null cin_ethnicity_code
                            		, null scottish_ethnicity_code	
                                from reference_data rd
                                where rd.ref_domain = 'ETHNICITY'
                                union
                                select dbo.append2(rd2.ref_code,'.',rd.ref_code) ethnicity_code
                                		,rd2.ref_description ethnicity_description
                                		,rd.ref_description sub_ethnicity_description
                                		,case dbo.f_getreportgroupingvalue(rd2.ref_code, 'Ethnicity')
                                				when 'White' then
                                					case dbo.f_getreportgroupingvalue(rd.ref_code, 'Sub-ethnicity')
                                						when 'British'
                                							then 'A1'
                                						when 'Irish'
                                							then 'A2'
                                						else
                                							'A3'
                                					end
                                				when 'Mixed' then
                                					case dbo.f_getreportgroupingvalue(rd.ref_code, 'Sub-ethnicity')
                                						when 'White and Black Caribbean'
                                							then 'B1'
                                						when 'White and Black African'
                                							then 'B2'
                                						when 'White and Asian'
                                							then 'B3'
                                						else
                                							'B4'
                                					end
                                				when 'Asian or Asian British' then
                                					case dbo.f_getreportgroupingvalue(rd.ref_code, 'Sub-ethnicity')
                                						when 'Indian'
                                							then 'C1'
                                						when 'Pakistani'
                                							then 'C2'
                                						when 'Bangladeshi'
                                							then 'C3'
                                						else
                                							'C4'
                                					end
                                				when 'Black or Black British' then
                                					case dbo.f_getreportgroupingvalue(rd.ref_code, 'Sub-ethnicity')
                                						when 'Caribbean'
                                							then 'D1'
                                						when 'African'
                                							then 'D2'
                                						else
                                							'D3'
                                					end
                                				when 'Chinese or other ethnic group' then
                                					case dbo.f_getreportgroupingvalue(rd.ref_code, 'Sub-ethnicity')
                                						when 'Chinese'
                                							then 'E1'
                                						else
                                							'E2'
                                					end
                                			end	ssda903_ethnicity_code
                                		, rd2.ref_code ref_ethnicity_code
                                		, rd.ref_code ref_sub_ethnicity_code
                                       ,(select rgc.category_value
                                            from report_groups rg
                                            inner join report_group_categories rgc
                                            on rgc.report_group_id = rg.id
                                            inner join report_group_mappings rgm
                                            on rgm.report_group_category_id = rgc.id
                                            where rg.name = 'Ethnicity'
                                            and rgm.mapped_value = rd2.ref_code) ethnicity_category
                                        ,(select rgc.category_value
                                            from report_groups rg
                                            inner join report_group_categories rgc
                                            on rgc.report_group_id = rg.id
                                            inner join report_group_mappings rgm
                                            on rgm.report_group_category_id = rgc.id
                                            where rg.name = 'Sub-ethnicity'
                                            and rgm.mapped_value = rd.ref_code) sub_ethnicity_category		
                                		, (select rgc.category_value
                                            from report_groups rg
                                            inner join report_group_categories rgc
                                            on rgc.report_group_id = rg.id
                                            inner join report_group_mappings rgm
                                            on rgm.report_group_category_id = rgc.id
                                            where rg.name = 'Child CIN Ethnicities'
                                            and rgm.mapped_value = rd.ref_code) cin_ethnicity_code
                                		, (select rgc.category_value
                                            from report_groups rg
                                            inner join report_group_categories rgc
                                            on rgc.report_group_id = rg.id
                                            inner join report_group_mappings rgm
                                            on rgm.report_group_category_id = rgc.id
                                            where rg.name = 'Child Scottish Ethnicities'
                                            and rgm.mapped_value = rd.ref_code) scottish_ethnicity_code
                                from reference_data rd
                                inner join reference_data rd2
                                on rd2.ref_code = rd.parent_code
                                and rd2.ref_domain = 'ETHNICITY'
                                where rd.ref_domain = 'SUBETHNICITY'	

    execute @error_code = dbo.synchronise_table 'dm_c_ethnicities',	
                          						'#dm_c_ethnicities_tmp'												

	if @error_code != 0 goto exception
														
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_former_accom_types'
    execute @error_code = dbo.synchronise_table 'dm_c_former_accom_types',
							'(select ref_code accomodation_type
								,ref_description description
							from reference_data
							where ref_domain = ''LAC_ACCOMM_TYPE'')'	

	if @error_code != 0 goto exception
							
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_former_activity_types'
    execute @error_code = dbo.synchronise_table 'dm_c_former_activity_types',
							'(select ref_code activity_type
                            		,ref_description description
                            		,(select ''Y''
                            		from dm_c_filters
                            		where filter_name = ''Child Age 19 In Employment, Education or Training''
                            		and mapped_value = ref_code) is_employ_educ_train
                            from reference_data
                            where ref_domain = ''LAC_ACTIVITY'')'	

	if @error_code != 0 goto exception
							
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_former_in_touch_types'
    execute @error_code = dbo.synchronise_table 'dm_c_former_in_touch_types',
							'(select ref_code in_touch_type
                            		,ref_description description
                            from reference_data
                            where ref_domain = ''LAC_IN_TOUCH'')'		

	if @error_code != 0 goto exception
							
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_former_looked_after'
    execute @error_code = dbo.synchronise_table 'dm_c_former_looked_after',
								'(select 	pfp.id future_prospect_id
					                    ,pfp.person_id
                                		,pfp.date_of_situation
                                		,case  
                                			when pfp.in_touch_at_19 = ''Y'' then ''1''
                                			when pfp.in_touch_at_19 = ''N'' then ''5''
											when pfp.in_touch_at_19 not in (''Y'',''N'') then pfp.in_touch_at_19
                                		 end in_touch_type
                                		,pfp.activity activity_type
                                		,fat.is_employ_educ_train
                                		,pfp.full_time
                                		,pfp.accommodation_suitable accomodation_suitable
                                		,pfp.accommodation_type accomodation_type
                                from person_future_prospects pfp
                                left outer join dm_c_former_activity_types fat
                                on fat.activity_type = pfp.activity)'	

	if @error_code != 0 goto exception
							
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_health_assessments'
    execute @error_code = dbo.synchronise_table 'dm_c_health_assessments',										
							'(select  phi.id health_id
									,''FWi Health'' created_from
                            		,phi.person_id
                            		,phi.intervention_date health_assessment_date
                             		,case 
                            			when dbo.f_person_age(c.date_of_birth,phi.intervention_date) < 5 then dbo.month_add(phi.intervention_date,6)
                            			else dbo.month_add(phi.intervention_date,12)
                            		 end next_health_assessment_due
    								,coalesce(sdq.completed,''N'') sdq_completed
    								,sdq.score sdq_score
    								,sdq.reason sdq_reason
    								,sdq.reason sdq_reason_category
                            from person_health_interventions phi
                            inner join dm_c_children c on c.person_id = phi.person_id
							left outer join person_health_intervention_sdq sdq
							on sdq.person_health_intervention_id = phi.id
                            where phi.code = ''HEALTH''
                            union all
                            select
                                -- Ensure Looked After Children have an entry
                                poc.period_of_care_id health_id, -- Consistent dummy health_id
								''FWi Looked After'' created_from,
                                poc.person_id,
                                dbo.make_date(null) health_assessment_date,
                                dbo.month_add(poc.start_date, 12) next_health_assessment_due,
								''N'' sdq_completed,
								null sdq_score,
								null sdq_reason,
								null sdq_reason_category
                            from
                                dm_c_periods_of_care poc
                            where
                                --CRITERIA: Child has no Health Check since period_of_care started
                                not exists (
                                      select
                                            ''x''
                                      from
                                            person_health_interventions phi
                                      where
                                            phi.person_id = poc.person_id
                                            and
                                            phi.code = ''HEALTH''
                                            and
                                            phi.intervention_date >= poc.start_date
                                			))'		

	if @error_code != 0 goto exception
											
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_immunisation_types'
    execute @error_code = dbo.synchronise_table 'dm_c_immunisation_types',
								'(select rd.ref_code immunisation_type
                                    		,rd.ref_description description
                                    		,(select g.category_value
                                                from dm_c_groups g
                                                where g.mapped_value = rd.ref_code
                                                and g.group_name = ''Child Immunisations Due'') immunisation_due_category
                                    from reference_data rd
                                    where rd.ref_domain = ''IMMUNISATION'')'	

	if @error_code != 0 goto exception
									
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_immunisations'
    execute @error_code = dbo.synchronise_table 'dm_c_immunisations',
								'(select pi.id immunisation_id
                                		,pi.person_id 
                                		,pi.code immunisation_type
                                		,coalesce(pi.age_months,0) age_months
                                		,coalesce(pi.age_years,0) age_years
                                		,coalesce(pi.age_months,0) + (coalesce(pi.age_years,0) * 12) full_age_in_months
                                		,t.immunisation_due_category
										,dbo.month_add(c.date_of_birth,coalesce(pi.age_months,0) + (coalesce(pi.age_years,0) * 12)) estimated_immunisation_date
                                from person_immunisations pi
                                inner join dm_c_immunisation_types t
                                on t.immunisation_type = pi.code
								inner join dm_c_children c
								on c.person_id = pi.person_id)'																												

	if @error_code != 0 goto exception
					
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_non_la_legal_status_types'
    execute @error_code = dbo.synchronise_table 'dm_c_non_la_legal_status_types',
							'(select rd.ref_code legal_status_type
                                		,rd.ref_description description
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Private Fostering Legal Statuses''
                                        and f.mapped_value = rd.ref_code) is_private_fostering
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Residence Order Legal Statuses''
                                        and f.mapped_value = rd.ref_code) is_residence_order
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Special Guardianship Order''
                                        and f.mapped_value = rd.ref_code) is_special_guardianship_order				
                                from reference_data rd
                                where rd.ref_domain = ''LEGAL_STATUS'')'								

	if @error_code != 0 goto exception
			
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_non_la_legal_statuses'
    execute @error_code = dbo.synchronise_table 'dm_c_non_la_legal_statuses',
							'(select nls.id legal_status_id
                            		,nls.person_id
                            		,nls.start_date
                            		,nls.end_date
									,nls.legal_status legal_status_type
                            		,t.is_private_fostering
                            		,t.is_residence_order
                            		,t.is_special_guardianship_order
                            from person_non_la_legal_statuses nls
                            inner join dm_c_children c on c.person_id = nls.person_id
                            inner join dm_c_non_la_legal_status_types t
                            on t.legal_status_type = nls.legal_status)'		

	if @error_code != 0 goto exception							

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_regist_category_types'
    execute @error_code = dbo.synchronise_table 'dm_c_regist_category_types',
							'(select rc.id category_id
                                		,rc.description category_description
                                		,r.id register_id
                                		,r.description register_description
                                		,case r.id when 1 then ''Y'' end is_child_protection_plan 
										,(select ''Y'' 
                                        	from dm_c_filters f
                                        	where f.mapped_value = r.id
                                        	and filter_name = ''Child CIN Registers'') is_cin_register,
										(
											select
												g.category_value
											from
												dm_c_groups g
											where
												g.group_name = ''Child CIN Category of Abuse''
												and
												g.mapped_value = rc.id
										) cin_abuse_category
                                from register_categories rc
                                inner join registers r
                                on r.id = rc.reg_id)'

	if @error_code != 0 goto exception
																
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_registration_categories'
    execute @error_code = dbo.synchronise_table 'dm_c_registration_categories',
							'(select prc.id registration_category_id
                                		,pr.person_id
                                		,prc.category category_id
                                		,prc.start_date category_start_date
                                		,prc.end_date category_end_date
                                		,pr.reg_reason registration_reason
                                		,pr.start_date registration_start_date
                                		,pr.dereg_reason deregistration_reason
                                		,pr.end_date deregistration_date
                                		,ct.is_child_protection_plan
                                		,case
											when pr.from_org_id is not null then
												''Y''
										end is_temporary_child_protection
                                		,ct.is_cin_register is_cin_register,
										ct.cin_abuse_category
                                from person_registration_categories prc
                                inner join person_registrations pr
                                on pr.id = prc.registration_id
                                inner join dm_c_regist_category_types ct
                                on ct.category_id = prc.category)'								

	if @error_code != 0 goto exception
								
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_school_absences'
    execute @error_code = dbo.synchronise_table 'dm_c_school_absences',
							'(select pa.id ABSENCE_ID
                                		,pa.person_id
                                		,pa.absence_date ABSENCE_START_DATE
                                		,coalesce(pa.end_absence_date, dbo.no_time(pa.absence_date) + (round(coalesce(pa.days_absent, 0.5),0) - 1)) ABSENCE_END_DATE
                                		,coalesce(pa.am_pm_flag,''F'') absence_start_am_pm_full
										,pa.end_part_of_day_flag absence_end_am_full
                                		,coalesce(pa.days_absent,0.5) DAYS_ABSENT
                                		,pa.authorised_flag
                                from person_absences pa)'

	if @error_code != 0 goto exception

    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_serv_user_group_types'
    execute @error_code = dbo.synchronise_table 'dm_c_serv_user_group_types',
							'(select dbo.append2(susg.group_id,''.'',susg.id) group_type
                                		,sug.description group_description
                                		,susg.description sub_group_description
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Absent Parenting''
                                        and f.mapped_value = dbo.append2(susg.group_id,''.'',susg.id)
                                        ) is_absent_parenting
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Disability''
                                        and f.mapped_value = dbo.append2(susg.group_id,''.'',susg.id)
                                        ) is_disability
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Unaccompanied Asylum Seeking Child''
                                        and f.mapped_value = dbo.append2(susg.group_id,''.'',susg.id)
                                        ) is_unaccompanied_asylum_seeker	
                                		,(select ''Y''
                                        from dm_c_filters f
                                        where f.filter_name = ''Child Young Carer''
                                        and f.mapped_value = dbo.append2(susg.group_id,''.'',susg.id)
                                        ) is_young_carer
                                		,(select g.category_value
                                        from dm_c_groups g
                                        where g.mapped_value = dbo.append2(susg.group_id,''.'',susg.id)
                                        and group_name = ''Child Categories of Need'') need_code_category							
                                from service_user_sub_groups susg
                                inner join service_user_groups sug
                                on sug.id = susg.group_id)'	

	if @error_code != 0 goto exception
								
    execute dbo.log_line @SCRIPT, @STEP, 'Synchronise dm_c_service_user_groups'
    execute @error_code = dbo.synchronise_table 'dm_c_service_user_groups',
							'(select psug.id service_user_group_id
                            		,psug.person_id
                            		,psug.group_id main_group_type
                            		,case when psug.sub_group_id is not null then dbo.append2(psug.group_id,''.'',psug.sub_group_id) end full_group_type
                            		,psug.primary_flag is_primary_group
                            		,dbo.no_time(psug.start_date) start_date
                            		,dbo.no_time(psug.end_date) end_date	 	
                            		,sugt.is_absent_parenting is_absent_parenting
                            		,sugt.is_disability is_disability
                            		,sugt.need_code_category
                            		,sugt.is_unaccompanied_asylum_seeker is_unaccompanied_asylum_seeker
                            		,sugt.is_young_carer
                                from people_service_user_groups psug
                                inner join dm_c_children c on c.person_id = psug.person_id
                                inner join dm_c_serv_user_group_types sugt
                                on sugt.group_type = case 
                                						when psug.sub_group_id is not null 
                                							then dbo.append2(psug.group_id,''.'',psug.sub_group_id) 
                                							else psug.group_id 
                                					 end)'		

	if @error_code != 0 goto exception											 											

    --
	execute dbo.create_table_indexes @SCRIPT, @STEP, 'dm_c_children, dm_c_addresses_home, dm_c_addresses_placement, dm_c_countries_of_birth, dm_c_dental_checks, dm_c_disability_types, dm_c_disabilities, dm_c_ethnicities, dm_c_former_accom_types, dm_c_former_activity_types, dm_c_former_in_touch_types, dm_c_former_looked_after, dm_c_health_assessments, dm_c_immunisation_types, dm_c_immunisations, dm_c_non_la_legal_status_types, dm_c_non_la_legal_statuses, dm_c_regist_category_types, dm_c_registration_categories, dm_c_school_absences, dm_c_serv_user_group_types, dm_c_service_user_groups', 'IF_NOT_EMPTY'		
    execute dbo.analyze_tables @SCRIPT, @STEP, 'dm_c_children, dm_c_addresses_home, dm_c_addresses_placement, dm_c_countries_of_birth, dm_c_dental_checks, dm_c_disability_types, dm_c_disabilities, dm_c_ethnicities, dm_c_former_accom_types, dm_c_former_activity_types, dm_c_former_in_touch_types, dm_c_former_looked_after, dm_c_health_assessments, dm_c_immunisation_types, dm_c_immunisations, dm_c_non_la_legal_status_types, dm_c_non_la_legal_statuses, dm_c_regist_category_types, dm_c_registration_categories, dm_c_school_absences, dm_c_serv_user_group_types, dm_c_service_user_groups'
    --
    execute dbo.log_tail @SCRIPT
	return 0

exception:

		execute dbo.log_error @SCRIPT, @step, @error_code, @error_message
		-- Abort neatly
		execute dbo.create_table_indexes @script, @step, 'dm_c_children, dm_c_addresses_home, dm_c_addresses_placement, dm_c_countries_of_birth, dm_c_dental_checks, dm_c_disability_types, dm_c_disabilities, dm_c_ethnicities, dm_c_former_accom_types, dm_c_former_activity_types, dm_c_former_in_touch_types, dm_c_former_looked_after, dm_c_health_assessments, dm_c_immunisation_types, dm_c_immunisations, dm_c_non_la_legal_status_types, dm_c_non_la_legal_statuses, dm_c_regist_category_types, dm_c_registration_categories, dm_c_school_absences, dm_c_serv_user_group_types, dm_c_service_user_groups', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_children, dm_c_addresses_home, dm_c_addresses_placement, dm_c_countries_of_birth, dm_c_dental_checks, dm_c_disability_types, dm_c_disabilities, dm_c_ethnicities, dm_c_former_accom_types, dm_c_former_activity_types, dm_c_former_in_touch_types, dm_c_former_looked_after, dm_c_health_assessments, dm_c_immunisation_types, dm_c_immunisations, dm_c_non_la_legal_status_types, dm_c_non_la_legal_statuses, dm_c_regist_category_types, dm_c_registration_categories, dm_c_school_absences, dm_c_serv_user_group_types, dm_c_service_user_groups'
		return @error_code
end
