create procedure pop_c_dm_c_episodes as

		declare @script varchar(30), @step varchar(30), @error_code int, @row_count int, @error_message varchar(200)
		set @script = 'Populate_Package'
		set @step   = 'Child Episodes'
	begin
		execute dbo.log_header @script, @step
		execute dbo.drop_table_indexes @script, @step, 'dm_c_episode_types, dm_c_episode_outcome_types, dm_c_episode_outcome_reasons, dm_c_visit_types, dm_c_visit_not_made_reasons, dm_c_episodes, dm_c_episode_outcomes, dm_c_visits, dm_c_episode_timings', 'IF_EMPTY'
		execute dbo.truncate_tables @script, @step, 'dm_c_episodes_tmp'
		--
		-- NB. Process lookup table(s) first
		execute dbo.log_line @script, @step, 'Synchronise dm_c_episode_types'
		execute dbo.synchronise_table 'dm_c_episode_types','(
			select
				et.type EPISODE_TYPE,
				et.description DESCRIPTION,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Adoption Best Interest Decision Episodes''
					) then
						''Y''
				end is_adoption_best_interest,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Adoption Match Episodes''
					) then
						''Y''
				end is_adoption_match,				
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Referrals''
					) then
						''Y''
				end is_child_referral,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Core Assessments''
					) then
						''Y''
				end is_core_assessment,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Initial Assessments''
					) then
						''Y''
				end is_initial_assessment,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Pathway Plans''
					) then
						''Y''
				end is_pathway_plan,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child PEP Reviews''
					) then
						''Y''
				end is_pep_review,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Private Fostering Notifications''
					) then
						''Y''
				end is_priv_fostering_notification,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Private Fostering Episodes''
					) then
						''Y''
				end is_private_fostering,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Section 47 Enquiries''
					) then
						''Y''
				end is_section_47_enquiry,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Strategy Discussions''
					) then
						''Y''
				end is_strategy_discussion,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child CIN Episodes''
					) then
						''Y''
				end is_cin_episode,
				case
					when et.type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child CIN Post Adoption Support Episodes''
					) then
						''Y''
				end is_cin_post_adoption_support,		
				(
					select
						rg.category_value
					from
						dm_c_groups rg
					where
						rg.group_name = ''Child LAC Reviews''
						and
						rg.mapped_value = et.type
				) lac_review_category,
				(
					select
						rg.category_value
					from
						dm_c_groups rg
					where
						rg.group_name = ''Child Visits''
						and
						rg.mapped_value = dbo.append(''EPI.'',et.type)
				) visit_category
			from
				episode_types et)', null, 'NO_TEMP', 'NO_DEBUG'
		--
		execute dbo.log_line @script, @step, 'Synchronise dm_c_episode_outcome_types'
		execute dbo.synchronise_table 'dm_c_episode_outcome_types','(
			select
				out.outcome_type outcome_code,
				out.description,
				case
					when out.outcome_type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						inner join report_switch_mappings rsm
						on rsm.switch_name =  ''Child Adoption Best Interest''
						and rsm.switch_value = ''Episode Outcomes''
						where
							rf.filter_name = ''Child Adoption Best Interest Decisions''
					) then
						''Y''
				end is_adoption_best_interest,
				case
					when out.outcome_type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						inner join report_switch_mappings rsm
						on rsm.switch_name =  ''Child Adoption Match''
						and rsm.switch_value = ''Episode Outcomes''							
						where
							rf.filter_name = ''Child Adoption Matches''
					) then
						''Y''
				end is_adoption_match,
				case
					when out.outcome_type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child CIN Case Closure Outcomes''
					) then
						''Y''
				end is_cin_closure,
				case
					when out.outcome_type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Contact Only''
					)
					then
						''Y''
				end is_contact_only,
				case
					when out.outcome_type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Transition Plan''
					) then
						''Y''
				end is_potential_transition_plan,
				case
					when out.outcome_type in (
						select
							rf.mapped_value
						from
							dm_c_filters rf
						where
							rf.filter_name = ''Child Temporary Child Protection Outcome''
					) then
						''Y''
				end is_temporary_child_protection
			from
				outcome_types out)', null, 'NO_TEMP', 'NO_DEBUG'
		--
		execute dbo.log_line @script, @step, 'Synchronise dm_c_episode_outcome_reasons'
		execute dbo.synchronise_table 'dm_c_episode_outcome_reasons','(			
			select
				ref.ref_code outcome_reason,
				ref_description description,
				case
					when ref.ref_code = ''ERR'' then
						''Y''
				end is_cancelled,
				(
					select
						rg.category_value
					from
						dm_c_groups rg
					where
						ref.ref_code = rg.mapped_value
						and
						rg.group_name =	''Child CIN Case Closure Reasons''
				) cin_reason_closed_category
			from
				reference_data ref
			where
				ref_domain = ''OUTCOME_REASON'')', null, 'NO_TEMP', 'NO_DEBUG'
		--
		execute dbo.log_line @script, @step, 'Synchronise dm_c_visit_types'
		execute dbo.synchronise_table 'dm_c_visit_types','(
			select
				''VIS.'' + ref_code visit_type,
				''Visit '' + ref_description description,
				(
					select
						rg.category_value
					from
						dm_c_groups rg
					where
						''VIS.'' + ref_code = rg.mapped_value
						and
						rg.group_name = ''Child Visits''
				) visit_category,
				''FWi Visit'' visit_source,
				rd.ref_code ref_visit_type
			from
				reference_data rd
			where
				rd.ref_domain = ''CP_VISIT_TYPE''
			union all
			select
				''EPI.'' + epi.episode_type visit_type,
				''Episode '' + epi.episode_type description,
				(
					select
						rg.category_value
					from
						dm_c_groups rg
					where
						''EPI.'' + episode_type = rg.mapped_value
						and
						rg.group_name = ''Child Visits''
				) visit_category,
				''FWi Episode'' visit_source,
				epi.episode_type ref_visit_type
			from
				dm_c_episode_types epi
			where
				epi.visit_category is not null)', null, 'NO_TEMP', 'NO_DEBUG'
		--
		execute dbo.log_line @script, @step, 'Synchronise dm_c_visit_not_made_reasons'
		execute dbo.synchronise_table 'dm_c_visit_not_made_reasons','(
			select
				rd.ref_code reason_visit_not_made,
				rd.ref_description description
			from
				reference_data rd
			where
				rd.ref_domain = ''VISIT_NM_REASON'')', null, 'NO_TEMP', 'NO_DEBUG'
		--
		-- Gather statistics on Lookup tables, as used to populate Detail tables
		execute dbo.create_table_indexes @script, @step, 'dm_c_episode_types, dm_c_episode_outcome_types, dm_c_episode_outcome_reasons, dm_c_visit_types, dm_c_visit_not_made_reasons', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_episode_types, dm_c_episode_outcome_types, dm_c_episode_outcome_reasons, dm_c_visit_types, dm_c_visit_not_made_reasons'
		--
		-- Gather statistics on Details tables, as used to populate summary tables
		execute dbo.create_table_indexes @script, @step, 'dm_c_episodes, dm_c_episode_outcomes, dm_c_visits', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_episodes, dm_c_episode_outcomes, dm_c_visits'
		--
		-- Gather statistics on Summary tables
		execute dbo.create_table_indexes @script, @step, 'dm_c_episode_timings', 'IF_NOT_EMPTY'
		execute dbo.analyze_tables @script, @step, 'dm_c_episode_timings'
		--
		execute dbo.log_tail @script
	end