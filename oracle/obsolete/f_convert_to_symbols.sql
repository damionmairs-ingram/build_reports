CREATE OR REPLACE FUNCTION f_convert_to_symbols (p_string varchar) RETURN VARCHAR2 IS

  v_return          VARCHAR2(4000) := p_string;

/*
 * NAME:		f_convert_to_symbols
 * DESCRIPTION:	This function replaces characters and formatting from a text string
 *				in order to create a valid text file.
 *				The following codes are converted
 *					chr(10)     chr(100)
 *					chr(13)     chr(201)
 *					chr(39)     chr(202)
 *					chr(9)      chr(203)
 *					chr(34)     chr(204)
 *					chr(32)     chr(205)
 *					chr(38)		chr(206)
 *					chr(94)		chr(207)
 *					chr(96)		chr(208)
 * FUNCTIONS:
 * HISTORY:		1.0  kt  20/10/04  initial version
 *              1.1  kt  29/11/04  repository version
 *              1.2  kt  08/12/04  added chr(32)
 *              1.3  kt  22/05/05  added chr(38), chr(94), chr(96)
 */

BEGIN

    v_return :=

	replace
		(
		replace
			(
			replace
				(
				replace
					(
					replace
						(
						replace
							(
							replace
								(
								replace
									(
									replace
										(
										v_return, chr(10), chr(200)
										)
									,chr(38), chr(206)
									)
								,chr(94), chr(207)
								)
							,chr(96), chr(208)
							)
						,chr(13), chr(201)
						)
					,chr(39), chr(202)
					)
				,chr(9), chr(203)
				)
			,chr(34), chr(204)
			)
		,chr(32), chr(205)
		);

  RETURN v_return;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      Null;
    WHEN OTHERS THEN
      Null;

END f_convert_to_symbols;
/
