declare
    /*
     * NAME:		mos_mapping_conversions_ora.sql
     * DESCRIPTION:	Convert mappings to MOSAiC tables.
	 *				Script should only be run once when moving from FWi mapping to Mosaic
     * HISTORY:     1.0 TW 09/09/2013 Created
	 *				--- Reports Repository 5.0.0.0-----------------------------------------------------------------------	 
	 *				1.1 TW 31/10/2013 Removed insert into report_switches for 'Child Visit Setup' as values will already exist
	 *				--- Reports Repository 5.0.0.1-----------------------------------------------------------------------	 
	 *				1.2 TW 25/11/2013 CORM-1498: added mappings for segments detail
	 *				1.3 TW 28/11/2013 CORM-1678: Added additional delete statements to remove reports where report class no longer exists.
	 *				1.4 TW 09/12/2013 CORM-1498: Added update statement to convert old questionnaire and form filter mapping.
	 *				1.5 TW 09/01/2014 CORM-1498: Added coalesce to report_filter_mappings update statement to stop column being updated to null.		  
	 *				--- Reports Repository 5.2.0.0-----------------------------------------------------------------------	 
     */
	cursor c_already_exists(tab_name varchar) is
		select 1
		from   user_tables tab
		where  tab.table_name = upper(tab_name);
	already_exists c_already_exists%rowtype;
	v_mos_version_exists varchar2(1);	
	--
begin
	--
    -- mos version already run?	
    select max('Y') into v_mos_version_exists
    from dm_mosaic_conversion;
	--
	--	only run conversion if previous version was FWi
	if coalesce(v_mos_version_exists,'N') = 'N' then
	--
    	dbms_output.put_line('Create Backup Tables');
    	open c_already_exists('report_filter_mappings_mbu');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%notfound then
            execute immediate ('create table report_filter_mappings_mbu as select * from report_filter_mappings');
    	end if;
    	close c_already_exists;
    	--
    	open c_already_exists('report_filters_mbu');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%notfound then
            execute immediate ('create table report_filters_mbu as select * from report_filters');
    	end if;
    	close c_already_exists;	
    	--
    	open c_already_exists('report_groups_mbu');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%notfound then
            execute immediate ('create table report_groups_mbu as select * from report_groups');
    	end if;
    	close c_already_exists;	
    	--
    	open c_already_exists('report_group_mappings_mbu');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%notfound then
            execute immediate ('create table report_group_mappings_mbu as select * from report_group_mappings');
    	end if;
    	close c_already_exists;
    	--
    	open c_already_exists('tmp_out_fil_map_conv');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%found then
    		execute immediate ('drop table tmp_out_fil_map_conv cascade constraints');
    	end if;
    	close c_already_exists;
    	--
    	dbms_output.put_line('Create Temp Tables for Outcome Types mapping conversion');
    	open c_already_exists('tmp_out_fil_map_conv');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%notfound then
    		execute immediate ('create table tmp_out_fil_map_conv as
                select 
    				distinct 
                    rfm.report_filter_id,
                    conv.workflow_next_action_type_id
                from
    				report_filters rf
    			inner join report_filter_mappings rfm
    			on rfm.report_filter_id = rf.id
                inner join conversion_next_action_info conv
                on conv.original_outcome_type = rfm.mapped_value
                and
                (
                    rf.source_table in (
                        ''MO_WORKFLOW_NEXT_ACTION_TYPES'',
						''OUTCOME_TYPES'',
                        ''RAP_ASSESS_REVIEW_OUTCOMES_VW'',
                        ''RAP_CARER_OUTCOMES_VW'',
                        ''CHILD_CIN_CLOSURE_OUTCOMES_VW'',
                        ''CHILD_CONTACT_ONLY_OUTCOMES_VW'',
                        ''CHILD_INELIGIBLE_REFERRALS_VW''					
                    )
                    or
                    (
                        rf.source_table = ''CHILD_ADOPTION_BEST_INTERES_VW''
                        and
                        dbo.switch_mapping(''Child Adoption Best Interest'') = ''Episode Outcome''
            		)
                    or
                    (
                        rf.source_table = ''CHILD_ADOPTION_MATCH_VW''
                        and
                        dbo.switch_mapping(''Child Adoption Match'') = ''Episode Outcome''								
            		)				
            	)');
    	end if;
    	close c_already_exists;
    	--
    	open c_already_exists('tmp_out_grp_map_conv');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%found then
    		execute immediate ('drop table tmp_out_grp_map_conv cascade constraints');
    	end if;
    	close c_already_exists;
    	--
    	open c_already_exists('tmp_out_grp_map_conv');
    	fetch c_already_exists into already_exists;
    	if c_already_exists%notfound then
    		execute immediate ('create table tmp_out_grp_map_conv as
                select 
    				distinct 
                    rg.id,
                    rgc.id report_group_category_id,
                    conv.workflow_next_action_type_id
                from 
    				report_groups rg
                inner join report_group_categories rgc
                on rgc.report_group_id = rg.id
                inner join report_group_mappings rgm
                on rgm.report_group_category_id = rgc.id
                inner join conversion_next_action_info conv
                on conv.original_outcome_type = rgm.mapped_value
    			where
                    rg.source_table in (
                    	''MO_WORKFLOW_NEXT_ACTION_TYPES'',
						''OUTCOME_TYPES'',
                    	''RAP_ASSESS_REVIEW_OUTCOMES_VW'',
                    	''RAP_CARER_OUTCOMES_VW'',
                    	''CHILD_CIN_CLOSURE_OUTCOMES_VW'',
                    	''CHILD_CONTACT_ONLY_OUTCOMES_VW'',
                    	''CHILD_INELIGIBLE_REFERRALS_VW'',
    					''CHILD_CP_OUTCOMES_VW''
                    	)			
    			');
    	end if;
    	close c_already_exists;	 
    	--	
    	--Convert Frameworki mappings to Mosaic Equivalents
    	--NB. Episode types are to become 'Workflow Step Types'; Outcomes are to become 'Next Action Types'
    	--
    	dbms_output.put_line('Delete mappings to EPISODE_TYPES not found in the conversion tables');
    	delete from report_filter_mappings
    	where report_filter_id in
    		(select rf.id 
    		from report_filters rf
    			where rf.source_table in ('MO_WORKFLOW_STEP_TYPES','EPISODE_TYPES')
    			and rf.source_column in ('WORKFLOW_STEP_TYPE_ID','TYPE'))
    		and not exists
    			(
    			select 1   		
    			from conversion_step_info csi
    				where 
    				csi.original_table = 'EPISODE_TYPES' 
    				and 
    				csi.original_type = report_filter_mappings.mapped_value);
    	-- 
    	dbms_output.put_line('Convert report filters which use EPISODE_TYPES as a source table');
        update report_filter_mappings rfm
        set rfm.mapped_value = (
        	select
        		workflow_step_type_id 
            from 
        		conversion_step_info csi
            where  
        		csi.original_table = 'EPISODE_TYPES'  
            	and 
        		csi.original_type = rfm.mapped_value
        	)
        where exists
            (
        		select 
        			1 
        		from 
        			report_filters rf 
                where 
        			rf.id = rfm.report_filter_id 
                    and 
        			rf.source_table in ('MO_WORKFLOW_STEP_TYPES','EPISODE_TYPES')
                    and 
        			rf.source_column in ('WORKFLOW_STEP_TYPE_ID','TYPE')
        	);
    	--
    	dbms_output.put_line('Delete mappings to EPISODE_TYPES used in views not found in the conversion tables');
    	delete from report_filter_mappings 
    	where report_filter_id in
    		(select rf.id 
    		from report_filters rf
    			where rf.source_table in ('CHILD_CIN_EPISODES_VW', 'CHILD_CIN_NON_CLO_EPI_VW', 'ADULTS_EPISODE_TYPES_VW','ADULTS_AVA_COMPLETED_REF_VW', 'CHILD_PATHWAY_PLAN_TYPES_VW'))
    		and not exists
    			(
    			select 1   		
    			from conversion_step_info csi
    				where 
    				csi.original_table = 'EPISODE_TYPES' 
    				and 
    				csi.original_type = report_filter_mappings.mapped_value);
    	--
    	dbms_output.put_line('Convert report filters which use a view containing episode types');
        update report_filter_mappings rfm
        set rfm.mapped_value = (
        	select 
        		workflow_step_type_id 
            from 
        		conversion_step_info csi
            where 
        		csi.original_table = 'EPISODE_TYPES' 
            	and 
        		csi.original_type = rfm.mapped_value
        	)
        where exists (
        	select 
        		1 
        	from 
        		report_filters rf 
            where 
        		rf.id = rfm.report_filter_id 
                and 
        		rf.source_table in ('CHILD_CIN_EPISODES_VW', 'CHILD_CIN_NON_CLO_EPI_VW', 'ADULTS_EPISODE_TYPES_VW','ADULTS_AVA_COMPLETED_REF_VW', 'CHILD_PATHWAY_PLAN_TYPES_VW'));
        --
    	dbms_output.put_line('Alter the definition of the filters from EPISODE_TYPES to Workflow Step Types');
        update report_filters
        set 
        	source_table = 'MO_WORKFLOW_STEP_TYPES',
        	source_column = 'WORKFLOW_STEP_TYPE_ID'
        where 
        	source_table = 'EPISODE_TYPES'
        	and 
        	source_column = 'TYPE';
        
        update report_filters
        set 
        	source_table = 'MO_WORKFLOW_STEP_TYPES'
        where 
        	source_table = 'EPISODE_TYPES'
        	and 
        	source_column = 'DESCRIPTION';
    	--
    	dbms_output.put_line('remove mapping to EPISODE_TYPES not found in the conversion tables');
    	delete from report_group_mappings
    	where report_group_category_id in
    		(select rgc.id 
    		from report_group_categories rgc
    		inner join report_groups rg
    		on rg.id = rgc.report_group_id
    			where rg.source_table in ('MO_WORKFLOW_STEP_TYPES','EPISODE_TYPES'))
    	and not exists
    		(
    			select 1   		
    			from conversion_step_info csi
    				where 
    				csi.original_table = 'EPISODE_TYPES' 
    				and 
    				csi.original_type = report_group_mappings.mapped_value);	
    	--
    	dbms_output.put_line('Convert report groups which use EPISODE_TYPES as a source table');
        update report_group_mappings rgm
        set rgm.mapped_value = (
        	select 
        		workflow_step_type_id 
            from 
        		conversion_step_info csi
            where 
        		csi.original_table = 'EPISODE_TYPES' 
        	    and 
        		csi.original_type = rgm.mapped_value
        	)
        where 
        	rgm.report_group_category_id in (
        		select 
        			rgc.id 
        		from 
        			report_group_categories rgc
                inner join report_groups rg
                on rg.id = rgc.report_group_id
                where 
        			rg.source_table in ('MO_WORKFLOW_STEP_TYPES','EPISODE_TYPES'));
    	--
    	dbms_output.put_line('Update the definition of groups which use EPISODE_TYPES as a source table');
        update report_groups
        set 
        	source_table = 'MO_WORKFLOW_STEP_TYPES',
        	source_column = 'WORKFLOW_STEP_TYPE_ID'
        where 
        	source_table = 'EPISODE_TYPES';
    	--
    	dbms_output.put_line('Delete existing mapping to outcome types');
    	delete from report_filter_mappings
    	where
    		exists (
    			select
    				1
    			from
    				report_filters rf
    			where
    				rf.id = report_filter_mappings.report_filter_id
    				and
    				(
        				rf.source_table in (
        					'MO_WORKFLOW_NEXT_ACTION_TYPES',
	    					'OUTCOME_TYPES',
        					'RAP_ASSESS_REVIEW_OUTCOMES_VW',
        					'RAP_CARER_OUTCOMES_VW',
        					'CHILD_CIN_CLOSURE_OUTCOMES_VW',
        					'CHILD_CONTACT_ONLY_OUTCOMES_VW',
        					'CHILD_INELIGIBLE_REFERRALS_VW'
        					)
    					or
    					(
    						rf.source_table = 'CHILD_ADOPTION_BEST_INTERES_VW'
    						and
    						dbo.switch_mapping('Child Adoption Best Interest') = 'Episode Outcome'
    					)
                        or
                        (
                            rf.source_table = 'CHILD_ADOPTION_MATCH_VW'
                            and
                            dbo.switch_mapping('Child Adoption Match') = 'Episode Outcome'
                		)					
    				)	
    			);
    	--
    	dbms_output.put_line('Insert Converted mappings of outcome types');
    	execute immediate ('insert into report_filter_mappings (id,report_filter_id, mapped_value)
    	select
    		rfm_seq.nextval,
    		report_filter_id,
    		workflow_next_action_type_id
    	from tmp_out_fil_map_conv');
    	--
    	
    	dbms_output.put_line('Alter definitions of report filters');
        update report_filters
        set 
        	source_table = 'MO_WORKFLOW_NEXT_ACTION_TYPES',
        	source_column = 'WORKFLOW_NEXT_ACTION_TYPE_ID'
        where 
        	source_table = 'OUTCOME_TYPES';
    	--
    	dbms_output.put_line('Delete existing mapping to outcome types');
    	delete from report_group_mappings
    	where
    		exists (
    			select
    				1
    			from
    				report_group_categories rgc
    			inner join report_groups rg
    			on rg.id = rgc.report_group_id
    			where
    				rgc.id = report_group_mappings.report_group_category_id
    				and
    				rg.source_table in (
						'MO_WORKFLOW_NEXT_ACTION_TYPES',
    					'OUTCOME_TYPES',
    					'RAP_ASSESS_REVIEW_OUTCOMES_VW',
    					'RAP_CARER_OUTCOMES_VW',
    					'CHILD_CIN_CLOSURE_OUTCOMES_VW',
    					'CHILD_CONTACT_ONLY_OUTCOMES_VW',
    					'CHILD_INELIGIBLE_REFERRALS_VW',
    					'CHILD_CP_OUTCOMES_VW'
    					)
    			);
    	--
    	dbms_output.put_line('Insert Converted mappings of outcome types');
    	execute immediate ('insert into report_group_mappings (id,report_group_category_id, mapped_value)
    	select
    		rgm_seq.nextval,
    		report_group_category_id,
    		workflow_next_action_type_id
    	from tmp_out_grp_map_conv');
    	--
    	dbms_output.put_line('Alter definition of report groups');
        update report_groups
        set 
        	source_table = 'MO_WORKFLOW_NEXT_ACTION_TYPES',
        	source_column = 'WORKFLOW_NEXT_ACTION_TYPE_ID'
        where 
        	source_table = 'OUTCOME_TYPES';
    	--
    	dbms_output.put_line('remove mapping to TASK_TYPES not found in the conversion tables');
    	delete from report_filter_mappings
    	where report_filter_id in
    		(select rf.id from report_filters rf
    		where rf.source_table in ('TASK_TYPES','MO_WORKFLOW_TASK_TYPES'))
    	and not exists
    		(select null from conversion_task_info na
    		where na.original_task_code = report_filter_mappings.mapped_value);	
    	--
    	dbms_output.put_line('Convert task mappings');
        update report_filter_mappings rfm
        set 
        rfm.mapped_value = (
            select 
            workflow_task_type_id 
            from 
            conversion_task_info cti
            where 
            cti.original_task_code = rfm.mapped_value)
        where 
    		exists (
                select 
                1 
                from 
                report_filters rf 
                where 
                rf.id = rfm.report_filter_id 
                and 
                rf.source_table in ('TASK_TYPES','MO_WORKFLOW_TASK_TYPES'));
    	--
    	dbms_output.put_line('Alter definition of task type filters');
        update report_filters
        set 
            source_table = 'MO_WORKFLOW_TASK_TYPES',
            source_column = 'WORKFLOW_TASK_TYPE_ID'
        where 
    	    source_table = 'TASK_TYPES';
    	--
    	dbms_output.put_line('remove mapping to TASK_TYPES not found in the conversion tables.');
    	delete from report_group_mappings
    	where report_group_category_id in
    		(select rgc.id from report_group_categories rgc
    		inner join report_groups rg
    		on rg.id = rgc.report_group_id
    		where rg.source_table in ('TASK_TYPES','MO_WORKFLOW_TASK_TYPES'))
    	and not exists
    		(select null from conversion_task_info na
    		where na.original_task_code = report_group_mappings.mapped_value);	
    	--
    	dbms_output.put_line('Convert task type group mappings');
        update report_group_mappings rgm
        set 
    		rgm.mapped_value = (
                select 
                workflow_task_type_id 
                from 
                conversion_task_info cti
                where 
                cti.original_task_code = rgm.mapped_value)
        where 
    		rgm.report_group_category_id in (
                select 
    	            rgc.id
                from 
     				report_group_categories rgc
                inner join report_groups rg
                on rg.id = rgc.report_group_id
                where 
                	rg.source_table in ('TASK_TYPES','MO_WORKFLOW_TASK_TYPES'));
    	--
    	dbms_output.put_line('Alter definition of task type groups');
        update report_groups
        set 
        	source_table = 'MO_WORKFLOW_TASK_TYPES',
        	source_column = 'WORKFLOW_TASK_TYPE_ID'
        where 
        	source_table = 'TASK_TYPES';
    	--
    	dbms_output.put_line('Convert mapping for ''Child CIN Case Closure Reasons'' to use new reason codes');
        update report_group_mappings
        set 
        	mapped_value = coalesce((select 
        								'OUTREAS_'||dbo.to_varchar(con.workflow_reason_type_id)
                                    from 
        								conversion_next_act_reas_info con
                                    where 
        								con.original_reason_type = replace(report_group_mappings.mapped_value,'OUTREAS_'))	
								,mapped_value)	
        where 
    		report_group_category_id in (
    			select 
    				rgc.id
                from 
    				report_groups rg
                inner join report_group_categories rgc
                on rgc.report_group_id = rg.id
                where 
    				name = 'Child CIN Case Closure Reasons')
    	    and 
    		mapped_value like 'OUTREAS%';	
    	--					
    	--
    	dbms_output.put_line('Drop temp tables');
    	execute immediate('drop table tmp_out_fil_map_conv');
    	execute immediate('drop table tmp_out_grp_map_conv');
    	--
    	commit;
		--
		--Insert mapping for segments that have been converted to form questions
        insert into report_filter_mappings (
        	id,
        	report_filter_id,
        	mapped_value
        )
		select
			rfm_seq.nextval,
			x.report_filter_id,
			x.mapped_value
		from
            (
                select ( select rf.id from report_filters rf where rf.name = 'Child CP Conference Actual Date (Document)') report_filter_id, 'SEGMENT_CONFERENCE->conferenceActualDate' mapped_value from dual
                union all
                select ( select rf.id from report_filters rf where rf.name = 'Child CP Conference Planned Date (Document)') report_filter_id, 'SEGMENT_CONFERENCE->plannedDate' mapped_value from dual					
    		) x
		where
			not exists (
				select
					1
				from
					report_filter_mappings rfm
				where
					rfm.report_filter_id = x.report_filter_id
					and
					rfm.mapped_value = x.mapped_value
				);
		--
		commit;		
		--
		-- create mapping for new Mosaic filters from old FWi filters
		--
		-- workflow steps start and end dates
		--
    	insert into report_filter_mappings(id,report_filter_id, mapped_value)
    	select rfm_seq.nextval
    			,rf.id
    			,rfm.mapped_value
    	from report_filter_mappings rfm
    	inner join report_filters rf
    	on rf.name = 'Workflow Step Start Date (Document)'
    	where rfm.report_filter_id in (	select id
                                	from report_filters
                                	where name in ('RAP Episode Start Date (Document)','Child Episode Start Date (Document)'));
    	--    
    	insert into report_filter_mappings(id,report_filter_id, mapped_value)
    	select rfm_seq.nextval
    			,rf.id
    			,rfm.mapped_value
    	from report_filter_mappings rfm
    	inner join report_filters rf
    	on rf.name = 'Workflow Step End Date (Document)'
    	where rfm.report_filter_id in (	select id
                                	from report_filters
                                	where name in ('RAP Episode End Date (Document)','Child Episode End Date (Document)'));
		--
		commit;									
		--
		/*
		delete from report_filter_mappings rfm
		where report_filter_id in (select id
									from report_filters
    								 where name in ('RAP Episode Start Date (Document)','Child Episode Start Date (Document)','RAP Episode End Date (Document)','Child Episode End Date (Document)'));											
		*/									 
		--
		-- Step Source	
		--
		insert into report_filter_mappings(id,report_filter_id, mapped_value)
    	select rfm_seq.nextval
    			,rf.id
    			,rfm.mapped_value
    	from report_filter_mappings rfm
    	inner join report_filters rf
    	on rf.name = 'Step Source Type (Document)'
    	where rfm.report_filter_id in (	select id
                                	from report_filters
                                	where name in ('Adults Episode Source (Document)','Child Episode Source Type (Document)'));
		--
		insert into report_filter_mappings(id,report_filter_id, mapped_value)					
    	select rfm_seq.nextval
    			,rf.id
				,'SEGMENT_REFERRAL->referrerSource'
		from report_filters rf
		where rf.name = 'Step Source Type (Document)';	
		--
		commit;
		--
		-- Step Method
		--
		insert into report_filter_mappings(id,report_filter_id, mapped_value)
    	select rfm_seq.nextval
    			,rf.id
    			,rfm.mapped_value
    	from report_filter_mappings rfm
    	inner join report_filters rf
    	on rf.name = 'Step Source Method (Document)'
    	where rfm.report_filter_id in (	select id
                                	from report_filters
                                	where name in ('Child Episode Source Method (Document)'));
		--							
		insert into report_filter_mappings(id,report_filter_id, mapped_value)
    	select rfm_seq.nextval
    			,rf.id
				,'SEGMENT_REFERRAL->referralMethod'
		from report_filters rf
		where rf.name = 'Step Source Method (Document)';
		--
		-- Presenting Need
		--		
		insert into report_filter_mappings(id,report_filter_id, mapped_value)
    	select rfm_seq.nextval,
    			rf.id,
				'SEGMENT_PRESENTING_NEEDS->issue'
		from report_filters rf
		where rf.name = 'Presenting Need Type (Document)';						
		--					
		commit;											
		--
		-- Convert switch setting from FWi to Mosaic
        update report_switch_mappings
        set switch_value = replace(switch_value,'Episodes','Steps')
        where switch_name = 'Child Visit Setup'
        and lower(switch_value) like '%episodes%';
        --
        delete from report_switches
        where switch_name = 'Child Visit Setup'
        and lower(switch_value) like '%episodes%';
		--
		commit;
		--
        -- Delete user reports that are no longer part of tree structure
		-- (To stop error in admin tool when building tree)
        -- report parameters
        delete from report_parameters
        where (id >= 1000000 or report_id >= 1000000)
        and report_id in (select id
                            from reports
                            where id >= 1000000
                            and report_class_id	in (select id
                            						from report_classes
                            						where parent_class_id not in (select id from report_classes)));
        -- report sections
        delete from report_sections 
        where (id >= 1000000 or report_id >= 1000000)
        and report_id in (select id
                            from reports
                            where id >= 1000000
                            and report_class_id	in (select id
                            						from report_classes
                            						where parent_class_id not in (select id from report_classes)));
        -- reports											
        delete from reports
        where id >= 1000000
        and report_class_id	in (select id
        						from report_classes
        						where parent_class_id not in (select id from report_classes));
        
        -- report classes roles
        delete from report_classes_roles
        where report_class_id in (select id
        						from report_classes
        						where parent_class_id not in (select id from report_classes));
        
        -- report classes
        delete from report_classes
        where parent_class_id not in (select id from report_classes);
		--
		commit;
		--		
        insert into dm_mosaic_conversion(converted,converted_on)
        values ('Y',sysdate);		
		--
		commit;
		--		
	end if;
	--
	--
	-- additional segment detail mappings
	--
	-- dm_workflow_steps.lac_review_date
    insert into report_filter_mappings(id,report_filter_id, mapped_value)
    select rfm_seq.nextval,
    		rf.id,
    		'SEGMENT_DISCUSSION->discussionDate'
    from report_filters rf
    where rf.name = 'Child LAC Review Date'		
    -- CONDITION: value not already inserted
    and not exists (select 1
    				from report_filter_mappings rfm
    				where rfm.report_filter_id = rf.id
    				and rfm.mapped_value = 'SEGMENT_DISCUSSION->discussionDate');
    --
    insert into report_filter_mappings(id,report_filter_id, mapped_value)
    select rfm_seq.nextval,
    		rf.id,
    		'SEGMENT_CONFERENCE->plannedDate'
    from report_filters rf
    where rf.name = 'Child LAC Review Date'	
    -- CONDITION: value not already inserted
    and not exists (select 1
    				from report_filter_mappings rfm
    				where rfm.report_filter_id = rf.id
    				and rfm.mapped_value = 'SEGMENT_CONFERENCE->plannedDate');    
    --
    insert into report_filter_mappings(id,report_filter_id, mapped_value)
    select rfm_seq.nextval,
    		rf.id,
    		'SEGMENT_CONFERENCE->conferenceActualDate'
    from report_filters rf
    where rf.name = 'Child LAC Review Date'
    -- CONDITION: value not already inserted
    and not exists (select 1
    				from report_filter_mappings rfm
    				where rfm.report_filter_id = rf.id
    				and rfm.mapped_value = 'SEGMENT_CONFERENCE->conferenceActualDate');
    --    
    --dm_workflow_steps.lac_review_participation_code
    insert into report_filter_mappings(id,report_filter_id, mapped_value)
    select rfm_seq.nextval,
    		rf.id,
    		'SEGMENT_CONFERENCE->participationCode'
    from report_filters rf
    where rf.name = 'Child LAC Review Method of Participation'	
    -- CONDITION: value not already inserted
    and not exists (select 1
    				from report_filter_mappings rfm
    				where rfm.report_filter_id = rf.id
    				and rfm.mapped_value = 'SEGMENT_CONFERENCE->participationCode');	
    --    
    --dm_filter_form_answers 'Child LAC Review Attendee'
    insert into report_filter_mappings(id,report_filter_id, mapped_value)
    select rfm_seq.nextval,
    		rf.id,
    		'SEGMENT_CONFERENCE->involvedPartyTableCompositeId'
    from report_filters rf
    where rf.name = 'Child LAC Review Attendee'
    -- CONDITION: value not already inserted
    and not exists (select 1
    				from report_filter_mappings rfm
    				where rfm.report_filter_id = rf.id
    				and rfm.mapped_value = 'SEGMENT_CONFERENCE->involvedPartyTableCompositeId');		
	--
	commit;	
	--
	--
	-- additional delete statements to remove reports where the report class no longer exists
	--
    -- report parameters
    delete from report_parameters		
    where (id >= 1000000 or report_id >= 1000000)
    and report_id in (select id
                        from reports
                        where id >= 1000000
                        and report_class_id	not in (select id
                        							from report_classes));
    -- report sections
    delete from report_sections 
    where (id >= 1000000 or report_id >= 1000000)
    and report_id in (select id
                        from reports
                        where id >= 1000000
                        and report_class_id	not in (select id
                        							from report_classes));
    -- reports											
    delete from reports
    where id >= 1000000
    and report_class_id	not in (select id
    							from report_classes);
    
    -- report classes roles
    delete from report_classes_roles
    where report_class_id not in (select id
    								from report_classes);	
	--
	commit;
	--
	-- convert old questionnaire mappings
    update report_filter_mappings
    	set mapped_value = coalesce(
							(select 
                    			max(dbo.append2(mt.template_user_code,'->',mq.question_user_code))        		
                            from conversion_qnr_question_info conv        
                            inner join mo_templates mt
                            on mt.template_id = conv.new_template_id
                            inner join mo_questions mq
                            on mq.question_id = conv.new_question_id
                            where
                            	-- CRITERIA: a questionnaire mapped value 
                    			'QST_'||conv.old_questionnaire_id||'->QST_'||conv.old_question_id = report_filter_mappings.mapped_value
                            	)
							,mapped_value)	
    where 
    	mapped_value like 'QST%';
	--
	commit;
	--		
	-- convert old form mappings 
    update report_filter_mappings
    set mapped_value = coalesce(
						(select max(conv.old_fty_id_code||'->'||conv.new_question_user_code)
    			  		from conversion_question_info conv	
    			  		where 
    			  		--CRITERIA: old mapping exists
    			  		conv.old_fty_id_code||'->'||conv.old_sit_tag_id = report_filter_mappings.mapped_value
    					and 
    					--CRITERIA: new mapping doesn't exist
    					conv.old_fty_id_code||'->'||conv.new_question_user_code != report_filter_mappings.mapped_value)
						,mapped_value)	
    where exists (select 1
    			  from conversion_question_info conv	
    			  where 
    			  		--CRITERIA: old mapping exists
    			  		conv.old_fty_id_code||'->'||conv.old_sit_tag_id = report_filter_mappings.mapped_value
    					and 
    					--CRITERIA: new mapping doesn't exist
    					conv.old_fty_id_code||'->'||conv.new_question_user_code != report_filter_mappings.mapped_value);	
	--
	commit;
	--								
end;
/
