--SQL*Loader Control file to populate IMU_REPORT_FILTERS table
--1.0  kt  11/8/2005 initial version
--1.1  kt  14/2/2006 amended for repository release 2.1.1
--1.2  djm 01/05/2008 YKAP 21373: Removed hard-coded paths
Load Data
replace into table imu_report_filters
fields terminated by '^' optionally enclosed by '"'
trailing nullcols
(
RFM_ID					integer external,
RFM_MAPPED_VALUE		char(255),
RFM_REPORT_FILTER_ID	integer external,
RF_CONTEXT				char,
RF_DESCRIPTION			char(4000),
RF_DISPLAY_COLUMN		char,
RF_FORM_PAGE_NUMBER     integer external,
RF_FORM_TYPE_ID         integer external,
RF_ID					integer external,
RF_ITEM_ID				integer external,
RF_NAME					char(255),
RF_QUESTION_ID			integer external,
RF_QUESTIONNAIRE_ID		integer external,
RF_SELECTION_COLUMN		char,
RF_SELECTION_VALUE		char,
RF_SOURCE_COLUMN		char,
RF_SOURCE_TABLE			char
)