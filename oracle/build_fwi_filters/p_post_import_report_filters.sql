declare

/*
 * NAME:        p_post_import_report_filters
 * DESCRIPTION: This procedure must be run after data has been imported into the IMU_REPORT_FILTERS table.
 *              This procedure will populate the following tables
 *                  REPORT_FILTERS
 *                  REPORT_FILTER_MAPPINGS (where mapping has previously been set up in rap_filter_values)
 * HISTORY:		1.0  kt  3/6/2005	initial version
 *				1.1  kt  19/7/2005	added distinct to cursor
 *				1.2  kt  13/9/2005	added update statements
 *				1.3  kt  26/1/2006	incorporates mappings from forms, removed copy from old mapping tables
 *				1.4  kt  10/2/2006	added check 
 *				1.5  kt  22/2/2006	added updates for source_column and source_table
 *									insert into report_filters.id from sequence
 *				1.6  djm 15/3/2006	removed unwanted ampersand from above comment
 *				1.7  djm 01/05/2008	YKAP 21373: Appended trailing slash and Exit
 */


BEGIN

	--Insert new Report_Filters 
	insert into report_filters	(
								CONTEXT
								,DESCRIPTION
								,DISPLAY_COLUMN
								,FORM_PAGE_NUMBER
								,FORM_TYPE_ID
								,ID
								,ITEM_ID
								,NAME
								,QUESTION_ID
								,QUESTIONNAIRE_ID
								,SELECTION_COLUMN
								,SELECTION_VALUE
								,SOURCE_COLUMN
								,SOURCE_TABLE
								)
						select
								RF_CONTEXT
								,f_convert_from_symbols(RF_DESCRIPTION)
								,RF_DISPLAY_COLUMN
								,RF_FORM_PAGE_NUMBER
								,RF_FORM_TYPE_ID
								,RF_SEQ.NEXTVAL
								,RF_ITEM_ID
								,RF_NAME
								,RF_QUESTION_ID
								,RF_QUESTIONNAIRE_ID
								,RF_SELECTION_COLUMN
								,RF_SELECTION_VALUE
								,RF_SOURCE_COLUMN
								,RF_SOURCE_TABLE
						from
							imu_report_filters
						where 
							rf_name not in
								(
								select name
								from report_filters
								);

	--update description
	update report_filters rf
	set rf.description =		(
								select distinct f_convert_from_symbols(rf1.rf_description)
								from imu_report_filters rf1
								where rf1.rf_name = rf.name
								)
	where
		rf.name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			);

	--update source_table
	update report_filters rf
	set rf.source_table =	(
								select distinct rf1.rf_source_table
								from imu_report_filters rf1
								where rf1.rf_name = rf.name
								)
	where
		rf.name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			);
			
	--update source_column
	update report_filters rf
	set rf.source_column =	(
								select distinct rf1.rf_source_column
								from imu_report_filters rf1
								where rf1.rf_name = rf.name
								)
	where
		rf.name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			);

	--update selection_column
	update report_filters rf
	set rf.selection_column =	(
								select distinct rf1.rf_selection_column
								from imu_report_filters rf1
								where rf1.rf_name = rf.name
								)
	where
		rf.name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			);

	--update selection_value
	update report_filters rf
	set rf.selection_value =	(
								select distinct rf1.rf_selection_value
								from imu_report_filters rf1
								where rf1.rf_name = rf.name
								)
	where
		rf.name in
			(
			select rf2.rf_name
			from imu_report_filters rf2
			);

	commit;
								
	delete from imu_report_filters;

	commit;

end;
/
exit


