create or replace view report_logs_redo_vw as
select
	rlo.step       step,
	to_number(rlo2.line) - to_number(rlo.line)
	               redo_step_kb,
	to_number(rlo.line)
			       redo_script_kb,
	rlo.routine    routine,
	rlo.order_by   order_by,
	rlo.time_stamp time_stamp,
	case
		when coalesce(rlo.step, '') = '' then
			dbo.to_numeric(null)
		when dbo.secs(rlo.time_stamp) <= dbo.secs(coalesce(rlo2.time_stamp, sysdate)) then
			-- Same side of midnight
			(dbo.secs(coalesce(rlo2.time_stamp, sysdate)) - dbo.secs(rlo.time_stamp))
		else
			-- Straddles midnight; 86400 = secs per day
			86400 + (dbo.secs(coalesce(rlo2.time_stamp, sysdate)) - dbo.secs(rlo.time_stamp))
	end            secs,
	case
		when rlo.line  like 'Error:%'
		or   rlo2.line like 'Error:%' then
			'Error'
	end            status
from
	report_logs rlo
inner join report_logs rlo2
on rlo2.order_by > rlo.order_by
and rlo2.routine = rlo.routine
and rlo2.step like 'Redo_%'
--and dbo.to_numeric(rlo2.line) >= dbo.to_numeric(rlo.line)
and rlo2.order_by = (
		select
			min(rlo3.order_by)
		from
			report_logs rlo3
		where
			rlo3.order_by > rlo.order_by
			and
			rlo3.routine = rlo.routine
			and
			rlo3.step like 'Redo_%'
--			and
--			dbo.to_numeric(rlo3.line) >= dbo.to_numeric(rlo.line)
)
where
	rlo.routine = 'Populate_Package'
	and
	rlo.step like 'Redo_%'
	and
	--CRITERIA: Exclude misleading results
	dbo.to_numeric(rlo2.line) >= dbo.to_numeric(rlo.line)
	and
	dbo.to_numeric(rlo.line) > (dbo.to_numeric(rlo2.line) - dbo.to_numeric(rlo.line))
