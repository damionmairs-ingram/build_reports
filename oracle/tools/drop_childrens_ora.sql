begin
/*
 * NAME:		drop_childrens_ora.sql
 * DESCRIPTION:	For *DEVELOPER* use only
 *				!!! Do not use on a Customer's database !!!
 * 				Drops all Children's datamart tables, filters, groups and switches
 * HISTORY:     1.0 djm 22/08/2008 FRA-921: initial version
 */
	--
	-- Filters -------------------------------------------------------------------
	--
	-- Delete DEV filter mappings
	delete from report_filter_mappings
	where
		report_filter_id in (
			select
				rf.id
			from
				report_filters rf
			where
				upper(rf.description) like '[DEV3.0%'
		)
	;
	--
	-- Delete DEV filters
	delete from report_filters
	where
		upper(description) like '[DEV3.0%'
	;
	--
	-- Delete Filter/Group Mapping views
	delete from report_filter_group_tables
	where
		table_name like 'CHILD%VW'
	;
	--
	-- Groups --------------------------------------------------------------------
	--
	-- Delete DEV group mappings
	delete from report_group_mappings
	where
		report_group_category_id in (
			select
				rgc.id
			from
				report_groups rg
			inner join report_group_categories rgc
			on rgc.report_group_id = rg.id
			where
				upper(rg.description) like '[DEV3.0%'
		)
	;
	--
	-- Delete DEV group categories
	delete from report_group_categories
	where
		report_group_id in (
			select
				rg.id
			from
				report_groups rg
			where
				upper(rg.description) like '[DEV3.0%'
		)
	;
	--
	-- Delete DEV groups
	delete from report_groups
	where
		upper(description) like '[DEV3.0%'
	;
	--
	-- Switches ------------------------------------------------------------------
	--
	-- Switch Mappings
	delete from report_switch_mappings
	where
		switch_name like 'Child %'
	;
	--
	-- Switches
	delete from report_switches
	where
		switch_name like 'Child %'
	;
	--
	-- Need Commit in case no DDL is executed
	commit;
	--
	-- Objects -------------------------------------------------------------------
	--
	-- Tables with Standard names
	for del in (
			select tab.table_name
			from
				user_tables tab
			where
				substr(tab.table_name, 1, 5) = 'DM_C_'
				and
				tab.temporary = 'N'
	) loop
		execute immediate 'drop table '||del.table_name;		
	end loop;
	--
	-- Other Objects
	for del in (
			select
				obj.object_name,
				obj.object_type
			from
				user_objects obj
			where
				obj.object_name||'.'||obj.object_type in (
					'CHILD_EDUCATION_RESULTS_VW.VIEW',
					'DM_EXTERNAL_EDUCATION.TABLE',
					'DM_EXT_EDU_SEQ.SEQUENCE'
				)
	) loop
		execute immediate 'drop '||del.object_type||' '||del.object_name;
	end loop;
end;