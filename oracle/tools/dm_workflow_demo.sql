-- These should all match if populated correctly
select 'EPI', count(1)
from episodes epi
union all
select 'FWD', count(distinct del.episode_id)
from dm_episode_forwards del
union all
select 'BWD', count(distinct del.episode_id)
from dm_episode_backwards del
union all
select 'WRK', count(1)
from dm_episode_workflows
/
--
-- Demo usage
--
-- List the first Episode in each Workflow
select
	distinct
	fwd.workflow_id
from
	dm_episode_forwards fwd
-- or
select
	distinct
	fwd.episode_id
from
	dm_episode_forwards fwd
where
	fwd.workflow_depth = 0
--
-- List all Episodes earlier in Workflow
select
	bwd.preceding_episode_id
from
	dm_episode_backwards bwd
where
	bwd.episode_id = 8075161
--
-- List all Episodes later in Workflow
select
	fwd.subsequent_episode_id
from
	dm_episode_forwards fwd
where
	fwd.episode_id = 8075146
--
-- Find Contact episode earlier in Workflow
select
	epi.id
from
	dm_episode_backwards bwd
inner join episodes epi
on epi.id = bwd.preceding_episode_id
where
	bwd.episode_id = 8075161
	and
	epi.type in (
		select
			rfm.mapped_value
		from
			report_filters rf
		inner join report_filter_mappings rfm
		on rfm.report_filter_id = rf.id
		where
			rf.name = 'Contacts'
	)
--
-- Find Assessment episode later in Workflow
select
	epi.id
from
	dm_episode_forwards fwd
inner join episodes epi
on epi.id = fwd.subsequent_episode_id
where
	fwd.episode_id = 8075146
	and
	epi.type in (
		select
			rfm.mapped_value
		from
			report_filters rf
		inner join report_filter_mappings rfm
		on rfm.report_filter_id = rf.id
		where
			rf.name = 'Adult Assessments'
	)
