-- This is a template of a *simple* Oracle procedure for Children's Datamart
-- It populates two lookup tables and a detail table
-- Additional steps are required for build tables or external data
--    Build tables should be truncated at the start, populated immediately before the Sync, and then truncated again. They should also be truncated in the Exception handler.
--    External tables typically just require an analyze at the start
-- Examples of both are already available.
--
-- NB. Square-brackets are used to identify bits that definitely need changing
	--
	-- [procedure_name] ---------------------------------------------------------
	--
	procedure [procedure_name] is
		SCRIPT	constant varchar(30) := 'Populate_Package';
		STEP	constant varchar(30) := '[procedure_name]';
		--
		-- Example Switch usage, where appropriate
		-- NB. There are various ways to use a Switch: in SQL or if-condition, etc.
		switch_name  constant varchar(30) := '[switch_name]';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_header(SCRIPT, STEP);
		dbo.drop_table_indexes(SCRIPT, STEP, '[lookup_table_name, lookup_table_name2, detail_table_name]', 'IF_EMPTY');
		--
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		-- NB. Process lookup table(s) first
		dbo.log_line(SCRIPT, STEP, 'Synchronise [lookup_table_name]');
		dbo.synchronise_table('[lookup_table_name]',
							'[source_table or (inline_view)]',
							'[filter destination if more than one Sync reqd]',
							'[TEMP only if reqd for performance]');
		--
		dbo.log_line(SCRIPT, STEP, 'Synchronise [lookup_table_name2]');
		dbo.synchronise_table('[lookup_table_name2]',
							'[source_table or (inline_view)]',
							'[filter destination if more than one Sync reqd]',
							'[TEMP only if reqd for performance]');
		--
		-- NB. Process detail table(s) afterwards, usually using lookups for flags, etc.
		dbo.log_line(SCRIPT, STEP, 'Synchronise [detail_table_name]');
		dbo.synchronise_table('[detail_table_name]',
							'[source_table or (inline_view)]',
							'[filter destination if more than one Sync reqd]',
							'[TEMP only if reqd for performance]');
		--
		-- Explicit Commit may be required
		commit;
		--
		dbo.create_table_indexes(SCRIPT, STEP, '[lookup_table_name, lookup_table_name2, detail_table_name]', 'IF_NOT_EMPTY');
		dbo.analyze_tables(SCRIPT, STEP, '[lookup_table_name, lookup_table_name2, detail_table_name]');
		--
		dbo.log_tail(SCRIPT);
	exception
		when others then
			dbo.log_line(SCRIPT, STEP, dbo.append('Error: ', sqlerrm));
			-- Abort neatly
			if sqlcode = -1013 then
				raise; -- User Cancelled
			else
				dbo.create_table_indexes(SCRIPT, STEP, '[lookup_table_name, lookup_table_name2, detail_table_name]', 'IF_NOT_EMPTY');
				dbo.analyze_tables(SCRIPT, STEP, '[lookup_table_name, lookup_table_name2, detail_table_name]');
			end if;
	end;
