DECLARE
	/*
	 * Convert_Filter_to_RFQM.sql
	 * Changes a filter based on Items to one based on Report_Form_Question_Mapping
	 * 1.0 DJM 13/04/2007 Created
	 */
	-- List filters to be changed
	CURSOR c_filters IS
		SELECT
			rf.id,
			rf.form_type_id_code
		FROM
			report_filters rf
		WHERE
			--CRITERIA: Name of filter to be changed !!!!!!!!!!!!!!!!!!!!!!!!!!
			rf.NAME = 'xxx'
			AND
			rf.source_table = 'ITEMS'
		FOR UPDATE OF
			rf.form_type_id_code,
		    rf.source_table,
			rf.source_column,
			rf.display_column
		;
BEGIN
	-- Loop through filters to be changed
	FOR filter IN c_filters LOOP
		-- Convert Filter Mappings
		UPDATE report_filter_mappings
		SET    mapped_value = dbo.append2(filter.form_type_id_code, '->', mapped_value)
		WHERE  report_filter_id = filter.id
		;
		-- Convert Filter
		UPDATE	report_filters
		SET		form_type_id_code = NULL,
				source_table = 'REPORT_FORM_QUESTION_MAPPING',
				source_column = 'ID',
				display_column = 'DESCRIPTION'
		WHERE	CURRENT OF c_filters
		;
	END LOOP;
END;
/
-- Check they have been changed OK
SELECT
	rf.NAME,
	rf.form_type_id_code,
	rfm.mapped_value,
	rfqm.description
FROM
	report_filters rf
inner join report_filter_mappings rfm
ON rfm.report_filter_id = rf.id
left outer join report_form_question_mapping rfqm
ON rfqm.id = rfm.mapped_value
WHERE
	rf.NAME = 'xxx'
/
