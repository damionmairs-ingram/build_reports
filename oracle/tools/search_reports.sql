set serverout on size 1000000
set linesize 255
set define on
set trimout on
/*
	Search reports for specified string (NB. allows embedded wildcards)
	DJM 14/11/2005 Created - while waiting for PC to finish installing  :-(
	DJM 25/04/2006 Added Folder hierarchy
	DJM 03/10/2006 Search Report Parameters as well
*/
declare
	search_string VARCHAR2(132) := upper('%&search_arg%');
	
	-- List all reports
	cursor c_reports is
		select rep.id         ID,
		       rep.title      TITLE,
		       rep.report_class_id
		                      FOLDER_ID
		from   reports rep
		where  rep.id < 1000000
		and    SEARCH_STRING != '%%' -- Just in case !
		order by rep.title;
	
	-- List current report's parameters
	cursor c_parameters(p_report_id number) is
		select par.label      LABEL,
		       par.list_sql   LIST_SQL
		from   report_parameters par
		where  par.report_id = P_REPORT_ID
		and    upper(par.list_sql) like SEARCH_STRING
		order by par.id;
	
	-- List all reports' sections
	cursor c_sections (p_report_id number) is
		select sect.title     TITLE,
		       sect.sql_text  SQL_TEXT
		from   report_sections sect
		where  sect.report_id = P_REPORT_ID
		order by sect.id;
		
	cursor c_folders(first_class_id number) is
		select cls.description DESCRIPTION
		from   report_classes cls
		start with cls.id = FIRST_CLASS_ID
		connect by prior cls.parent_class_id = cls.id
		order by level desc;
	folder_path varchar2(4000);
		
	-- Parse SQL lines
	NEWLINE CONSTANT VARCHAR2(1) := CHR(10);
	
	line_counter INTEGER;
	start_of_line NUMBER(6);
	end_of_line NUMBER(6);
	curr_line varchar2(4000);
	
	first_shown boolean;
begin
	for report in c_reports loop
		first_shown := true;
		
		-- Check Report's parameters
		for parameter in c_parameters(report.id) loop
			if first_shown then
				-- Display Report Info
				dbms_output.put_line('----------');
				dbms_output.put_line('Report  '''||report.title||''' ('||to_char(report.id)||')');

				-- Display Folder Path
				folder_path := null;
				for folder in c_folders(report.folder_id) loop
					folder_path := folder_path||'/'||folder.description;
				end loop;
				dbms_output.put_line(substr('Path    '''||ltrim(folder_path, '/')||'''', 1, 255));
			
				first_shown := false;
			end if;	
		
			dbms_output.put_line(substr('Param   '''||parameter.label||''''/*||': '||parameter.list_sql*/, 1, 255));
		end loop;
	
		-- Check Report's sections
		for section in c_sections(report.id) loop
			-- 'Quick' check to see if search_string is contained in section's SQL
			if upper(section.sql_text) like search_string then			
				if first_shown then
					-- Display Report Info
					dbms_output.put_line('----------');
					dbms_output.put_line('Report  '''||report.title||''' ('||to_char(report.id)||')');

					-- Display Folder Path
					folder_path := null;
					for folder in c_folders(report.folder_id) loop
						folder_path := folder_path||'/'||folder.description;
					end loop;
					dbms_output.put_line(substr('Path    '''||ltrim(folder_path, '/')||'''', 1, 255));
			
					first_shown := false;
				end if;
				
				dbms_output.put_line(substr('Section '''||section.title||'''', 1, 255));
				
				-- Parse SQL Text lines, to find which rows contain search_string
				line_counter := 1;
				end_of_line := 0;

				loop
					start_of_line := end_of_line + 1;
    					end_of_line := instr(section.sql_text, NEWLINE, start_of_line);
    	
					EXIT WHEN end_of_line = 0;

					curr_line := substr(section.sql_text, start_of_line, (end_of_line - start_of_line));
				
					if upper(curr_line) like search_string then
						-- Shorten line for dbms_output
						dbms_output.put_line(lpad(to_char(line_counter), 5, '.')||' '||substr(curr_line, 1, 74));
					end if;

					line_counter := line_counter + 1;
				end loop;
			end if;
		end loop;
	end loop;
end;
/