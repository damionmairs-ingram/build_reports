DECLARE
/*
 * identify_old_questions.sql
 * Identify old/inconsistent questions
 * 1.0 DJM 13/04/2007 Created
 *
 * NB. Only looks at Questions on 1st Panel level !
 */
	v_question_filter VARCHAR(132) := 'depression';
	--
	CURSOR c_old_versions IS
		SELECT
			fty.id_code,
			fty.id,
			fty.title,
			Count(1)
		FROM
			form_types fty
		--JOIN: Only old versions that have been used !
		inner join forms frm
		ON frm.form_type_id = fty.id
		WHERE
			--CRITERIA: Form Type !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			fty.id_code = 'FORM1155827498869'
			AND
			--CRITERIA: Exclude current version
			fty.id NOT IN (
				SELECT
					fta.form_type_id
				FROM
					form_types_active fta
			)
		GROUP BY
			fty.id_code,
			fty.id,
			fty.title
		;
	CURSOR c_sections (p_form_type_id NUMBER) IS
		select
			fts.id              ID,
			title_txt.text_data TITLE
		from
			form_type_sections fts
		--JOIN: Section Title, if present
		left outer join section_items title_sit
		on title_sit.section_id = fts.id
		and title_sit.section_level_flag = 'Y'
		and title_sit.type_code = 'LEGEND'
		and title_sit.to_date is null
		left outer join items title_itm
		on title_itm.id = title_sit.item_id
		left outer join text_items title_txt
		on title_txt.id = title_itm.text_item_id
		WHERE
			fts.form_type_id = p_form_type_id
			and
			--CRITERIA: Exclude Panel Titles
			not exists (
				select 'x'
				from   panel_item_contents parent_pic
				where  parent_pic.section_item_id = title_sit.id
			)
		order by
			fts.page_num,
			title_sit.sequence
		;
	CURSOR c_panels(p_section_id NUMBER) is
		select
			pit.id               ID,
			case
				when pit.description = 'unnamed' then
					'[Panel]'
				else
					pit.description
				end TITLE
		from
			section_items sit
		inner join items itm
		on itm.id = sit.item_id
		inner join panel_items pit
		on pit.id = itm.panel_item_id
		where
			sit.section_id = p_section_id
		;
		CURSOR c_questions (p_panel_id NUMBER) is
			select
				sit.tag_id,
				rtrim(coalesce(ltrim(qst.question_text), ltrim(com.label), '[Question]')) question_text,
				coalesce(qst.answer_type, com.answer_type) answer_type
			from
				panel_item_contents parent_pic
			inner join section_items sit
			on sit.id = parent_pic.section_item_id
			inner join items itm
			on itm.id = sit.item_id
			and (
					itm.question_id is not null
					or
					itm.component_id is not null
			)
			--JOIN: Question/Component details
			left outer join questions qst
			on qst.id = itm.question_id
			left outer join components com
			on com.id = itm.component_id
			where
				parent_pic.panel_item_id = p_panel_id
				AND
				Upper(rtrim(coalesce(qst.question_text, com.label, '[Question]'))) LIKE '%'||Upper(v_question_filter)||'%'
		;
BEGIN
	FOR old_version IN c_old_versions LOOP
		Dbms_Output.put_line(dbo.append4(old_version.id_code, ';	', dbo.to_varchar(old_version.id), ';	', old_version.title));
		FOR sect IN c_sections(old_version.id) LOOP
			Dbms_Output.put_line(dbo.append3('.	', dbo.to_varchar(sect.ID), ';	', sect.title));
			FOR panel IN c_panels(sect.ID) LOOP
				Dbms_Output.put_line(dbo.append3('.		', dbo.to_varchar(panel.ID), ';	', panel.title));
				FOR question IN c_questions(panel.ID) LOOP
					Dbms_Output.put_line(dbo.append7('.			', old_version.id_code, '->', question.tag_id, ';	', question.question_text, ';	', question.answer_type));
				END LOOP;
			END LOOP;
		END LOOP;
	END LOOP;
END;
/
INSERT INTO report_filter_mappings (id, report_filter_id, mapped_value)
SELECT
	rfm_seq.NEXTVAL,
	rf.id,
	'FORM1155827498869->B17D1F71-1BB0-3187-A13C-3001E7AF7713'
FROM report_filters rf
WHERE rf.NAME = 'POPP Monthly Activity Sheet(Can dress alone?)'
/