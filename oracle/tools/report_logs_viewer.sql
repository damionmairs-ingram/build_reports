select
/*
 * NAME:		report_logs_viewer.sql
 * DESCRIPTION:	Displays logs from p_populate_report_tables
 * HISTORY:		1.0 djm 23/03/2006	Inititial Version
 *         		1.1 djm 15/05/2006	Include Populate_Package
 *         		1.2 djm 12/06/2006	SQLServer-friendly
 *         		1.3 djm 13/12/2006	Allow for running over midnight
 *         		2.0 djm 16/11/2007	ykap 18760: Converted to use Report_Logs_vw
 */
	rlo."TimeStamp",
	rlo."Secs",
	rlo."Status",
	rlo."Step",
	rlo."Line"
from
	report_logs_vw rlo
where
	rlo.routine = 'Populate_Package'
-- and rlo."Status" is not null -- Rows without a null status
--order by 2 desc -- for Perfomance testing
order by rlo.order_by -- Chronological, oldest first
-- order by rlo.order_by desc -- Reverse Chronological, most recent first. Can use PL/SQL Developer's Auto Refresh