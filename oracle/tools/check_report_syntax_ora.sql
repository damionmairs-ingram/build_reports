-- Tables to hold results of Syntax Check
drop table report_syntax_check
/
create table report_syntax_check (
	class_id          number(9)    not null,
	class_description varchar(80)  not null,
	report_id         number(9)    not null,
	report_title      varchar(80)  not null,
	sql_id            number(9)    not null,
	sql_type          varchar(10)  not null,
	sql_name          varchar(80)  not null,
	sql_original      clob         not null,
	sql_parsed        clob         not null, -- To debug this script !
	errors            varchar(255),
	constraint report_syntax_check_pk
	primary key (report_id, sql_id, sql_type)
)
/
declare
/*
 * Reports' Syntax Checker
 * DJM 30/03/2007 Created
 * DJM 04/09/2008 Check parameter syntax as well
 */
	-- List all Classes under 'Frameworki Reports Repository'
	cursor c_classes is
		select
			cls.id,
			cls.description
		from
			report_classes cls
		start with cls.id = 1
		connect by prior cls.id = cls.parent_class_id
		;
	--
	-- List all Reports
	cursor c_reports(p_class_id number) is
		select
			rep.id,
			rep.title
		from
			reports rep
		where
			rep.report_class_id = p_class_id
		order by
			2, 1
		;
	--
	-- List Report's params
	cursor c_params(p_report_id number) is
		select
			par.id,
			par.label,
			par.data_type,
			par.default_value,
			par.optional_flag,
			par.list_sql
		from
			report_parameters par
		where
			par.report_id = p_report_id
		order by
			1
		;
	--
	-- List Report's Sections
	-- Excluding those that don't return any results (?)
	cursor c_sections(p_report_id number) is
		select
			sec.id,
			coalesce(sec.title, '[No Name]') title,
			sec.sql_text
		from
			report_sections sec
		where
			sec.report_id = p_report_id
			and
			coalesce(sec.no_results_flag, 'N') = 'N'
		order by
			1, 2
		;
	-- PL/SQL Table to hold param data
	type paramsTab is table of c_params%rowtype;
	params paramsTab;
	--
	ampersand constant varchar(1) := chr(38);
	param_replacement  varchar(9);
	parsed_sql         varchar(32000);
	error_messages     varchar(255);
	--
	function validate_sql(v_sql varchar) return varchar as
		v_cursor_handle int;
	begin
		v_cursor_handle := dbms_sql.open_cursor;
		dbms_sql.parse(v_cursor_handle, v_sql, dbms_sql.native);
		dbms_sql.close_cursor(v_cursor_handle);
		-- dbms_output.put_line('OK');
		--
		return null;
	exception
		when others then
			-- Close cursor if required
			if dbms_sql.is_open(v_cursor_handle) then
				dbms_sql.close_cursor(v_cursor_handle);
			end if;
			--
			if sqlcode = -942 then
				return 'Error: Unknown Table:  '||sqlerrm;
			elsif sqlcode = -904 then
				return 'Error: Unknown Column: '||sqlerrm;
			else
				return 'Error: '||sqlcode||': '||sqlerrm;
			end if;
	end;
begin
	dbms_output.enable(1000000);
	--
	execute immediate('truncate table report_syntax_check');
	--
	-- Loop through all Classes
	for cls in c_classes loop
		-- Loop through this Class' Reports
		for report in c_reports(cls.id) loop
			--dbms_output.put_line(report.title);
			--
			-- Bulk load future params into collection
			open  c_params(report.id);
			fetch c_params bulk collect into params;
			close c_params;
			--
			-- Check parameter SQL
			if params.count() > 0 then
				for idx in params.first..params.last loop
					if params(idx).list_sql is not null then
						-- Validate SQL, storing any error messages
						error_messages := validate_sql(params(idx).list_sql);
						insert into report_syntax_check (
							class_id,
							class_description,
							report_id,
							report_title,
							sql_id,
							sql_type,
							sql_name,
							sql_original,
							sql_parsed,
							errors)
						values (
							cls.id,
							cls.description,
							report.id,
							report.title,
							params(idx).id,
							'PARAM',
							params(idx).label,
							params(idx).list_sql,
							params(idx).list_sql,
							error_messages
						);
					end if;
				end loop;
			end if;
			--
			-- Check Sections
			for section in c_sections(report.id) loop
				-- dbms_output.put_line('...'||section.title);
				parsed_sql := section.sql_text;
				--
				-- Replace parameter placeholders
				if params.count() > 0 then
					for idx in params.first..params.last loop
						--dbms_output.put_line(to_char(idx)||' '||params(idx).label);
						--
						-- Find suitable dummy value for parameter
						if    params(idx).data_type = 'DATE' then
							param_replacement := '31-DEC-99';
						elsif params(idx).data_type = 'NUMBER' then
							param_replacement := '0';
						elsif params(idx).data_type = 'ORGANISATION' then
							param_replacement := '0';
						elsif params(idx).data_type = 'PERSON' then
							param_replacement := '0';
						elsif params(idx).data_type = 'TEXT' then
							param_replacement := '0'; -- 'x' doesn't work if it's a number without quotes !
						elsif params(idx).data_type = 'WORKER' then
							param_replacement := '0';
						else
							param_replacement := null;
						end if;
						--
						-- Replace parameter placeholder with dummy value
						parsed_sql := replace(parsed_sql, ampersand||'P'||to_char(idx)||ampersand, param_replacement);
					end loop;
				end if;
				--
				-- Validate SQL, storing any error messages
				error_messages := validate_sql(parsed_sql);
				insert into report_syntax_check (
					class_id,
					class_description,
					report_id,
					report_title,
					sql_id,
					sql_type,
					sql_name,
					sql_original,
					sql_parsed,
					errors)
				values (
					cls.id,
					cls.description,
					report.id,
					report.title,
					section.id,
					'SECTION',
					section.title,
					section.sql_text,
					parsed_sql,
					error_messages
				);
			end loop;
		end loop;
	end loop;
	--
	commit;
end;
/
select *
from
	report_syntax_check rsc
where
	rsc.errors is not null
/
