set feedback off
set heading off
set echo off
set serveroutput on size 1000000
set linesize 200

declare
	-- Anonymise_Script_ora.sql
	-- Oracle Anonymise script
	-- Currently Compatable for Frameworki Version 3.4.5
	--
    -- KT 11/06/2007 converted to Oracle from SQL Server version
    -- KT 12/06/2007 Added outputs to identify current table action
    -- DJM 25/06/2007 Tuning for Documents table (Constraints, etc. are discarded)
    --                Anonymise Form_Answers, but preserve lookup column
    -- TW 24/07/2008 Amended cursors to only select tables and columns that already exist in database
    --                 Amended DOCUMENTS anonymisation to dynamically create table and insert statement
    --                 (NB the reason a simple update is not used here is because it takes less resource 
    --                  to create all document records that we do want than to amend this one that we don't)
    --                 Amended to include all DM_ and SSDA903 tables
    -- MP 02/01/2009 FRA-1952 - Scrambling Personal data from _History Tables
    -- PB 06/04/2009 FRA-3121 - Truncate Report_form_answers_repeat and Lucene_* tables
    --                            created new cursor for Merge_* tables (since these have some constraints on them) Delete data rather than truncateing.
    --                            Disabled triggers before Overwriteing columns with '[Text]', since this was causing an error. (re-enable them afterwards)
    -- RW 22/05/2009 FRA-3122 - added table patterns to c_merge cursor: YKAP%, HLP%, FRA%
    --          added DM%, %TEMP, TEMP%, TMP%, %TMP but excluded %template% patterns to c_tables cursor
    --          REG_ENQUIRY_NEW_ENQUIRER, REG_ENQUIRY_SEARCH_CRITERIA and PEOPLE_HISTORY tables added to c_columns cursor
	--          datetime columns (DOB) updated with sysdate
	-- PB 17/08/2011 FRA-8003: Updated for use with DB version 4.2.1.1
	-- TW 24/01/2012 FRA-8143: Updated for use with DB version 4.2.5.0	(numbered script 722)
    --
    -- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    -- !!! Do not run this on a Live database !!!
    -- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    --
	v_table_exists NUMERIC(1);
    -- Tables' columns to be anonymised
    cursor c_columns is
        select table_name, column_name, data_type
        from user_tab_cols 
        where (table_name,column_name) in (        
        select 'ADDRESSES', 'FLAT_NUMBER' from dual union all
        select 'ADDRESSES', 'ACCESS_NOTES' from dual union all
        select 'ADDRESSES', 'BUILDING' from dual union all
        select 'ADDRESSES', 'STREET' from dual union all
        select 'ADDRESSES', 'STREET_NUMBER' from dual union all
        select 'ADDRESSES', 'COUNTY' from dual union all
        select 'ADDRESSES', 'COUNTRY' from dual union all
        select 'ADDRESSES', 'DISTRICT' from dual union all
        select 'ADDRESSES', 'TOWN' from dual union all
        select 'ADDRESSES', 'POST_CODE' from dual union all
        select 'ADDRESSES', 'UPPER_STREET' from dual union all
        select 'ADDRESSES', 'UPPER_BUILDING' from dual union all
        select 'ADDRESSES_HISTORY', 'FLAT_NUMBER' from dual union all
        select 'ADDRESSES_HISTORY', 'ACCESS_NOTES' from dual union all
        select 'ADDRESSES_HISTORY', 'BUILDING' from dual union all
        select 'ADDRESSES_HISTORY', 'STREET' from dual union all
        select 'ADDRESSES_HISTORY', 'STREET_NUMBER' from dual union all
        select 'ADDRESSES_HISTORY', 'COUNTY' from dual union all
        select 'ADDRESSES_HISTORY', 'COUNTRY' from dual union all
        select 'ADDRESSES_HISTORY', 'DISTRICT' from dual union all
        select 'ADDRESSES_HISTORY', 'TOWN' from dual union all
        select 'ADDRESSES_HISTORY', 'POST_CODE' from dual union all
        select 'ADDRESSES_HISTORY', 'UPPER_STREET' from dual union all
        select 'ADDRESSES_HISTORY', 'UPPER_BUILDING' from dual union all
        select 'ALERTS', 'ALERT_TEXT' from dual union all
        select 'CARE_PLAN_TEXT', 'LONG_TEXT' from dual union all
        select 'CARERS', 'NOTES' from dual union all
        select 'CARERS', 'CONTACT_NUMBER' from dual union all
        select 'CASE_NOTES', 'NOTE' from dual union all
        select 'CASE_NOTES_HISTORY', 'NOTE' from dual union all
        select 'CONFERENCE_CORE_GROUP_MEMBERS', 'COMMENTS' from dual union all
        select 'CONTRACTS_HISTORY', 'NAME' from dual union all
        select 'CONTRACTS_HISTORY', 'NOTES' from dual union all
        select 'CONTRACTS_HISTORY', 'DESCRIPTION' from dual union all
        select 'DOCUMENT_STAGING', 'DOCUMENT_FILE' from dual union all		
        select 'EIR2_TEXT', 'LONG_TEXT' from dual union all
        select 'EPISODES', 'COMMENTS' from dual union all
        select 'EPISODE_PARTICIPANTS', 'COMMENTS' from dual union all
        select 'EPISODE_QUESTION_ANSWERS', 'TEXT_ANSWER' from dual union all
        select 'EPISODE_REFERRAL_DETAILS', 'COMMENTS' from dual union all
        select 'EPISODE_REFERRAL_DETAILS', 'REFERRER_ADDRESS' from dual union all
        select 'EPISODE_REFERRAL_DETAILS', 'REFERRER_NAME' from dual union all
        select 'EPISODE_TOPICS', 'COMMENTS' from dual union all
        select 'EXTERNAL_ACCESS_REQUESTS', 'SUBJECT_FIRST_NAME' from dual union all		
        select 'EXTERNAL_ACCESS_REQUESTS', 'SUBJECT_LAST_NAME' from dual union all		
        -- Form_Answers handled separately, to preserve lookup data
        --select 'FORM_ANSWERS', 'TEXT_ANSWER' from dual union all
        select 'FORM_ANSWERS_REPEATING', 'TEXT_ANSWER' from dual union all
        select 'LONG_TEXTS', 'TEXT_DATA' from dual union all
        select 'MERGE_PERSON_FILE_RETENT_INFO', 'NOTES' from dual union all		
        select 'ORGANISATIONS', 'ORGANISATION_NOTES' from dual union all
        select 'PARTNERSHIP_CONSENT_HISTORY', 'NOTE' from dual union all
        select 'PARTNERSHIP_CONSENT_HISTORY', 'CONSENT_GIVER_NAME' from dual union all
        select 'PEOPLE', 'EMAIL_ADDRESS' from dual union all
        select 'PERSON_CAUTIONS', 'OFFENCE_TEXT' from dual union all
        select 'PERSON_CAUTIONS', 'OUTCOME_TEXT' from dual union all
        select 'PERSON_FILE_RETENTION_INFO', 'NOTES' from dual union all		
        select 'PERSON_FILE_RETENTION_HISTORY', 'NOTES' from dual union all				
        select 'PERSON_NAMES', 'FIRST_NAMES' from dual union all
        select 'PERSON_NAMES', 'LAST_NAME' from dual union all
        select 'PERSON_NAMES', 'MATCH_FIRST_NAMES' from dual union all
        select 'PERSON_NAMES', 'MATCH_LAST_NAME' from dual union all
        select 'PERSON_NOTES', 'NOTE' from dual union all
        select 'PERSON_NAMES_HISTORY', 'FIRST_NAMES' from dual union all
        select 'PERSON_NAMES_HISTORY', 'LAST_NAME' from dual union all
        select 'PERSON_NAMES_HISTORY', 'MATCH_FIRST_NAMES' from dual union all
        select 'PERSON_NAMES_HISTORY', 'MATCH_LAST_NAME' from dual union all
        select 'REGISTER_ENQUIRIES', 'NOTES' from dual union all
        -- added by RA 02/05/2007
        select 'ORGANISATIONS', 'NAME' from dual union all
        select 'ORGANISATIONS', 'EMAIL_ADDRESS' from dual union all
        select 'ORGANISATIONS', 'WEB_ADDRESS' from dual union all
        select 'ORGANISATIONS', 'DESCRIPTION' from dual union all
        select 'ORGANISATION_REFERENCES','REFERENCE' from dual union all
        select 'COST_CODE_NAMES','DESCRIPTION' from dual union all
        select 'TELEPHONE_NUMBERS','PHONE_NUMBER' from dual union all
        select 'PERSON_REFERENCES','REFERENCE' from dual union all
        select 'PERSON_REFERENCES_HISTORY','REFERENCE' from dual union all
        select 'EPISODE_OUTCOMES','NOTE' from dual union all
        select 'EPISODE_HISTORY','NOTES' from dual union all
        select 'WORKERS', 'FIRST_NAMES' from dual union all
        select 'WORKERS', 'LAST_NAMES' from dual union all
        select 'WORKERS', 'EMAIL_ADDRESS' from dual union all
        select 'ELEMENT_DETAILS','NOTE' from dual union all
        select 'ELEMENT_CHANGES','NOTE' from dual union all
        select 'PERSON_HEALTH_INTERVENTIONS', 'NOTES' from dual union all
        select 'PERSON_HOSPITAL_STAYS', 'CONSULTANT' from dual union all
        select 'PERSON_HOSPITAL_STAYS', 'REASON_TEXT' from dual union all
        --select 'MERGE_PERSON_NAMES', 'FIRST_NAMES' from dual union all
        --select 'MERGE_PERSON_NAMES', 'LAST_NAME' from dual union all
        --select 'MERGE_PERSON_NAMES', 'MATCH_FIRST_NAMES' from dual union all
        --select 'MERGE_PERSON_NAMES', 'MATCH_LAST_NAME' from dual union all
        --select 'MERGE_PERSON_NOTES', 'NOTE' from dual union all
        --select 'MERGE_PERSON_NOTES', 'NOTE' from dual union all
        --select 'MERGE_PERSON_CAUTIONS', 'OFFENCE_TEXT' from dual union all
        --select 'MERGE_PERSON_CAUTIONS', 'OUTCOME_TEXT' from dual union all
        --select 'MERGE_PEOPLE', 'EMAIL_ADDRESS' from dual union all
        --select 'MERGE_EPISODES', 'COMMENTS' from dual union all
        --select 'MERGE_EPISODE_PARTICIPANTS', 'COMMENTS' from dual union all
        --select 'MERGE_EPISODE_REFERRAL_DETAILS', 'COMMENTS' from dual union all
        --select 'MERGE_EPISODE_REFERRAL_DETAILS', 'REFERRER_ADDRESS' from dual union all
        --select 'MERGE_EPISODE_REFERRAL_DETAILS', 'REFERRER_NAME' from dual union all
        --select 'MERGE_CONF_CORE_GROUP_MEMBERS', 'COMMENTS' from dual union all
        --select 'MERGE_CASE_NOTES', 'NOTE' from dual union all
        --select 'MERGE_CASE_NOTES_HISTORY', 'NOTE' from dual union all
        --select 'MERGE_CASE_NOTES_HISTORY', 'TITLE' from dual union all
           --select 'MERGE_ALERTS', 'ALERT_TEXT' from dual union all
        --select 'MERGE_ELEMENT_DETAILS','NOTE' from dual union all
        --select 'MERGE_ELEMENT_CHANGES','NOTE' from dual union all
        --select 'MERGE_PERSON_HEALTH_INT', 'NOTES' from dual union all
        --select 'MERGE_PERSON_HOSPITAL_STAYS', 'CONSULTANT' from dual union all
        --select 'MERGE_PERSON_HOSPITAL_STAYS', 'REASON_TEXT' from dual union all
        --added by RA 14/05/2007
        select 'CASE_NOTES', 'TITLE' from dual union all
        select 'CASE_NOTES_HISTORY', 'TITLE' from dual union all
        --added by PB 06/04/2009 FRA-3121
        select 'CHRONOLOGY_FORM_EVENTS', 'TITLE' from dual union all
        select 'CHRONOLOGY_SELECTED_EVENTS', 'TITLE' from dual union all
        select 'CHRONOLOGY_SELECTED_EVENTS', 'TITLE_FORM_EDIT' from dual union all
        select 'CHRONOLOGY_SELECTED_EVENTS', 'NOTES_1' from dual union all
        select 'CHRONOLOGY_SELECTED_EVENTS', 'NOTES_2' from dual union all
        select 'CHRONOLOGY_SELECTED_EVENTS', 'NOTES_3' from dual union all
        select 'EPISODE_CHRONOLOGY', 'CHRONOLOGY_TITLE' from dual union all
        -- RW FRA-3122
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'FIRST_NAMES' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'LAST_NAMES' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'LEGACY_REG_ENQ_FULL_NAME' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'TELEPHONE_NUMBER' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'MOBILE_NUMBER' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'FAX_NUMBER' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'EMAIL_ADDRESS' from dual union all
        select 'REG_ENQUIRY_NEW_ENQUIRER', 'NOTES' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'PERSON_FIRST_NAMES' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'PERSON_LAST_NAMES' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'PERSON_DOB' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'FLAT_NUMBER' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'HOUSE_NUMBER' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'BUILDING_NAME' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'STREET_NAME' from dual union all
        select 'REG_ENQUIRY_SEARCH_CRITERIA', 'POST_CODE' from dual union all
        select 'PEOPLE_HISTORY', 'EMAIL_ADDRESS' from dual union all
        select 'EPISODE_PARTICIPANTS', 'LOCAL_NAME'  from dual union all
        select 'EPISODE_PARTICIPANTS', 'LOCAL_ADDRESS' from dual union all
        select 'EPISODE_PARTICIPANTS', 'Local_Phone' from dual union all
        -- PB FRA-8003
        
        select 'PARTNERSHIP_CONSENT', 'AGENCY' from dual union all
        select 'PARTNERSHIP_CONSENT', 'NOTE' from dual union all
        select 'PARTNERSHIP_CONSENT_HISTORY', 'AGENCY' from dual union all
        select 'ASSESSMENT_FORM_ANSWERS', 'OLD_ANSWER' from dual union all
        select 'ASSESSMENT_FORM_ANSWERS', 'NEW_ANSWER' from dual union all
        select 'ESAP_ASSESSMENTS_CONTENT', 'CONTENT' from dual union all
        select 'SUBJECT_WARNING_FLAG', 'CONTACT_PERSON' from dual union all
        select 'SUBJECT_WARNING_FLAG', 'CONTACT_PHONE_NUMBER' from dual union all
        select 'SUBJECT_WARNING_FLAG', 'CONTACT_EMAIL' from dual union all
        select 'SUBJECT_WARNING_FLAG', 'CONTACT_AGENCY' from dual union all
        select 'SUBJECT_WARNING_FLAG', 'WARNING_GUIDANCE' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY', 'CONTACT_PERSON' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY', 'CONTACT_PHONE_NUMBER' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY', 'CONTACT_EMAIL' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY', 'CONTACT_AGENCY' from dual union all
        select 'SUBJECT_WARNING_FLAG_HISTORY', 'WARNING_GUIDANCE' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR', 'MESSAGEHEADER1' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR', 'MESSAGEHEADER2' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR', 'MESSAGEINSTRUCTION' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR', 'STANDBYAGENCY' from dual union all
        select 'SUBJECT_WARNING_FLAG_EXT_ATTR', 'STANDBYNUMBER' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY', 'MESSAGEHEADER1' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY', 'MESSAGEHEADER2' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY', 'MESSAGEINSTRUCTION' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY', 'STANDBYAGENCY' from dual union all
        select 'SJT_WRN_FLG_EXT_ATTR_HISTORY', 'STANDBYNUMBER' from dual union all
        select 'PARTNERSHIP_PROCESS_LOG', 'PROFESSIONAL_NAME' from dual union all
        select 'TELEPHONE_NUMBERS_HISTORY', 'PHONE_NUMBER' from dual union all
        select 'EXTERNAL_SYSTEM_MSGADT_ATTACH', 'ATTACHMENT' from dual union all
        -- Additional Fields
        select 'CARER_UNAVAILABILITIES', 'NOTES' from dual union all
		select 'CARE_PLANS', 'CHILD_ADDRESS1' from dual union all
		select 'CARE_PLANS', 'CHILD_ADDRESS2' from dual union all
		select 'CARE_PLANS', 'CHILD_ADDRESS3' from dual union all
		select 'CARE_PLANS', 'CHILD_ADDRESS4' from dual union all
		select 'CARE_PLANS', 'CHILD_PHONE' from dual union all
		select 'CARE_PLANS', 'CONTINGENCY_PLAN' from dual union all
		select 'CARE_PLANS', 'FAMILY_NAME' from dual union all
		select 'CARE_PLANS', 'FORENAMES' from dual union all
		select 'CARE_PLANS', 'OTHER_TEXT' from dual union all
		select 'CARE_PLANS', 'PRINCIPAL_CARER_NAME' from dual union all
		select 'CARE_PLANS', 'PRINCIPAL_CARER_RELATIONSHIP' from dual union all
		select 'CARE_PLANS', 'REVIEW_ADDRESS1' from dual union all
		select 'CARE_PLANS', 'REVIEW_ADDRESS2' from dual union all
		select 'CARE_PLANS', 'REVIEW_ADDRESS3' from dual union all
		select 'CARE_PLANS', 'REVIEW_ADDRESS4' from dual union all
		select 'CARE_PLANS', 'SA_TEAM_MANAGER_NAME' from dual union all
		select 'CARE_PLANS', 'SA_WORKER_ADDRESS1' from dual union all
		select 'CARE_PLANS', 'SA_WORKER_ADDRESS2' from dual union all
		select 'CARE_PLANS', 'SA_WORKER_ADDRESS3' from dual union all
		select 'CARE_PLANS', 'SA_WORKER_ADDRESS4' from dual union all
		select 'CARE_PLANS', 'SA_WORKER_NAME' from dual union all
		select 'CARE_PLANS', 'SA_WORKER_PHONE' from dual union all
		select 'CARE_PLANS', 'TEAM_MANAGER_NAME' from dual union all
		select 'CARE_PLANS', 'WORKER_ADDRESS1' from dual union all
		select 'CARE_PLANS', 'WORKER_ADDRESS2' from dual union all
		select 'CARE_PLANS', 'WORKER_ADDRESS3' from dual union all
		select 'CARE_PLANS', 'WORKER_ADDRESS4' from dual union all
		select 'CARE_PLANS', 'WORKER_NAME' from dual union all
		select 'CARE_PLANS', 'WORKER_PHONE' from dual union all
		select 'CGM_PARTICIPANTS', 'COMMENTS' from dual union all
		select 'CGM_PARTICIPANTS', 'LOCAL_ADDRESS' from dual union all
		select 'CGM_PARTICIPANTS', 'LOCAL_NAME' from dual union all
		select 'CGM_PARTICIPANTS', 'LOCAL_PHONE' from dual union all
		select 'CGM_PARTICIPANT_ACTIONS', 'FINDINGS' from dual union all
		select 'CGM_PARTICIPANT_ACTIONS', 'FURTHER_ACTION' from dual union all
		select 'CONFERENCES', 'COMMENTS' from dual union all
		select 'CONFERENCES', 'LOCATION' from dual union all
		select 'CONFERENCE_ATTENDEES', 'COMMENTS' from dual union all
		select 'CONFERENCE_ATTENDEES', 'LOCAL_ADDRESS' from dual union all
		select 'CONFERENCE_ATTENDEES', 'LOCAL_NAME' from dual union all
		select 'CONFERENCE_ATTENDEES', 'LOCAL_PHONE' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS', 'LOCAL_ADDRESS' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS', 'LOCAL_NAME' from dual union all
		select 'CONFERENCE_CORE_GROUP_MEMBERS', 'LOCAL_PHONE' from dual union all
		select 'CONTRACTS', 'DESCRIPTION' from dual union all
		select 'CONTRACTS', 'NOTE' from dual union all
		select 'CONTRACTS', 'TITLE' from dual union all
		select 'CORE_GROUP_MEETINGS', 'MEETING_SUMMARY' from dual union all
		select 'CORE_GROUP_MEETINGS', 'REVIEW_OF_PREVIOUS_MEETING' from dual union all
		select 'CORE_GROUP_VISITS', 'ACTION_ARISING' from dual union all
		select 'CORE_GROUP_VISITS', 'VISIT_SUMMARY' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS', 'COMMENTS' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS', 'LOCAL_ADDRESS' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS', 'LOCAL_NAME' from dual union all
		select 'CORE_GROUP_VISIT_PARTICIPANTS', 'LOCAL_PHONE' from dual union all
		select 'CORE_GROUP_VISIT_SUBJECTS', 'NOTE' from dual union all
		select 'EPISODE_ACTIONS', 'LOCAL_ADDRESS' from dual union all
		select 'EPISODE_ACTIONS', 'LOCAL_NAME' from dual union all
		select 'EPISODE_ACTIONS', 'LOCAL_PHONE' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS', 'DESIRED_OUTCOME' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS', 'HOW_MET' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS', 'IDENTIFIED_NEED' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS', 'SERVICE_PROVIDED' from dual union all
		select 'EPISODE_ACTION_PLAN_ITEMS', 'WHO_RESPONSIBLE' from dual union all
		select 'EPISODE_ACTION_PLAN_REVIEWS', 'FURTHER_SERVICES_TEXT' from dual union all
		select 'EPISODE_ACTION_PLAN_REVIEWS', 'OUTCOME_TEXT' from dual union all
		select 'EPISODE_CONF_INVITEES', 'ADDRESS' from dual union all
		select 'EPISODE_CONF_INVITEES', 'NAME' from dual union all
		select 'EPISODE_CONF_INVITEES', 'PHONE' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'FAMILY' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'HOME_ADDRESS' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'KEY_WORKER' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'RETURN_EXT' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'RETURN_NAME' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'SOURCE_CONCERNS' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'TEAM' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'VENUE' from dual union all
		select 'EPISODE_CONF_INV_CLISTS', 'WORKER_PHONE' from dual union all
		select 'EPISODE_IA_ELEMENTS', 'ELEMENT_TEXT' from dual union all
		select 'EPISODE_OUTCOME_RULES', 'DESCRIPTION' from dual union all
		select 'EPISODE_OUTCOME_TASKS', 'ISSUE_NOTES' from dual union all
		select 'EPISODE_PARTICIPANTS', 'LOCAL_PHONE' from dual union all
		select 'EPISODE_PARTICIPANT_ACTIONS', 'FINDINGS' from dual union all
		select 'EPISODE_PARTICIPANT_ACTIONS', 'FURTHER_ACTION' from dual union all
		select 'EPISODE_PARTICIPANT_PLANS', 'ACTUAL_OUTCOME' from dual union all
		select 'EPISODE_PARTICIPANT_PLANS', 'PLANNED_OUTCOME' from dual union all
		select 'EPISODE_PARTICIPANT_PLANS', 'RESPONSE_TO_NEED' from dual union all
		select 'EPISODE_REFERRAL_DETAILS', 'REFERRER_PHONE' from dual union all
		select 'EPISODE_S47_OUTCOMES', 'ACTION_REASONS' from dual union all
		select 'EPISODE_S47_OUTCOMES', 'DECISION_REASONS' from dual union all
		select 'EPISODE_SERVICE_TASKS', 'NOTE' from dual union all
		select 'EPISODE_SERV_TASK_ALLOWANCES', 'NOTES' from dual union all
		select 'EPISODE_STEPS', 'COMMENTS' from dual union all
		select 'EPISODE_STEP_HISTORY', 'NOTES' from dual union all
		select 'EPISODE_STEP_OUTCOMES', 'NOTES' from dual union all
		select 'EPISODE_STRATEGY_DISCUSSIONS', 'DECISION_REASON' from dual union all
		select 'EPISODE_STRATEGY_DISCUSSIONS', 'PURPOSE' from dual union all
		select 'EPISODE_TASKS', 'ASSIGN_NOTES' from dual union all
		select 'EPISODE_TASKS', 'ISSUE_NOTES' from dual union all
		select 'EPISODE_TASKS', 'RETURN_NOTES' from dual union all
		select 'EPISODE_TASK_AUTHS', 'BUDGET_CODES' from dual union all
		select 'EPISODE_TASK_AUTHS', 'ISSUE_NOTES' from dual union all
		select 'EPISODE_TASK_AUTHS', 'RETURN_NOTES' from dual union all
		select 'EPISODE_VISITS', 'NOTES' from dual union all
		select 'INVOICES', 'NOTE' from dual union all
		select 'LAC_CAUTIONS', 'OFFENCE_TEXT' from dual union all
		select 'LAC_CAUTIONS', 'ORG_NAME' from dual union all
		select 'LAC_CAUTIONS', 'OUTCOME_DESC' from dual union all
		select 'LAC_CAUTIONS', 'OUTCOME_TEXT' from dual union all
		select 'LAC_HEALTH', 'NOTES' from dual union all
		select 'LAC_PLACEMENT_CHILDREN', 'NAME' from dual union all
		select 'LAC_REVIEWS', 'CARER_ADDRESS1' from dual union all
		select 'LAC_REVIEWS', 'CARER_ADDRESS2' from dual union all
		select 'LAC_REVIEWS', 'CARER_ADDRESS3' from dual union all
		select 'LAC_REVIEWS', 'CARER_ADDRESS4' from dual union all
		select 'LAC_REVIEWS', 'CARER_NAME' from dual union all
		select 'LAC_REVIEWS', 'CARER_PHONE' from dual union all
		select 'LAC_REVIEWS', 'CARER_RELATIONSHIP' from dual union all
		select 'LAC_REVIEWS', 'CARE_NAME' from dual union all
		select 'LAC_REVIEWS', 'CONS_1_NAME' from dual union all
		select 'LAC_REVIEWS', 'CONS_1_PHONE' from dual union all
		select 'LAC_REVIEWS', 'CONS_2_NAME' from dual union all
		select 'LAC_REVIEWS', 'CONS_2_PHONE' from dual union all
		select 'LAC_REVIEWS', 'CONS_3_NAME' from dual union all
		select 'LAC_REVIEWS', 'CONS_3_PHONE' from dual union all
		select 'LAC_REVIEWS', 'CONS_ED_NAME' from dual union all
		select 'LAC_REVIEWS', 'CONS_ED_PHONE' from dual union all
		select 'LAC_REVIEWS', 'CONS_HS_NAME' from dual union all
		select 'LAC_REVIEWS', 'CONS_HS_PHONE' from dual union all
		select 'LAC_REVIEWS', 'DISC_OTHER_TEXT' from dual union all
		select 'LAC_REVIEWS', 'FAMILY_NAME' from dual union all
		select 'LAC_REVIEWS', 'FORENAMES' from dual union all
		select 'LAC_REVIEWS', 'NEXT_ADDRESS1' from dual union all
		select 'LAC_REVIEWS', 'NEXT_ADDRESS2' from dual union all
		select 'LAC_REVIEWS', 'NEXT_ADDRESS3' from dual union all
		select 'LAC_REVIEWS', 'NEXT_ADDRESS4' from dual union all
		select 'LAC_REVIEWS', 'NEXT_PHONE' from dual union all
		select 'LAC_REVIEWS', 'OVERALL_PLAN' from dual union all
		select 'LAC_REVIEW_CHILD_VISITS', 'PERSONS_SEEN' from dual union all
		select 'LAC_REVIEW_INVITEES', 'NAME' from dual union all
		select 'LAC_REVIEW_PARENT_VISITS', 'OTHER_TEXT' from dual union all
		select 'LAC_REVIEW_TEXT', 'LONG_TEXT' from dual union all
		select 'LAC_SCHOOLS', 'SCHOOL' from dual union all
		select 'ORGANISATIONS', 'DEPARTMENT' from dual union all
		select 'PACKAGE_AUTHORISATION_TASKS', 'AUTHORISATION_NOTE' from dual union all
		select 'PACKAGE_AUTHORISATION_TASKS', 'REQUEST_NOTE' from dual union all
		select 'PACKAGE_BUDGET_ELEMENTS', 'NOTE' from dual union all
		select 'PACKAGE_BUDGET_ELEMENT_CHANGES', 'NOTE' from dual union all
		select 'PACKAGE_SERVICE_DETAILS', 'NOTE' from dual union all
		select 'PARTNERSHIP_CONSENT', 'CONSENT_GIVER_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT', 'FORM_FILE_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT', 'LOCATION' from dual union all
		select 'PARTNERSHIP_CONSENT', 'PROFESSIONAL_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT_FORM', 'FORM_DATA' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY', 'FORM_FILE_NAME' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY', 'LOCATION' from dual union all
		select 'PARTNERSHIP_CONSENT_HISTORY', 'PROFESSIONAL_NAME' from dual union all
		select 'PAYMENT_CYCLE_REPORTS', 'EMAIL_ADDRESS' from dual union all
		select 'PAYMENT_CYCLE_REPORTS', 'FAX_NUMBER' from dual union all
		select 'PERSON_NAMES_HISTORY', 'SOUNDEX_LAST_NAME' from dual union all
		select 'PERSON_NON_LA_LEGAL_STATUSES', 'NOTES' from dual union all
		select 'PERSON_SCHOOL_REPORTS', 'NOTES' from dual union all
		select 'PERSON_SERVICE_TASKS', 'NOTE' from dual union all
		select 'PERSON_SERV_TASK_ALLOWANCES', 'NOTES' from dual union all
		select 'PERSON_VISITS', 'NOTES' from dual union all
		select 'QAF_ASSESSMENTS', 'NOTES' from dual union all
		select 'QAF_OBJECTIVES', 'DESCRIPTION' from dual union all
		select 'QAF_SUBMISSIONS', 'NOTES' from dual union all
		select 'RA_ACKNOWLEDGEMENTS', 'NOTE' from dual union all
		select 'REGISTER_ENQUIRIES', 'LOCAL_ADDRESS' from dual union all
		select 'REGISTER_ENQUIRIES', 'LOCAL_NAME' from dual union all
		select 'REGISTER_ENQUIRIES', 'LOCAL_PHONE' from dual union all
		select 'SUBJECT_WARNING_FLAG_EXT_ATTR', 'STANDBYPHONENUMBER' from dual union all
		select 'WORKER_CASES', 'DESCRIPTION' from dual union all
		select 'WORKER_CASES', 'EMAIL_ADDRESS' from dual union all
		select 'WORKER_CASES', 'FIRST_NAMES' from dual union all
		select 'WORKER_CASES', 'LAST_NAME' from dual union all
		select 'WORKER_CASES', 'MATCH_FIRST_NAMES' from dual union all
		select 'WORKER_CASES', 'MATCH_LAST_NAME' from dual

        );
        
    -- tables to be truncated
    
    cursor c_tables is
        select table_name
        from user_tables
        where (table_name like 'DM%'
        or table_name like 'SSDA903%'
        or table_name like 'LUCENE_%'
        or table_name in ('RAP_CLIENTS'
                        ,'REPORT_FORM_ANSWERS'
                        ,'REPORT_FORM_ANSWERS_REPEAT'
                        ,'CP_REGISTRATION_CHILDREN'
                        ,'LAC_ADDRESSES'
                        ,'LAC_CARERS'
                        ,'RESIDENCE_ORDER_CHILDREN'
                        ,'UASC_CHILDREN'
                        ,'CHILD_REFERRAL_CHILDREN'
                        ,'CHILD_ASSESSMENT_CHILDREN'
                        ,'CHILD_S47_ENQUIRIES_CHILDREN'
                        ,'CHILD_STRAT_DISC_CHILDREN'
                        ,'CHILD_INITIAL_CP_CONF_CHILDREN'
                        ,'CHILD_PF1_CHILDREN' 
                        ,'SR1_CLIENTS'
                        ,'PSSEX1_XSSM_PEOPLE'
                        ,'PSSEX1_ACTIVITY_PEOPLE'
                        )
    or table_name like '%TEMP' or
    table_name like 'TEMP%' or
    table_name like 'TMP%' or
    table_name like '%TMP')
    and table_name not like '%TEMPLATE%';            
        
    -- documents table structure    
    cursor c_doc_columns is
        select table_name, column_name, data_type, 
                case data_type 
                    when 'NUMBER' then data_precision
                    when 'VARCHAR2' then data_length
                end length    
        from user_tab_cols 
        where table_name = 'DOCUMENTS';       


    -- Merge tables 
    cursor c_Merge is
        SELECT distinct    table_name
        FROM     user_constraints 
        where     table_name like 'MERGE_%' or
        table_name like 'YKAP%' or
        table_name like '%\_YKAP\_%' escape '\' or
        table_name like 'HLP%' or
        table_name like '%\_HLP\_%' escape '\' or
        table_name like 'FRA%' or
        table_name like '%\_FRA\_%' escape '\' or
        translate(table_name, '1234567890', '0000000000') like '%FRA000%' or
        translate(table_name, '1234567890', '0000000000') like '%YKAP000%' or
        translate(table_name, '1234567890', '0000000000') like '%HLP000%'
        order by case table_name 
                    when  'MERGE_LOGS' then 
                        'z' 
                    when 'MERGE_SQL_LOGS' then 
                        'x' 
                    else 
                        table_name 
                end;

    --
    v_table_name  varchar(32);
    v_column_name varchar(32);
    v_column_type varchar(100);
    v_data_type varchar2(16);        
    v_sql varchar2(3000);
    v_sql_select varchar2(3000);
    v_length number(9);
--    v_exists number;        
    
begin

    -- Remove uploaded documents
    -- NB. The Documents table's constraints will have to be reinstated after import
    dbms_output.put_line('Anonymising DOCUMENTS.image_data');
    --
    begin
        execute immediate 'drop table tmp_anonymised_documents';
    exception
        when others then
            if sqlcode != -942 then
                dbms_output.put_line('Error '||to_char(sqlcode)||': dropping tmp_anonymised_documents');
                raise;
            end if;
    end;
    
    open c_doc_columns;
    fetch c_doc_columns into v_table_name,v_column_name,v_data_type,v_length;
    while c_doc_columns%found loop
        -- create column list
        v_sql := v_sql || v_column_name||' '||v_data_type|| 
            case when v_length is not null or v_length > 0 then '('||v_length||'), '
                else ', '
            end || chr(10);
        fetch c_doc_columns into v_table_name,v_column_name,v_data_type,v_length;
    end loop;        
    close c_doc_columns;
    
    -- amend table name
    v_table_name := 'TMP_ANONYMISED_'||v_table_name;
    
    --remove final comma
    v_sql := rtrim(v_sql,', '|| chr(10));
    
    v_sql := 'create table '||v_table_name||' ('||v_sql||')';
    
    -- create table 
    execute immediate v_sql;
    
    v_sql := null;
    
    open c_doc_columns;
    fetch c_doc_columns into v_table_name,v_column_name,v_data_type,v_length;
    while c_doc_columns%found loop
        -- create column list
        v_sql := v_sql || v_column_name||', '|| chr(10);    
        -- if data type is long then insert null
        v_sql_select := v_sql_select || 
                        case when v_data_type like 'LONG%' 
                            then 'null, '
                            else v_column_name||', '
                        end || chr(10);        
        fetch c_doc_columns into v_table_name,v_column_name,v_data_type,v_length;
    end loop;        
    close c_doc_columns;
    
    --remove final comma
    v_sql := rtrim(v_sql,', '|| chr(10));
    v_sql_select := rtrim(v_sql_select,', '|| chr(10));
    
    -- create insert statement
    v_sql := 'insert into TMP_ANONYMISED_'||v_table_name||' ( '|| chr(10) || v_sql || ')' || chr(10) ||
             'select '|| v_sql_select || chr(10) ||' from ' ||  v_table_name || '';                       
             
    -- execute insert statement
    execute immediate v_sql;                 

    --
    -- Replace original Documents table with anonymised version
    begin
    execute immediate 'alter table documents disable primary key cascade'; 
    exception
       when others then
       null;
    end;
    begin
       execute immediate 'drop table documents cascade constraints';    
    exception
       when others then
       null;
    end;
    begin
       execute immediate 'rename tmp_anonymised_documents to documents';
    exception
       when others then
       null;
    end;
    
    
    --
    -- Anonymise other columns
    open c_columns;
    loop

        fetch c_columns into v_table_name, v_column_name, v_data_type;
        exit when c_columns%notfound;

		SELECT COUNT(1) INTO v_table_exists
	 	FROM user_tables ut inner join USER_TAB_COLUMNS utc on ut.table_name = utc.table_name
		WHERE ut.table_name = v_table_name and utc.column_name = v_column_name;

	 
	 	IF v_table_exists = 1 then
			dbms_output.put_line('Anonymising '||v_table_name||'.'||v_column_name);
			
			execute immediate('ALTER TABLE '||v_table_name||' DISABLE ALL TRIGGERS');
			

			-- Determine column's data type
			v_column_type := null;

			-- 
			if v_data_type like 'LONG%' then
				-- Overwrite column with '[Text]'
				execute immediate('update '||v_table_name||' set '||v_column_name||' = ''[Text]'' where '||v_column_name||' is not null');
			elsif v_data_type = 'DATE' then
				execute immediate('update '||v_table_name||' set '||v_column_name||' = sysdate where '||v_column_name||' is not null');
			elsif v_data_type = 'BLOB' then
				execute immediate('update '||v_table_name||' set '||v_column_name||' = null where '||v_column_name||' is not null');
			else
				-- Overwrite column with last letter of each value
				execute immediate('update '||v_table_name||' set '||v_column_name||' = rpad(upper(substr('||v_column_name||', length('||v_column_name||'), 1)), length('||v_column_name||'), upper(substr('||v_column_name||', length('||v_column_name||'), 1))) where length('||v_column_name||') > 1');
			end if;

			execute immediate('ALTER TABLE '||v_table_name||' ENABLE ALL TRIGGERS');
		else
			dbms_output.put_line(v_table_name||'.'||v_column_name||' Does not exist within this database');
		END IF;

    end loop;
    close c_columns;    
    
    --
    -- Anonymise Form_Answers, but preserve lookup column
    dbms_output.put_line('Anonymising FORM_ANSWERS.TEXT_ANSWER');
    Begin    
        update form_answers
        set
            text_answer = '[Text]'
        where
            --CRITERIA: Text Answer present
            text_answer is not null
            and
            --CRITERIA: Not based on a lookup Component
            not exists (
                select 'x'
                from   section_items sit
                --JOIN: Component Items
                inner join items itm
                on itm.id = sit.item_id
                and itm.component_id is not null
                --JOIN: Lookup Answer
                inner join component_answers can
                on can.component_id = itm.component_id
                where
                    sit.id = form_answers.section_item_id
            )
            and
            --CRITERIA: Not based on a lookup Question
            not exists (
                select 'x'
                from   section_items sit
                --JOIN: Component Items
                inner join items itm
                on itm.id = sit.item_id
                and itm.question_id is not null
                --JOIN: Lookup Answer
                inner join question_answers qan
                on qan.question_id = itm.question_id
                where
                    sit.id = form_answers.section_item_id
            )
        ;
    exception
        when others then
            dbms_output.put_line('Error '||to_char(sqlcode)||': updating form_answers.text_answer');
            raise;    
    end;        
    
    
    --
    -- Truncate datamart tables with personal details
    open c_tables;
    fetch c_tables into v_table_name;
    while c_tables%found loop
        dbms_output.put_line('Truncating datatmart table '||v_table_name);
        execute immediate ('truncate table '||v_table_name);    
        fetch c_tables into v_table_name;
    end loop;
    close c_tables;    
    
    -- Clear merge tables
    open c_merge;
    fetch c_merge into v_table_name;
    while c_merge%found loop
        dbms_output.put_line('Anonymising table '||v_table_name);
        execute immediate ('delete from '||v_table_name);    
        fetch c_merge into v_table_name;
    end loop;
    close c_merge;

    begin
        execute immediate 'purge recyclebin';
    exception
    when others then
        null;
    end;

    dbms_output.put_line('Completed!');

end;
/
