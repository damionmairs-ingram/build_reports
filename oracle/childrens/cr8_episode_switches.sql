create or replace procedure pop_c_swt_adopt_best_interest is

	switch_name  constant varchar(30) := 'Child Adoption Best Interest';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);

	begin
		if switch_value = 'Episode Outcome'
		then 
			update dm_c_episodes epi
			set
				epi.adoption_best_interest_date = (
					select
						max(eo.entered_date)
					from
						dm_c_episode_outcomes eo
					where
						eo.episode_id = epi.episode_id
						and
						eo.is_adoption_best_interest = 'Y'
					)
			where
				epi.is_adoption_best_interest = 'Y'
				and
				epi.adoption_best_interest_date is null;
			
			
		elsif switch_value = 'Form'
		then
			update dm_c_episodes epi
			set
				epi.adoption_best_interest_date = (
				select
					min(rfa.date_answer)
				from
					report_form_answers rfa
				inner join dm_c_filters rf
				on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
				and
				rf.filter_name = 'Child Adoption Best Interest Decisions'
				where
					rfa.episode_id = epi.episode_id
					and
					rfa.date_answer is not null
				)
			where
				epi.is_adoption_best_interest = 'Y'
				and
				epi.adoption_best_interest_date is null;
		end if;	
	end;
/

create or replace procedure pop_c_swt_adopt_match_date is

	switch_name  constant varchar(30) := 'Child Adoption Match';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Episode Outcome'
		then 
			update dm_c_episodes epi
			set
				epi.adoption_match_date = (
					select
						max(eo.entered_date)
					from
						dm_c_episode_outcomes eo
					where
						eo.episode_id = epi.episode_id
						and
						eo.is_adoption_match = 'Y'
					)
			where
				epi.is_adoption_match = 'Y'
				and
				epi.adoption_match_date is null;
			
			
		elsif switch_value = 'Form'
		then
			update dm_c_episodes epi
			set
				epi.adoption_match_date = (
				select
					min(rfa.date_answer)
				from
					report_form_answers rfa
				inner join dm_c_filters rf
				on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
				and
				rf.filter_name = 'Child Adoption Matches'
				where
					rfa.episode_id = epi.episode_id
					and
					rfa.date_answer is not null
				)
			where
				epi.is_adoption_match = 'Y'
				and
				epi.adoption_match_date is null;
		
		end if;	
	end;
/

create or replace procedure pop_c_swt_lac_review_date is

	switch_name constant varchar(30) := 'Child LAC Review Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form answer; system end date'
		then
			update dm_c_episodes epi
				set epi.lac_review_date = (
					select
						min(rfa.date_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = 'Child LAC Review Date'
					where
						rfa.episode_id = epi.episode_id
					)
				where
					epi.lac_review_category is not null
					and
					epi.lac_review_date is null;
			
		elsif switch_value in  ('Strategy discussion date', 'Strategy Old; Conference New')
		then
			update dm_c_episodes epi
				set epi.lac_review_date = (
					select
						min(esd.discussion_date)
					from
						episode_strategy_discussions esd
					where
						esd.episode_id = epi.episode_id
					)
				where
					epi.lac_review_category is not null
					and
					epi.lac_review_date is null;
			
		elsif switch_value in ('Conference; system end date', 'Strategy Old; Conference New')
		then
			update dm_c_episodes epi
				set epi.lac_review_date = (
					select
						min(coalesce(con.actual_date,con.planned_date))
					from
						conferences con
					inner join episodes on
					episodes.conference_id = con.id
					where
						coalesce(con.actual_date,con.planned_date) is not null
					)
				where
					epi.lac_review_category is not null
					and
					epi.lac_review_date is null;
			
		elsif switch_value in ('Form answer; system end date', 'Conference; system end date')
		then
			update dm_c_episodes epi
				set
					epi.lac_review_date = epi.end_datetime
				where
					epi.lac_review_category is not null
					and
					epi.lac_review_date is null;
					
		end if;
	end;
										
/

create or replace procedure pop_c_swt_conf_category is 

	switch_name constant varchar(30) := 'CP Conference Category';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);

	begin
		if switch_value = 'Separate Episode Type'
		then
			update dm_c_episodes epi
				set cp_conference_category = (
			select
				rg.category_value
			from
				dm_c_groups rg
			where
				rg.group_name = 'Child Conference Category'
				and
				epi.episode_type = rg.mapped_value
				and
				epi.lac_review_category is not null
			);
			
		
		elsif switch_value = 'Same Episode Type'
		then
			update dm_c_episodes epi
				set cp_conference_category = (
			select
				rg.category_value
			from
			/* An episode should not have two conflicting mapped outcomes */
				episode_outcomes eo
			inner join dm_c_groups rg
			on eo.code = rg.mapped_value
			where
				rg.group_name = 'Child Conference Category'
				and
				eo.episode_id = epi.episode_id
				and
				epi.lac_review_category is not null
				);
				
		end if;
	end;
/

create or replace procedure pop_c_swt_end_date is

	switch_name constant varchar(30) := 'Child Episode End Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form; System'
		then
			update dm_c_episodes epi
				set 
					epi.end_date = dbo.no_time(epi.end_datetime_form),
					epi.end_datetime = epi.end_datetime_form;
				
		elsif switch_value = 'Last Worked On'
		then
			update dm_c_episodes epi
				set 
					epi.end_date = dbo.no_time(epi.end_datetime_last_worked_on),
					epi.end_datetime = epi.end_datetime_last_worked_on;
				
		elsif switch_value = 'Outcome Entered'			
		then
			update dm_c_episodes epi
				set 
					epi.end_date = dbo.no_time(epi.end_datetime_entered_date),
					epi.end_datetime = epi.end_datetime_entered_date;
				
		elsif switch_value in ('Form; Episode', 'System')
		then
			update dm_c_episodes epi
				set 
					epi.end_date = dbo.no_time(epi.end_datetime_system),
					epi.end_datetime = epi.end_datetime_system
			where
				epi.end_date is null
				and
				epi.end_datetime is null;
				
		end if;
	
	commit;
	
end;
/

create or replace procedure pop_c_swt_lac_participation is

	switch_name constant varchar(30) := 'Child LAC Review Participation';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form'
		then
			update dm_c_episodes epi
				set epi.lac_review_participation_code = (
					select
						min(rfa.text_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = 'Child LAC Review Method of Participation'
					where
						rfa.episode_id = epi.episode_id
						and
						rfa.text_answer is not null
					)
				
				where
					epi.lac_review_category is not null
					and
					epi.lac_review_participation_code is null;
					
		elsif switch_value = 'Separate Questions;Attendees'
		then
			update dm_c_episodes cepi
				set cepi.lac_review_participation_code = (
					select
						rg.category_value
					from
						report_form_answers rfa
					inner join dm_c_groups rg
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rg.mapped_value
					and
					rg.group_name = 'Child LAC Review Method of Participation (Multiple Questions)'
					where
						rfa.episode_id = cepi.episode_id
						and
						rfa.text_answer is not null
					)
				where
					cepi.lac_review_category is not null
					and
					cepi.lac_review_participation_code is null;					
			
		elsif switch_value in ('Conference Attendees', 'Separate Questions;Attendees')
		then
			update dm_c_episodes cepi
				set cepi.lac_review_participation_code = (
					select
						ca.participation_code
					from
						conference_attendees ca
					inner join episodes ep
					on ca.conference_id = ep.conference_id
					where
						ca.attendee_role = 'SUBJECT'
						and
						ep.id = cepi.episode_id
					)
				where
					cepi.lac_review_category is not null
					and
					cepi.lac_review_participation_code is null;
					
		end if;
	end;
/

create or replace procedure pop_c_swt_lac_review_chair is

	switch_name constant varchar(30) := 'Child LAC Review Chair';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form'
		then
			update dm_c_episodes cepi
				set cepi.lac_review_chair = (
					select
						min(rfa.text_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = '	Child LAC Review Chairs'
					where
						rfa.episode_id = cepi.episode_id
						and
						rfa.text_answer is not null
					)
				where
					cepi.lac_review_category is not null
					and
					cepi.lac_review_chair is null;
					
		elsif switch_value = 'Participant; Attendee'
		then
			update dm_c_episodes cepi
				set cepi.lac_review_chair = (
					select
						ept.local_name
					from
						episode_participants ept
					where
						ept.episode_id = cepi.episode_id
						and
						ept.local_address like dbo.append2('C','&','F Independent Reviewing Officer%')
					)
				where
					cepi.lac_review_category is not null
					and
					cepi.lac_review_chair is null;
					
		elsif switch_value in ('Conference Attendee', 'Participant; Attendee')
		then
			update dm_c_episodes cepi
				set cepi.lac_review_chair = (
					select
						ca.local_name
					from
						conference_attendees ca
					inner join episodes epi
					on ca.conference_id = epi.id
					where
						epi.id = cepi.episode_id
						and
						ca.attendee_role = 'CHAIR'
					)
				where
					cepi.lac_review_category is not null
					and
					cepi.lac_review_chair is null;
					
		end if;
	end;
/

create or replace procedure pop_c_swt_pathway_plan_date is

	switch_name constant varchar(30) := 'Care Pathway Plan Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form'
		then
			update dm_c_episodes epi
			set
				epi.pathway_plan_date = (
					select
						min(rfa.date_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = 'Pathway Plan Date'
					where
						rfa.episode_id = epi.episode_id
						and
						rfa.date_answer is not null
					)
			where
				epi.is_pathway_plan = 'Y'
				and
				epi.pathway_plan_date is null;
			
		elsif switch_value = 'Episode End Date'
		then
			update dm_c_episodes epi
			set
				epi.pathway_plan_date = epi.end_date
			where
				epi.is_pathway_plan = 'Y'
				and
				epi.pathway_plan_date is null;
				
		end if;
	end;
/

create or replace procedure pop_c_swt_s47_enquiry_date is

	switch_name constant varchar(30) := 'Child S47 Enquiry Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form; Episode Start'
		then
			update dm_c_episodes cepi
				set cepi.section_47_enquiry_date = (
					select
						min(rfa.date_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = 'Child Section 47 Date (Document)'
					where
						rfa.date_answer is not null
						and
						rfa.episode_id = cepi.episode_id
					)
				where
					cepi.is_section_47_enquiry = 'Y'
					and
					cepi.section_47_enquiry_date is null;
			
		elsif switch_value = 'Discussion Date; Episode Start'
		then
			update dm_c_episodes cepi
				set cepi.section_47_enquiry_date = (
					select
						esd.discussion_date
					from
						episode_strategy_discussions esd
					where
						esd.episode_id = cepi.episode_id
						and
						esd.discussion_date is not null
					)
				where
					cepi.section_47_enquiry_date is null
					and
					cepi.is_section_47_enquiry = 'Y';
					
		elsif switch_value in ('Episode Start Date', 'Form; Episode Start', 'Discussion Date; Episode Start')
		then
			update dm_c_episodes cepi
				set cepi.section_47_enquiry_date = cepi.start_date
			where
				cepi.section_47_enquiry_date is null
				and
				cepi.is_section_47_enquiry = 'Y';
				
		end if;
		
	end;
					
				
				
					
		
/

create or replace procedure pop_c_swt_start_date is

	switch_name constant varchar(30) := 'Child Episode Start Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form; System'
		then
			update dm_c_episodes epi
				set 
					epi.start_date = dbo.no_time(epi.start_datetime_form),
					epi.start_datetime = epi.start_datetime_form;
				
		elsif switch_value = 'User; System'
		then
			update dm_c_episodes epi
				set 
					epi.start_date = dbo.no_time(epi.start_datetime_user),
					epi.start_datetime = epi.start_datetime_user;
				
		elsif switch_value in ('Form; Episode', 'System', 'User; System')
		then
			update dm_c_episodes epi
				set 
					epi.start_date = dbo.no_time(epi.start_datetime_system),
					epi.start_datetime = epi.start_datetime_system
			where
				epi.start_date is null
				and
				epi.start_datetime is null;
				
		end if;
	
	commit;
	
end;
/

create or replace procedure pop_c_swt_strat_disc_date is

	switch_name constant varchar(30) := 'Child Strategy Discussion Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form; Episode Start'
		then
			update dm_c_episodes cepi
				set cepi.strategy_discussion_date = (
					select
						min(rfa.date_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = 'Child Strategy Discussion Date (Document)'
					where
						rfa.date_answer is not null
						and
						rfa.episode_id = cepi.episode_id
					)
				where
					cepi.is_strategy_discussion = 'Y'
					and
					cepi.strategy_discussion_date is null;
			
		elsif switch_value = 'Discussion Date; Episode Start'
		then
			update dm_c_episodes cepi
				set cepi.strategy_discussion_date = (
					select
						esd.discussion_date
					from
						episode_strategy_discussions esd
					where
						esd.episode_id = cepi.episode_id
						and
						esd.discussion_date is not null
					)
				where
					cepi.is_strategy_discussion = 'Y'
					and
					cepi.strategy_discussion_date is null;
					
		elsif switch_value in ('Episode Start Date', 'Form; Episode Start', 'Discussion Date; Episode Start')
		then
			update dm_c_episodes cepi
				set cepi.strategy_discussion_date = cepi.start_date
			where
					cepi.is_strategy_discussion = 'Y'
					and
					cepi.strategy_discussion_date is null;
				
		end if;
		
	end;
					

/

create or replace procedure pop_c_swt_s47_enquiry_date is

	switch_name constant varchar(30) := 'Child S47 Enquiry Date';
	switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	
	begin
		if switch_value = 'Form; Episode Start'
		then
			update dm_c_episodes cepi
				set cepi.section_47_enquiry_date = (
					select
						min(rfa.date_answer)
					from
						report_form_answers rfa
					inner join dm_c_filters rf
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rf.mapped_value
					and
					rf.filter_name = 'Child Section 47 Date (Document)'
					where
						rfa.date_answer is not null
						and
						rfa.episode_id = cepi.episode_id
					)
				where
					cepi.is_section_47_enquiry = 'Y'
					and
					cepi.section_47_enquiry_date is null;
			
		elsif switch_value = 'Discussion Date; Episode Start'
		then
			update dm_c_episodes cepi
				set cepi.section_47_enquiry_date = (
					select
						esd.discussion_date
					from
						episode_strategy_discussions esd
					where
						esd.episode_id = cepi.episode_id
						and
						esd.discussion_date is not null
					)
				where
					cepi.section_47_enquiry_date is null
					and
					cepi.is_section_47_enquiry = 'Y';
					
		elsif switch_value in ('Episode Start Date', 'Form; Episode Start', 'Discussion Date; Episode Start')
		then
			update dm_c_episodes cepi
				set cepi.section_47_enquiry_date = cepi.start_date
			where
				cepi.section_47_enquiry_date is null
				and
				cepi.is_section_47_enquiry = 'Y';
				
		end if;
		
	end;
					
				
				
					
		
/
