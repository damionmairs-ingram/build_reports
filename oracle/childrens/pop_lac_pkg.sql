create or replace package pop_c_lac is

	SCRIPT	constant varchar(30) := 'Populate_Package';
	STEP	constant varchar(30) := 'Looked after children';
	
	procedure pop_dm_c_legal_statuses;
	procedure pop_dm_c_placements;
	procedure pop_dm_c_placement_details;
	procedure pop_dm_c_placement_carers;
	procedure pop_dm_c_placement_carer_addr;
	
end;
/
create or replace package body pop_c_lac is


	procedure pop_dm_c_legal_statuses is

	begin
		dbo.synchronise_table('dm_c_legal_statuses','(
			select 
				pls.id legal_status_id,
				per.period_of_care_id,
				pls.person_id,
				pls.legal_status,
				pls.start_date,
				-- ''Correct'' end_date unless it''s the last in the block
				case
					when exists (
						select
							''x''
						from
							person_legal_statuses pls1
						where
							pls1.person_id = pls.person_id
							and
							pls1.start_date = pls.end_date
					) then
						pls.end_date - 1
					else
						pls.end_date
				end end_date,
				pls.end_date end_date_overlap,
				type.is_assessment_order,
				type.is_emergency_protection_order,
				type.is_police_protection,
				type.is_respite_care
			from 
				person_legal_statuses pls

			--JOIN: Find legal status type	
			inner join dm_c_legal_status_types type
			on type.legal_status = pls.legal_status

			--JOIN: Find period of care	
			inner join dm_c_periods_of_care per
			on per.person_id = pls.person_id
			and per.start_date <=
				-- ''Correct'' end_date unless it''s the last in the block
				case
					when exists (
						select
							''x''
						from
							person_legal_statuses pls1
						where
							pls1.person_id = pls.person_id
							and
							pls1.start_date = pls.end_date
					) then
						dbo.future(pls.end_date - 1)
					else
						dbo.future(pls.end_date)
				end
			and dbo.future(per.end_date) >= pls.start_date
			)',null,'NO_TEMP','NO_DEBUG');
	end;

	procedure pop_dm_c_placements is
		-- Variables to handle splitting Placements
		v_period_of_care_id dm_c_periods_of_care.period_of_care_id%type;
		v_split_number		dm_c_placements.split_number%type;
		v_split_start_date	dm_c_placements.start_date%type;
		v_split_end_date	dm_c_placements.end_date%type;
		v_gap_start_date	dm_c_placements.start_date%type;
		--
		-- Cache today's date for performance
		v_today date := dbo.no_time(sysdate);
	begin 
	
		execute immediate 'truncate table dm_c_placements_tmp';

		insert /*+APPEND*/ into dm_c_placements_tmp (
			placement_id,
			split_number,
			period_of_care_id,
			start_date,
			end_date,
			end_date_overlap,
			person_id,
			category_of_need,
			placement_type,
			reason_episode_ceased,
			local_end_reason,
			created_on
		)
		select
			details.placement_id,
			0 split_number,
			(
				select
					-- Earliest Period of Care at time of Placement
					poc.period_of_care_id
				from
					dm_c_periods_of_care poc
				where
					poc.person_id = details.person_id
					and
					details.start_date
							between poc.start_date
							and    dbo.future(poc.end_date)
			) period_of_care_id,
			details.start_date,
			details.end_date,
			details.end_date_overlap,
			details.person_id,
			details.category_of_need,		
			details.placement_type,		
			details.reason_episode_ceased,
			details.local_end_reason,
			details.created_on
		from (
			select 
				lap.id placement_id,
				0 split_number,
				lap.start_date,
				-- 'Correct' end_date unless it's the last in the block
				case
					when exists (
						select
							'x'
						from
							looked_after_placements lap1
						where
							lap1.person_id = lap.person_id
							and
							lap1.start_date = lap.end_date
					) then
						lap.end_date - 1
					else
						lap.end_date
				end end_date,
				lap.end_date end_date_overlap,
				lap.person_id,
				lap.category_of_need,		
				lap.placement_code placement_type,		
				lap.reason_episode_ceased,
				lap.local_end_reason,
				lap.created_on
			from 
				looked_after_placements lap
		) details
		;
		--
		commit; -- Due to Direct Path Insert
		--
		-- Placements covering more than one period of care must be split
		-- NB. This can be caused by Respite Care or dodgy data
		--
		for split in (
						select
							-- Period of Care at end of Placement is different
							plc.person_id,
							plc.placement_id,
							plc.split_number,
							plc.start_date,
							plc.end_date,
							plc.period_of_care_id
						from
							dm_c_placements_tmp plc
						--JOIN: Period of care at end of Placement
						left outer join dm_c_periods_of_care poc
						on poc.person_id = plc.person_id
						and dbo.future(plc.end_date)
									between poc.start_date
									and    dbo.future(poc.end_date)
						where
							--CRITERIA: Original Placement lasted longer than one day !
							plc.start_date < dbo.future(plc.end_date_overlap)
							and
							(
								--CRITERIA: Period of Care different at start/end of Placemnt
								coalesce(plc.period_of_care_id, 0) != coalesce(poc.period_of_care_id, 0)
								or
								(
									--CRITERIA: No period of care at start or end, but one in the middle !
									plc.period_of_care_id is null
									and
									poc.period_of_care_id is null
									and
									exists (
										select
											'x'
										from
											dm_c_periods_of_care poc2
										where
											poc2.person_id = plc.person_id
											and
											poc2.start_date <= dbo.future(plc.end_date)
											and
											dbo.future(poc2.end_date) >= plc.start_date
									)
								)
							)
					) loop
			dbms_output.put_line(split.placement_id);
			--
			-- Find end_date of initial split
			if split.period_of_care_id is not null then
				-- Retrieve end date of this period of care
				select
					coalesce(poc.end_date, split.end_date)
				into v_split_end_date
				from
					dm_c_periods_of_care poc
				where
					poc.period_of_care_id = split.period_of_care_id
				;
			else
				-- Find day before next period of care during Placement
				select
					min(poc.start_date) - 1
				into v_split_end_date
				from
					dm_c_periods_of_care poc
				where
					poc.person_id = split.person_id
					and
					poc.start_date <= dbo.future(split.end_date)
				    and
					dbo.future(poc.end_date) >= split.start_date
				;
			end if;
			--
			-- Close initial split
			update dm_c_placements_tmp
			set
				end_date = v_split_end_date,
				end_date_overlap =
					case
						when dbo.future(v_split_end_date) = dbo.future(split.end_date) then
							v_split_end_date
						else
							v_split_end_date + 1
					end
			where
				placement_id = split.placement_id
				and
				split_number = split.split_number
			;
			--
			-- Initialise split counter
			v_split_number := split.split_number;
			--
			while dbo.future(v_split_end_date + 1) < dbo.future(split.end_date) loop
				-- Setup variables for next split
				-- NB. These four variables will change between splits
				v_period_of_care_id := null;
				v_split_number := v_split_number + 1;
				v_split_start_date := v_split_end_date + 1;
				v_split_end_date := split.end_date;
				-- NB. This will match the next v_split_start_date, unless there are gaps to be filled
				v_gap_start_date := v_split_start_date;
				--
				begin
					-- Find next split
					select
						poc.period_of_care_id,
						-- Latest start date
						case
							when poc.start_date >= v_split_start_date then
								poc.start_date
							else
								v_split_start_date
						end start_date,
						-- Earliest end date
						case
							when dbo.future(poc.end_date) <= dbo.future(v_split_end_date) then
								poc.end_date
							else
								v_split_end_date
						end end_date
					into v_period_of_care_id,
						 v_split_start_date,
						 v_split_end_date
					from
						dm_c_periods_of_care poc
					where
						poc.person_id = split.person_id
						and
						--CRITERIA: Overlaps this split
						poc.start_date <= dbo.future(v_split_end_date)
					    and
						dbo.future(poc.end_date) >= v_split_start_date
					;
					--
					-- Check if gap before next period of care
					if v_gap_start_date != v_split_start_date then
						-- Insert new Split to fill gap
						insert into dm_c_placements_tmp (
							placement_id,
							split_number,
							period_of_care_id,
							start_date,
							end_date,
							end_date_overlap,
							person_id,
							category_of_need,
							placement_type,
							reason_episode_ceased,
							local_end_reason,
							created_on
						)
						select
							tmp.placement_id,
							v_split_number,
							null, -- Gap in Periods of Care !
							v_gap_start_date,
							v_split_start_date - 1,
							v_split_start_date,
							tmp.person_id,
							tmp.category_of_need,
							tmp.placement_type,
							tmp.reason_episode_ceased,
							tmp.local_end_reason,
							tmp.created_on
						from
							dm_c_placements_tmp tmp
						where
							tmp.placement_id = split.placement_id
							and
							tmp.split_number = 0
						;
						--
						dbms_output.put_line('... '||to_char(v_split_number));
						--
						v_split_number := v_split_number + 1;
					end if;
					--
					-- Insert new Split
					insert into dm_c_placements_tmp (
						placement_id,
						split_number,
						period_of_care_id,
						start_date,
						end_date,
						end_date_overlap,
						person_id,
						category_of_need,
						placement_type,
						reason_episode_ceased,
						local_end_reason,
						created_on
					)
					select
						tmp.placement_id,
						v_split_number,
						v_period_of_care_id,
						v_split_start_date,
						v_split_end_date,
						case
							when dbo.future(v_split_end_date) = dbo.future(split.end_date) then
								v_split_end_date + 1
							else
								v_split_end_date
						end end_date_overlap,
						tmp.person_id,
						tmp.category_of_need,
						tmp.placement_type,
						tmp.reason_episode_ceased,
						tmp.local_end_reason,
						tmp.created_on
					from
						dm_c_placements_tmp tmp
					where
						tmp.placement_id = split.placement_id
						and
						tmp.split_number = 0
					;
					--
					dbms_output.put_line('... '||to_char(v_split_number));
				exception
					when no_data_found then
						-- No more periods of care during Placement - Insert filler
						insert into dm_c_placements_tmp (
							placement_id,
							split_number,
							period_of_care_id,
							start_date,
							end_date,
							end_date_overlap,
							person_id,
							category_of_need,
							placement_type,
							reason_episode_ceased,
							local_end_reason,
							created_on
						)
						select
							tmp.placement_id,
							v_split_number,
							null, -- Gap in Periods of Care !
							v_split_start_date,
							v_split_end_date,
							case
								when dbo.future(v_split_end_date) = dbo.future(split.end_date) then
									v_split_end_date + 1
								else
									v_split_end_date
							end end_date_overlap,
							tmp.person_id,
							tmp.category_of_need,
							tmp.placement_type,
							tmp.reason_episode_ceased,
							tmp.local_end_reason,
							tmp.created_on
						from
							dm_c_placements_tmp tmp
						where
							tmp.placement_id = split.placement_id
							and
							tmp.split_number = 0
						;
						--
						-- No more splits for this Placement
						exit;
				end;
			end loop;
		end loop;
		--
		-- Identify splits that are fully enclosed by Respite Care
		update dm_c_placements_tmp
		set
			is_fully_respite = 'Y'
		where
			--CRITERIA: No. Days covered by Placement = No. Days covered by Respite
			(coalesce(dm_c_placements_tmp.end_date, v_today) - dm_c_placements_tmp.start_date) = (
				select
					sum(
						-- Earliest end date
						case
							when coalesce(lst.end_date, v_today) <= coalesce(dm_c_placements_tmp.end_date, v_today) then
								coalesce(lst.end_date, v_today)
							else
								coalesce(dm_c_placements_tmp.end_date, v_today)
						end -
						-- Latest start date
						case
							when lst.start_date <= dm_c_placements_tmp.start_date then
								lst.start_date
							else
								dm_c_placements_tmp.start_date
						end
					)
				from
					dm_c_legal_statuses lst
				where
					--CRITERIA: Respite legal status at same time as Placement
					lst.person_id = dm_c_placements_tmp.person_id
					and
					lst.is_respite_care = 'Y'
					and
					lst.start_date <= dbo.future(dm_c_placements_tmp.end_date)
					and
					dbo.future(lst.end_date) >= dm_c_placements_tmp.start_date
			)
		;
		--
		dbo.synchronise_table('dm_c_placements',
			'(select
				tmp.placement_id,
				tmp.split_number,
				tmp.period_of_care_id,
				tmp.start_date,
				tmp.end_date,
				tmp.end_date_overlap,
				tmp.person_id,
				tmp.category_of_need,
				tmp.placement_type,
				tmp.reason_episode_ceased,
				tmp.local_end_reason,
				tmp.created_on,
				tmp.is_fully_respite,
				plc.is_placed_with_parents,
				plc.is_placed_for_adoption,
				plc.is_placed_for_adoption_foster,
				plc.is_placed_in_childrens_home,
				plc.is_placed_with_foster_carers,
				plc.is_placed_indep_foster_agency,
				plc.is_placed_residential_care,
				plc.is_placed_residential_school,
				plc.is_placed_specialist,
				plc.is_placed_temporarily,
				cease.is_adopted
			from
				dm_c_placements_tmp tmp
			inner join dm_c_placement_types plc
			on plc.placement_type = tmp.placement_type
			left outer join dm_c_reasons_episode_ceased cease
			on cease.reason_episode_ceased = tmp.reason_episode_ceased)');
		execute immediate 'truncate table dm_c_placements_tmp';
	end;

	procedure pop_dm_c_placement_details is 

	cursor c_overlapped is
	select
		t.element_detail_id,
		t.period_of_care_id
	from
		dm_c_placement_details t
	where
		t.element_detail_id in (
		select
			t1.element_detail_id
		from
			dm_c_placement_details_tmp t1
		group by
			t1.element_detail_id
		having
			count(1) > 1
		);
		
	cursor c_split (p_element_detail_id varchar) is
	select
		t.period_of_care_id
	from
		dm_c_placement_details_tmp t
	where
		t.element_detail_id = p_element_detail_id;

	v_counter number;


	begin
	
		execute immediate 'truncate table dm_c_placement_details_tmp';

		insert into dm_c_placement_details_tmp (
			element_detail_id,
			start_date,
			end_date,
			episode_id,
			element_start_date,
			element_end_date,	
			ref_element_id,	
			type_id,
			person_id,	
			period_of_care_id,
			carer_id,
			split_number,
			cin_provider_category
		)
		select 
			s.element_detail_id,
			s.start_date,
			s.end_date,
			ep.episode_id,
			s.element_start_date,
			s.element_end_date,	
			s.ref_element_id,	
			s.type_id,
			s.person_id,	
			per.period_of_care_id,
			case
				when s.allocated_person_id is not null
				then dbo.append('P',to_char(s.allocated_person_id))
				else dbo.append('O',to_char(s.supplier_id))
			end carer_id,
			0 split_number,
			s.cin_provider_category
		from 
			dm_c_services s
		inner join dm_c_services_episodes ep
		on s.element_detail_id = ep.element_detail_id

		inner join dm_c_periods_of_care per
		on dbo.future(per.end_date) >= s.start_date
		and
		per.start_date <= dbo.future(s.end_date)
		and
		per.person_id = s.person_id

		where
			s.is_placement = 'Y';
			
		v_counter := 0;
			
		for placement in c_overlapped loop
			
			for split in c_split(placement.element_detail_id) loop
			
				update dm_c_placement_details_tmp
					set split_number = v_counter
				where
					dm_c_placement_details_tmp.period_of_care_id = split.period_of_care_id;
				
				v_counter := v_counter + 1;
				
			end loop;
			
			v_counter := 0;
			
		end loop;

		dbo.synchronise_table('dm_c_placement_details','dm_c_placement_details_tmp',null,'NO_TEMP','NO_DEBUG');

	end;

	procedure pop_dm_c_placement_carers is

	begin

		dbo.synchronise_table('dm_c_placement_carers','(
			select
				dbo.append(''P'',to_char(p.id)) carer_id,
				dbo.f_get_person_name(p.id) name,
				(
				select
					max(tn.phone_number)
				from
					people_telephone_numbers ptn
				inner join telephone_numbers tn
				on ptn.telephone_id = tn.id
				where
					ptn.person_id = p.id
				) telephone,
				null ref_organisation_id,
				p.id ref_person_id
			from
				people p
			where
				dbo.append(''P'',to_char(p.id)) in (
				select
					det.carer_id
				from
					dm_c_placement_details det
				)
				
			union all
				
			select
				dbo.append(''O'',to_char(o.id)) carer_id,
				dbo.f_get_org_name(o.id) name,
				(
				select
					max(tn.phone_number)
				from
					organisations_telephone_nos otn
				inner join telephone_numbers tn
				on otn.telephone_id = tn.id
				where
					otn.organisation_id = o.id
				) telephone,
				o.id ref_organisation_id,
				null ref_person_id
			from
				organisations o
			where
				dbo.append(''O'',to_char(o.id)) in (
				select
					det.carer_id
				from
					dm_c_placement_details det
				)
			)',null,'NO_TEMP','NO_DEBUG');
			
	end;

	procedure pop_dm_c_placement_carer_addr is

	begin
		dbo.synchronise_table('dm_c_placement_carer_addresses','(

			select
				dbo.append(''P'',to_char(ap.person_id)) carer_id,
				dbo.past(ap.start_date) start_date,
				ap.end_date,
				dbo.format_address(a.flat_number,a.building,a.street_number,a.street,a.district,a.town,a.post_code) address,
				a.id ref_addresses_id,
				ap.person_id ref_addresses_people_id,
				null ref_addresses_org_id,
				case
					when a.in_la_area = ''Y''
					then ''IN''
					else ''OUT''
				end ssda903_placement_location,
				a.post_code,
				a.district
			from
				addresses_people ap
			inner join addresses a
			on ap.address_id = a.id

			where
				dbo.append(''P'',to_char(ap.person_id)) in (
				select
					det.carer_id
				from
					dm_c_placement_details det
				)
					

			union all

			select
				dbo.append(''O'',to_char(ao.organisation_id)) carer_id,
				dbo.past(ao.start_date) start_date,
				ao.end_date,
				dbo.format_address(a.flat_number,a.building,a.street_number,a.street,a.district,a.town,a.post_code) address,
				a.id ref_addresses_id,
				null ref_addresses_people_id,
				ao.organisation_id ref_addresses_org_id,
				case
					when a.in_la_area = ''Y''
					then ''IN''
					else ''OUT''
				end ssda903_placement_location,
				a.post_code,
				a.district				
			from
				addresses_organisations ao
			inner join addresses a
			on ao.address_id = a.id

			where
				dbo.append(''O'',to_char(ao.organisation_id)) in (
				select
					det.carer_id
				from
					dm_c_placement_details det
				)

				)',null,'NO_TEMP','NO_DEBUG');
		
	end;
	
end;
/
