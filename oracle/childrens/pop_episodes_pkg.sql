create or replace package pop_c_episodes_pkg is

/*
 * NAME:		pop_c_episodes
 * DESCRIPTION:	populates the child episodes tables
 * HISTORY:		1.0  sf  28/02/2008	initial version
 */

	SCRIPT	constant varchar(30) := 'Populate_Package';
	STEP	constant varchar(30) := 'Episodes';

	procedure pop_c_dm_c_episode_timings;
	procedure pop_c_dm_c_episodes;
	--
	procedure pop_c_episodes_main;

end;
/
create or replace package body pop_c_episodes_pkg is
	--
	-- Switches used by dm_c_episodes --------------------------------------------
	--
	procedure swt_adopt_best_interest is
		switch_name  constant varchar(30) := 'Child Adoption Best Interest';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value = 'Episode Outcome'
		then 
			update dm_c_episodes_tmp
			set
				adoption_best_interest_date = (
					select
						max(eo.entered_date)
					from
						episode_outcomes eo
					inner join dm_c_episode_outcome_types oty
					on oty.outcome_code = eo.code
					where
						eo.episode_id = dm_c_episodes_tmp.episode_id
						and
						oty.is_adoption_best_interest = 'Y'
				)
			where
				is_adoption_best_interest = 'Y'
				and
				adoption_best_interest_date is null
			;
		elsif switch_value = 'Form'
		then
			update dm_c_episodes_tmp
			set
				adoption_best_interest_date = (
					select
						min(ffa.date_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Adoption Best Interest Decisions'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
				)
			where
				is_adoption_best_interest = 'Y'
				and
				adoption_best_interest_date is null
			;
		else
			raise_application_error(-20001, 'Unhandled switch value');
		end if;	
	end;		
	--
	procedure swt_lac_review_date is
		switch_name constant varchar(30) := 'Child LAC Review Date';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value not in ('Form answer; system end date', 'Strategy discussion date', 'Conference; system end date', 'Strategy Old; Conference New')
		then
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
		--
		if switch_value = 'Form answer; system end date'
		then
			update dm_c_episodes_tmp
			set lac_review_date = (
				select
					min(ffa.date_answer)
				from
					dm_c_filter_form_answers ffa
				where
					ffa.filter_name = 'Child LAC Review Date'
					and
					ffa.episode_id = dm_c_episodes_tmp.episode_id
			)
			where
				lac_review_category is not null
				and
				lac_review_date is null
			;
		end if;			
		--
		if switch_value in ('Strategy discussion date', 'Strategy Old; Conference New')
		then
			update dm_c_episodes_tmp
			set lac_review_date = (
				select
					min(esd.discussion_date)
				from
					episode_strategy_discussions esd
				where
					esd.episode_id = dm_c_episodes_tmp.episode_id
			)
			where
				lac_review_category is not null
				and
				lac_review_date is null
			;
		end if;
		--
		if switch_value in ('Conference; system end date', 'Strategy Old; Conference New')
		then
			update dm_c_episodes_tmp
			set lac_review_date = (
				select
					min(coalesce(con.actual_date,con.planned_date))
				from
					conferences con
				inner join episodes on
				episodes.conference_id = con.id
				where
					coalesce(con.actual_date,con.planned_date) is not null
					and 
					episodes.id = dm_c_episodes_tmp.episode_id
			)
			where
				lac_review_category is not null
				and
				lac_review_date is null
			;
		end if;
		--
		if switch_value in ('Form answer; system end date', 'Conference; system end date')
		then
			update dm_c_episodes_tmp
			set
				lac_review_date = end_datetime
			where
				lac_review_category is not null
				and
				lac_review_date is null
			;
		end if;
	end;
	--
	procedure swt_end_date is
		switch_name constant varchar(30) := 'Child Episode End Date';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value not in ('Form; System-generated', 'System-generated') then
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
		--
		if switch_value = 'Form; System-generated' then
			update dm_c_episodes_tmp
			set 
				end_date = dbo.no_time(end_datetime_form),
				end_datetime = end_datetime_form
			where
				end_datetime_form is not null
			;
		end if;
		--
		if switch_value in ('Form; System-generated', 'System-generated')
		then
			update dm_c_episodes_tmp
			set 
				end_date = dbo.no_time(end_datetime_system),
				end_datetime = end_datetime_system
			where
				end_datetime is null
			;
		end if;
	end;
	--
	procedure swt_lac_participation is
		switch_name constant varchar(30) := 'Child LAC Review Participation';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value not in ('Form', 'Conference Attendees', 'Separate Questions;Attendees') then
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
		--
		if switch_value = 'Form'
		then
			update dm_c_episodes_tmp
			set
				lac_review_participation_code = (
					select
						min(ffa.text_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child LAC Review Method of Participation'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
						and
						ffa.text_answer is not null
				)
			where
				lac_review_category is not null
				and
				lac_review_participation_code is null
			;
		end if;
		--
		if switch_value = 'Separate Questions;Attendees'
		then
			update dm_c_episodes_tmp
			set
				lac_review_participation_code = (
					select
						rg.category_value
					from
						report_form_answers rfa
					inner join dm_c_groups rg
					on dbo.append2(rfa.form_type_id_code,'->',rfa.section_item_tag_id) = rg.mapped_value
					and
					rg.group_name = 'Child LAC Review Method of Participation (Multiple Questions)'
					where
						rfa.episode_id = dm_c_episodes_tmp.episode_id
						and
						rfa.text_answer is not null
				)
			where
				lac_review_category is not null
				and
				lac_review_participation_code is null
			;
		end if;	
		--
		if switch_value in ('Conference Attendees', 'Separate Questions;Attendees')
		then
			update dm_c_episodes_tmp
			set
				lac_review_participation_code = (
					select
						ca.participation_code
					from
						conference_attendees ca
					inner join episodes ep
					on ca.conference_id = ep.conference_id
					where
						ca.attendee_role = 'SUBJECT'
						and
						ep.id = dm_c_episodes_tmp.episode_id
				)
			where
				lac_review_category is not null
				and
				lac_review_participation_code is null
			;
		end if;
	end;
	--
	procedure swt_lac_review_chair is
		switch_name constant varchar(30) := 'Child LAC Review Chair';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value not in ('Conference Attendee', 'Form', 'Participant; Attendee')
		then
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
		--
		if switch_value = 'Form'
		then
			update dm_c_episodes_tmp
			set
				lac_review_chair = (
					select
						min(ffa.text_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child LAC Review Chairs'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
						and
						ffa.text_answer is not null
				)
			where
				lac_review_category is not null
				and
				lac_review_chair is null
			;
		end if;
		--
		if switch_value = 'Participant; Attendee'
		then
			-- NB. This is for hsitorical data only.
			--     The local_address is free-text, and should have been tidied-up to match the search string
			update dm_c_episodes_tmp
			set
				lac_review_chair = (
					select
						ept.local_name
					from
						episode_participants ept
					where
						ept.episode_id = dm_c_episodes_tmp.episode_id
						and
						ept.local_address like 'C&F Independent Reviewing Officer%'
				)
			where
				lac_review_category is not null
				and
				lac_review_chair is null
			;
		end if;
		--
		if switch_value in ('Conference Attendee', 'Participant; Attendee')
		then
			update dm_c_episodes_tmp
			set
				lac_review_chair = (
					select
						ca.local_name
					from
						conference_attendees ca
					inner join episodes epi
					on ca.conference_id = epi.id
					where
						epi.id = dm_c_episodes_tmp.episode_id
						and
						ca.attendee_role = 'CHAIR'
			)
			where
				lac_review_category is not null
				and
				lac_review_chair is null
			;
		end if;
	end;
	--
	procedure swt_strat_disc_date is
		switch_name constant varchar(30) := 'Child Strategy Discussion Date';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value not in ('Episode Start Date', 'Discussion Date; Episode Start', 'Form; Episode Start')
		then
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
		--
		if switch_value = 'Form; Episode Start'
		then
			update dm_c_episodes_tmp
			set
				strategy_discussion_date = (
					select
						min(ffa.date_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Strategy Discussion Date (Document)'
						and
						ffa.date_answer is not null
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
				)
			where
				is_strategy_discussion = 'Y'
				and
				strategy_discussion_date is null
			;
		end if;
		--
		if switch_value = 'Discussion Date; Episode Start'
		then
			update dm_c_episodes_tmp
			set
				strategy_discussion_date = (
					select
						esd.discussion_date
					from
						episode_strategy_discussions esd
					where
						esd.episode_id = dm_c_episodes_tmp.episode_id
						and
						esd.discussion_date is not null
				)
			where
				is_strategy_discussion = 'Y'
				and
				strategy_discussion_date is null
			;
		end if;
		--
		if switch_value in ('Episode Start Date', 'Form; Episode Start', 'Discussion Date; Episode Start')
		then
			update dm_c_episodes_tmp
			set
				strategy_discussion_date = start_date
			where
				is_strategy_discussion = 'Y'
				and
				strategy_discussion_date is null
			;
		end if;
	end;
	--
	procedure swt_start_date is
		switch_name constant varchar(30) := 'Child Episode Start Date';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin 
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--	
		if switch_value = 'System-generated'
		then
			update dm_c_episodes_tmp
			set
				start_date = dbo.no_time(start_datetime_system),
				start_datetime = start_datetime_system,
				weighted_start_datetime = dbo.to_weighted_start(start_datetime_system, episode_id) 
			where
				--CRITERIA: Start Datetime has changed
				start_datetime != start_datetime_system
			;
		elsif switch_value = 'User-entered'
		then
			update dm_c_episodes_tmp
			set
				start_date = dbo.no_time(start_datetime_user),
				start_datetime = start_datetime_user,
				weighted_start_datetime = dbo.to_weighted_start(start_datetime_user, episode_id) 
			where
				--CRITERIA: Start Datetime has changed
				start_datetime != start_datetime_user
			;
		elsif switch_value = 'Form; System-generated'
		then
			update dm_c_episodes_tmp
			set
				start_date = dbo.no_time(coalesce(start_datetime_form, start_datetime_system)),
				start_datetime = coalesce(start_datetime_form, start_datetime_system),
				weighted_start_datetime = dbo.to_weighted_start(coalesce(start_datetime_form, start_datetime_system), episode_id) 
			where
				--CRITERIA: Start Datetime has changed
				start_datetime != coalesce(start_datetime_form, start_datetime_system)
			;
		elsif switch_value = 'Form; User-entered'
		then
			update dm_c_episodes_tmp
			set
				start_date = dbo.no_time(coalesce(start_datetime_form, start_datetime_user)),
				start_datetime = coalesce(start_datetime_form, start_datetime_user),
				weighted_start_datetime = dbo.to_weighted_start(coalesce(start_datetime_form, start_datetime_user), episode_id) 
			where
				--CRITERIA: Start Datetime has changed
				start_datetime != coalesce(start_datetime_form, start_datetime_user)
			;			
		else
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
	end;
	--
	procedure swt_pathway_plan_date is
		switch_name constant varchar(30) := 'Child Pathway Plan Date';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--	
		if switch_value = 'Form'
		then
			update dm_c_episodes_tmp
			set
				pathway_plan_date = (
					select
						min(ffa.date_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Pathway Plan Date (Document)'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
						and
						ffa.date_answer is not null
				)
			where
				is_pathway_plan = 'Y'
				and
				pathway_plan_date is null
			;
		elsif switch_value = 'Episode End Date'
		then
			update dm_c_episodes_tmp
			set
				pathway_plan_date = end_date
			where
				is_pathway_plan = 'Y'
				and
				pathway_plan_date is null
			;
		else
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
	end;
	--
	procedure swt_s47_enquiry_date is
		switch_name constant varchar(30) := 'Child S47 Enquiry Date';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--
		if switch_value not in ('Episode Start Date', 'Discussion Date; Episode Start', 'Form; Episode Start')
		then
			raise_application_error(-20001, 'Unhandled switch value');
		end if;
		--
		if switch_value = 'Form; Episode Start'
		then
			update dm_c_episodes_tmp
			set
				section_47_enquiry_date = (
					select
						min(ffa.date_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Section 47 Date (Document)'
						and
						ffa.date_answer is not null
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
				)
			where
				is_section_47_enquiry = 'Y'
				and
				section_47_enquiry_date is null
			;
		end if;	
		--
		if switch_value = 'Discussion Date; Episode Start'
		then
			update dm_c_episodes_tmp
			set
				section_47_enquiry_date = (
					select
						esd.discussion_date
					from
						episode_strategy_discussions esd
					where
						esd.episode_id = dm_c_episodes_tmp.episode_id
						and
						esd.discussion_date is not null
				)
			where
				section_47_enquiry_date is null
				and
				is_section_47_enquiry = 'Y'
			;
		end if;
		--
		if switch_value in ('Episode Start Date', 'Form; Episode Start', 'Discussion Date; Episode Start')
		then
			update dm_c_episodes_tmp
			set
				section_47_enquiry_date = start_date
			where
				section_47_enquiry_date is null
				and
				is_section_47_enquiry = 'Y'
			;
		end if;
	end;
	--
	procedure swt_adopt_match_date is
		switch_name  constant varchar(30) := 'Child Adoption Match';
		switch_value constant varchar(30) := dbo.switch_mapping(switch_name);
	begin
		dbo.log_switch(SCRIPT, step, switch_name, switch_value);
		--	
		if switch_value = 'Episode Outcome'
		then 
			update dm_c_episodes_tmp
			set
				adoption_match_date = (
					select
						max(eo.entered_date)
					from
						dm_c_episode_outcomes eo
					where
						eo.episode_id = dm_c_episodes_tmp.episode_id
						and
						eo.is_adoption_match = 'Y'
				)
			where
				is_adoption_match = 'Y'
				and
				adoption_match_date is null
			;
		elsif switch_value = 'Form'
		then
			update dm_c_episodes_tmp
			set
				adoption_match_date = (
					select
						min(ffa.date_answer)
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Adoption Matches'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
						and
						ffa.date_answer is not null
				)
				where
					is_adoption_match = 'Y'
					and
					adoption_match_date is null
			;
		else
			raise_application_error(-20001, 'Unhandled switch value');
		end if;	
	end;
	--
	-- Episodes
	--
	procedure pop_c_dm_c_episodes is 
	begin
		dbo.log_header(SCRIPT, 'Episodes Main');
		dbo.drop_table_indexes(SCRIPT, STEP, 'dm_c_episodes', 'IF_EMPTY');
	
		dbo.truncate_tables(SCRIPT, step, 'dm_c_episodes_tmp');
			
		dbo.log_line(SCRIPT, STEP, 'Insert into dm_c_episodes_tmp');
		insert /*+APPEND*/ into dm_c_episodes_tmp (
			person_id,
			episode_id,
			workflow_id,
			workflow_depth,
			episode_type, 
			start_date,
			start_datetime,
			start_datetime_system,
			start_datetime_user,
			weighted_start_datetime,
			end_datetime_system,
			end_datetime_last_worked_on,
			completed,
			cancelled,
			worker_id,
			is_adoption_best_interest,
			is_adoption_match,
			is_child_referral,
			is_cin_episode,
			is_cin_post_adoption_support,
			is_core_assessment,
			core_assessment_trigger_id,
			is_initial_assessment,
			is_pathway_plan,
			is_pep_review,
			is_priv_fostering_notification,
			is_section_47_enquiry,
			is_strategy_discussion,
			lac_review_category,
			visit_category
		)
		select
			epi.subject person_id,
			epi.id episode_id,
			wrf.workflow_id,
			wrf.workflow_depth,
			epi.type episode_type,
			dbo.no_time(epi.start_date) start_date,
			epi.start_date start_datetime,
			case
				when seg.segment_code = 'REFERRAL' then
					epi.work_start_date
				else
					epi.start_date
			end	start_datetime_system,
			case
				when seg.segment_code = 'REFERRAL' then
					epi.start_date
				else
					epi.work_start_date
			end	start_datetime_user,
			dbo.to_weighted_start(epi.start_date, epi.id) weighted_start_datetime,
			epi.end_date end_datetime_system,
			epi.last_worked_on end_datetime_last_worked_on,
			epi.completed completed,
			'N' cancelled,
			epi.done_by_worker_id worker_id,
			type.is_adoption_best_interest,
			type.is_adoption_match,
			type.is_child_referral,
			type.is_cin_episode,
			type.is_cin_post_adoption_support,
			type.is_core_assessment,
			coalesce((
				select
					eo.episode_id
				from
					episode_outcomes eo
				where
					eo.new_episode_id = epi.id
			),epi.id)
			core_assessment_trigger_id,
			type.is_initial_assessment,
			type.is_pathway_plan,
			type.is_pep_review,
			type.is_priv_fostering_notification,
			type.is_section_47_enquiry,
			type.is_strategy_discussion,
			type.lac_review_category,
			type.visit_category
		from
			episodes epi
		--JOIN: Children only
		inner join dm_c_children chi
		on chi.person_id = epi.subject
		--JOIN: Episode Type
		inner join dm_c_episode_types type
		on epi.type = type.episode_type
		--JOIN: Position within Workflow 
		inner join dm_workflow_episodes wrf
		on wrf.episode_id = epi.id
		--JOIN: Segment to determine start_date columns
		left outer join episode_segments seg
		on seg.episode_type = epi.type
		and seg.segment_code = 'REFERRAL';
		--
		/* Insert the episode start date from a form */
		dbo.log_line(SCRIPT, STEP, 'Start Date from a form');
		if dbo.f_report_filter_mapped('Child Episode Start Date (Document)') = 'Y'
		then
			update dm_c_episodes
			set
				start_datetime_form = (
					select
						dbo.no_time(min(ffa.date_answer))
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Episode Start Date (Document)'
						and
						ffa.episode_id = dm_c_episodes.episode_id
						and
						ffa.date_answer is not null
				)
			where
				--CRITERIA: Episode has Form(s) attached
				exists (
					select
						'x'
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Episode Start Date (Document)'
						and
						ffa.episode_id = dm_c_episodes.episode_id
						and
						ffa.date_answer is not null
				)
			;
		end if;
		--
		/* Insert the episode end date from a form */
		if dbo.f_report_filter_mapped('Child Episode End Date (Document)') = 'Y'
		then
			dbo.log_line(SCRIPT, STEP, 'End Date from a form');
			update dm_c_episodes_tmp
			set
				end_datetime_form = (
					select
						dbo.no_time(min(ffa.date_answer))
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Episode End Date (Document)'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
						and
						ffa.date_answer is not null
				)
			where
				exists (
					select
						'x'
					from
						dm_c_filter_form_answers ffa
					where
						ffa.filter_name = 'Child Episode End Date (Document)'
						and
						ffa.episode_id = dm_c_episodes_tmp.episode_id
						and
						ffa.date_answer is not null
				)
			;
		end if;
		--
		/* Insert the last outcome date */
		dbo.log_line(SCRIPT, STEP, 'Last Outcome Date');
		update dm_c_episodes_tmp
		set
			end_datetime_entered_date = (
				select
					max(eo.entered_date)
				from
					episode_outcomes eo
				where
					eo.episode_id = dm_c_episodes_tmp.episode_id
			)
		;
		--
		/* The episode is cancelled if it has any outcome where the reason is the 'ERR' error code */
		dbo.log_line(SCRIPT, STEP, 'Episode Cancelled (ERR Code)');
		update dm_c_episodes_tmp
		set
			cancelled = 'Y'
		where
			cancelled = 'N'
			and
			episode_id in (
				select
					eo.episode_id
				from
					episode_outcomes eo
				where
					--CRITERIA: Cancelled
					eo.reason = 'ERR'
			)
		;
		--
		/* The episode is cancelled if there is only one outcome and that outcome is marked 'nonaction' */
		dbo.log_line(SCRIPT, STEP, 'Episode Cancelled (Non-action)');
		update	dm_c_episodes_tmp
		set cancelled = 'Y'
		where
			cancelled = 'N'
			and
			--CRITERIA: Only one Outcome ...
			1 = (
				select
					count(1)
				from
					episode_outcomes eo
				where
					eo.episode_id = dm_c_episodes_tmp.episode_id
			)
			and
			--CRITERIA: ... and that is marked 'nonaction'
			exists (
				select
					'x'
				from
					episode_outcomes eo
				where
					eo.episode_id = dm_c_episodes_tmp.episode_id
					and
					eo.nonaction_date is not null
			)
		;
		--
		/* Populate the start date column based based on switch mapping */
		swt_start_date;
		--
		/* Populate the end date column based based on switch mapping */
		swt_end_date;
		--
		/* Populate the Adoption Best Interest Date */
		swt_adopt_best_interest;
		--
		/* Populate the Adoption Match Date */
		swt_adopt_match_date;
		--
		/* Populate the Pathway Plan Date */
		swt_pathway_plan_date;
		--
		/* Populate the LAC Review Date */
		swt_lac_review_date;
		--
		dbo.log_line(SCRIPT, STEP, 'Populate next LAC Review Date');
		update dm_c_episodes_tmp
		set
			lac_next_review_date = (
				select
					min(nep.lac_review_date)
				from
					dm_c_episodes nep
				where
					--The first lac review date >= the current review,
					--in the same workflow, but a different episode.
					nep.workflow_id = dm_c_episodes_tmp.workflow_id
					and
					nep.lac_review_date >= dm_c_episodes_tmp.lac_review_date
					and
					nep.episode_id != dm_c_episodes_tmp.episode_id
				)
		where
			lac_review_category is not null
		;
		--
		dbo.log_line(SCRIPT, STEP, 'Categorise CP Conferences');
		-- Verify report group and logic <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		update dm_c_episodes_tmp
		set
			cp_conference_category = (
				select
					rg.category_value
				from
					dm_c_groups rg
				where
					rg.group_name = 'Child Conference Category'
					and
					dm_c_episodes_tmp.episode_type = rg.mapped_value
			)
		where
			--CRITERIA: Where mapped as a CP Conference
			episode_type in (
				select
					rg.mapped_value
				from
					dm_c_groups rg
				where
					rg.group_name = 'Child Conference Category'
			)
		;
		--
		/* Populate LAC Participation Code */
		swt_lac_participation;
		--
		/* Populate LAC Conference Chair */
		swt_lac_review_chair;
		--
		/* Populate the section_47 enquiry date if is_section_47_enquiry = 'Y' */
		swt_s47_enquiry_date;
		--
		/* Populate the strategy discussion date column based based on switch mapping */	
		swt_strat_disc_date;
		--
		dbo.log_line(SCRIPT, STEP, 'Identify Worker''s Latest Team during Episode');
		-- NB. This is dependent upon the Episode's start/end dates
		update dm_c_episodes_tmp
		set
			team_id = (
				select
					wro.org_id
				from
					dm_worker_roles wro
				where
					--CRITERIA: Roles during Episode
					wro.worker_id = dm_c_episodes_tmp.worker_id
					and
					wro.start_date <= dbo.future(dm_c_episodes_tmp.end_date)
					and
					dbo.future(wro.end_date) >= dm_c_episodes_tmp.start_date
					and
					--CRITERIA: Latest Role during Episode
					dbo.to_weighted_start(wro.start_date, wro.id) = (
						select
							max(dbo.to_weighted_start(wro2.start_date, wro2.id))
						from
							dm_worker_roles wro2
						where
							wro2.worker_id = dm_c_episodes_tmp.worker_id
							and
							wro2.start_date <= dbo.future(dm_c_episodes_tmp.end_date)
							and
							dbo.future(wro2.end_date) >= dm_c_episodes_tmp.start_date
					)
			)
		where
			--CRITERIA: Worker has a Role during Episode (they all should have !)
			exists (
				select
					'x'
				from
					dm_worker_roles wro
				where
					wro.worker_id = dm_c_episodes_tmp.worker_id
					and
					wro.start_date <= dbo.future(dm_c_episodes_tmp.end_date)
					and
					dbo.future(wro.end_date) >= dm_c_episodes_tmp.start_date
			)
		;
		--
		dbo.log_line(SCRIPT, STEP, 'Synchronise dm_c_episodes');
		dbo.synchronise_table('dm_c_episodes','dm_c_episodes_tmp');
		dbo.truncate_tables(SCRIPT, step, 'dm_c_episodes_tmp');
		--
		dbo.log_tail(SCRIPT);
	end;
	--
	procedure pop_c_dm_c_episode_timings is
	
		begin
			dbo.log_header(SCRIPT, 'Episode Timings');
			dbo.drop_table_indexes(SCRIPT, STEP, 'dm_c_episode_timings', 'IF_EMPTY');
			dbo.truncate_tables(SCRIPT, step, 'dm_c_episode_timings_tmp');
			--
			dbo.log_line(SCRIPT, STEP, 'Populate Build table with Episode Timings');
			insert /*+ APPEND */ into dm_c_episode_timings_tmp (
				episode_id,
				trigger_episode_id,
				workflow_id,
				person_id,
				is_core_assessment,
				is_initial_assessment,
				is_initial_cp_conference,
				is_section_47_enquiry,
				trigger_is_referral,
				trigger_is_initial_assessment,
				trigger_is_strategy_discussion,
				trigger_datetime,
				target_working_days,
				target_end_datetime,
				end_datetime,
				is_within_target
			)
			select
				/* Referral --> Initial and Core assessment */
				cepi.episode_id,
				trig.episode_id trigger_episode_id,
				cepi.workflow_id,
				cepi.person_id,
				cepi.is_core_assessment,
				cepi.is_initial_assessment,
				case
					when upper(cepi.cp_conference_category) = 'INITIAL' then 'Y'
					else 'N'
				end is_initial_cp_conference,
				cepi.is_section_47_enquiry,
				coalesce(trig.is_child_referral,'N') trigger_is_referral,
				trig.is_initial_assessment trigger_is_initial_assessment,
				trig.is_strategy_discussion trigger_is_strategy_discussion,
				trig.start_datetime trigger_datetime,
				case
					when cepi.is_initial_assessment = 'Y' then 7
					when cepi.is_core_assessment = 'Y' then 42
				end target_working_days,
				dbo.f_add_working_days_child(trig.start_datetime,
					case
						when cepi.is_initial_assessment = 'Y' then 7
						when cepi.is_core_assessment = 'Y' then 42
					end) target_end_datetime,
				cepi.end_datetime,
				case
					when cepi.end_datetime <= dbo.f_add_working_days_child(trig.start_datetime,
					case
						when cepi.is_initial_assessment = 'Y' then 7
						when cepi.is_core_assessment = 'Y' then 42
					end)
					then 'Y'
					else 'N'
				end is_within_target
			from
				dm_c_episodes cepi
			--JOIN: Triggering Referral
			inner join dm_c_episodes trig
			on trig.person_id = cepi.person_id
			and trig.workflow_id = cepi.workflow_id
			and trig.is_child_referral = 'Y'
			and trig.workflow_depth = (
				select /*+ NO_UNNEST */
					max(t.workflow_depth)
				from
					dm_c_episodes t
				where
					t.workflow_id = trig.workflow_id
					and
					t.person_id = trig.person_id
					and
					t.is_child_referral = 'Y'
					and
					t.workflow_depth < cepi.workflow_depth
			)
			where
				--CRITERIA: Initial and Core assessments
				(
					cepi.is_core_assessment = 'Y'
					or
					cepi.is_initial_assessment = 'Y'
				)
			union all
			select
				/* Initial Assessment --> Core Assessment */
				cepi.episode_id,
				trig.episode_id,
				cepi.workflow_id,
				cepi.person_id,
				cepi.is_core_assessment,
				cepi.is_initial_assessment,
				case
					when upper(cepi.cp_conference_category) = 'INITIAL' then 'Y'
					else 'N'
				end is_initial_cp_conference,
				cepi.is_section_47_enquiry,
				coalesce(trig.is_child_referral,'N') trigger_is_referral,
				trig.is_initial_assessment trigger_is_initial_assessment,
				trig.is_strategy_discussion trigger_is_strategy_discussion,
				trig.start_datetime trigger_datetime,
				35 target_working_days,
				dbo.f_add_working_days_child(trig.start_datetime,35) target_end_datetime,
				cepi.end_datetime,
				case
					when cepi.end_datetime <= dbo.f_add_working_days_child(trig.start_datetime,35)
					then 'Y'
					else 'N'
				end is_within_target
			from
				dm_c_episodes cepi
			--JOIN: Triggering Initial Assessment
			inner join dm_c_episodes trig
			on trig.person_id = cepi.person_id
			and trig.workflow_id = cepi.workflow_id
			and trig.is_initial_assessment = 'Y'
			and trig.workflow_depth = (
				select /*+ NO_UNNEST */
					max(t.workflow_depth)
				from
					dm_c_episodes t
				where
					t.workflow_id = trig.workflow_id
					and
					t.person_id = trig.person_id
					and
					t.is_initial_assessment = 'Y'
					and
					t.workflow_depth <= cepi.workflow_depth
				)
			where
				--CRITERIA: Core Assessments
				cepi.is_core_assessment = 'Y'
			union all
			select
				/* Strategy Discussion --> Core Assessment */
				cepi.episode_id,
				trig.episode_id,
				cepi.workflow_id,
				cepi.person_id,
				cepi.is_core_assessment,
				cepi.is_initial_assessment,
				case
					when upper(cepi.cp_conference_category) = 'INITIAL' then 'Y'
					else 'N'
				end is_initial_cp_conference,
				cepi.is_section_47_enquiry,
				coalesce(trig.is_child_referral,'N') trigger_is_referral,
				trig.is_initial_assessment trigger_is_initial_assessment,
				trig.is_strategy_discussion trigger_is_strategy_discussion,
				trig.start_datetime trigger_datetime,
				35 target_working_days,
				dbo.f_add_working_days_child(trig.start_datetime,35) target_end_datetime,
				cepi.end_datetime,
				case
					when cepi.end_datetime <= dbo.f_add_working_days_child(trig.start_datetime,35)
					then 'Y'
					else 'N'
				end is_within_target
			from
				dm_c_episodes cepi
			--JOIN: Triggering Strategy Discussion
			inner join dm_c_episodes trig
			on trig.person_id = cepi.person_id
			and trig.workflow_id = cepi.workflow_id
			and trig.is_strategy_discussion = 'Y'
			and trig.workflow_depth = (
				select /*+ NO_UNNEST */
					max(t.workflow_depth)
				from
					dm_c_episodes t
				where
					t.workflow_id = trig.workflow_id
					and
					t.person_id = trig.person_id
					and
					t.is_strategy_discussion = 'Y'
					and
					t.weighted_start_datetime <= cepi.weighted_start_datetime
				)
			where
				--CRITERIA: Core Assessment
				cepi.is_core_assessment = 'Y'
			union all
			select
				/* Section 47 --> Initial CP */
				cepi.episode_id,
				trig.episode_id,
				cepi.workflow_id,
				cepi.person_id,
				cepi.is_core_assessment,
				cepi.is_initial_assessment,
				case
					when upper(cepi.cp_conference_category) = 'INITIAL' then 'Y'
					else 'N'
				end is_initial_cp_conference,
				cepi.is_section_47_enquiry,
				coalesce(trig.is_child_referral,'N') trigger_is_referral,
				trig.is_initial_assessment trigger_is_initial_assessment,
				trig.is_strategy_discussion trigger_is_strategy_discussion,
				trig.start_datetime trigger_datetime,
				15 target_working_days,
				dbo.f_add_working_days_child(trig.start_datetime,15) target_end_datetime,
				cepi.end_datetime,
				case
					when cepi.end_datetime <= dbo.f_add_working_days_child(trig.start_datetime,15)
					then 'Y'
					else 'N'
				end is_within_target
			from
				dm_c_episodes cepi
			--JOIN: Triggering Section 47
			inner join dm_c_episodes trig
			on trig.person_id = cepi.person_id
			and trig.workflow_id = cepi.workflow_id
			and trig.is_section_47_enquiry = 'Y'
			and trig.workflow_depth = (
				select /*+ NO_UNNEST */
					max(t.workflow_depth)
				from
					dm_c_episodes t
				where
					t.workflow_id = trig.workflow_id
					and
					t.person_id = trig.person_id
					and
					t.is_section_47_enquiry = 'Y'
					and
					t.weighted_start_datetime <= cepi.weighted_start_datetime
			)
			where
				--CRITERIA: Initial CP
				upper(cepi.cp_conference_category) = 'INITIAL'
			;
			--
			commit; -- Direct Path Insert
			--	
			dbo.log_line(SCRIPT, STEP, 'Synchronise dm_c_episode_timings');
			dbo.synchronise_table('dm_c_episode_timings', 'dm_c_episode_timings_tmp');
			dbo.truncate_tables(SCRIPT, step, 'dm_c_episode_timings_tmp');
			
			dbo.log_tail(SCRIPT);

	end;
	--
	procedure pop_c_episodes_main is
	begin
		pop_c_episodes_pkg.pop_c_dm_c_episodes;
		pop_c_episodes_pkg.pop_c_dm_c_episode_timings;
	end;

end;
/
