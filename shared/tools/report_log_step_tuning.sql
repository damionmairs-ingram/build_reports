select
/*
 * NAME:		report_logs_viewer.sql
 * DESCRIPTION:	Displays logs from p_populate_report_tables
 * HISTORY:		1.0 djm 03/03/2008	YKAP 20523: Initial Version
 */
	summary.step          "Step",
	summary.start_time    "Start Time",
	summary.end_time      "End Time",
	summary.seconds,
	summary.hours_minutes "Hours:Minutes"
from (
	select
		rlo."Step"                       step,
		min(rlo."TimeStamp")             start_time,
		max(rlo."TimeStamp")             end_time,
		sum(rlo."Secs")                  seconds,
		dbo.to_hrs_mins(sum(rlo."Secs")) hours_minutes
	from
		report_logs_vw rlo
	where
		rlo.routine = 'Populate_Package'
		and
		rlo."Step" is not null
	group by
		rlo."Step"
) summary
order by
	summary.seconds desc