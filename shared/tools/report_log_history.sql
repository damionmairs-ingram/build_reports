with specific_run as (
/*
 * NAME:		report_logs_viewer.sql
 * DESCRIPTION:	Displays historic logs from Populate Script
 * HISTORY:		1.0 djm 23/03/2006	Inititial Version
 *         		1.1 djm 15/05/2006	Include Populate_Package
 *         		1.2 djm 12/06/2006	SQLServer-friendly
 *         		1.3 djm 13/12/2006	Allow for running over midnight
 *         		2.0 djm 16/11/2007	ykap 18760: Converted to use Report_Logs_vw
 *         		2.1 djm 14/04/2010	FRA-3520: Ensure FWi does not remove the time-component
 *         		3.0 djm 21/02/2011	FRA-7583: Redesigned for log history
 */
 	-- Identify specific run start/end
	select
		rlh.routine routine,
		rlh.time_stamp run_started,
		(
			-- Start of next run, defaults to 'now'
			select
				coalesce(min(next_run.time_stamp), dbo.now())
			from
				report_log_history next_run
			where
				--CRITERIA: Populate script
				next_run.routine = rlh.routine
				and
				--CRITERIA: First step
				next_run.order_by = 1
				and
				--CRITERIA: After this run
				next_run.time_stamp > rlh.time_stamp
		) next_run_started
	from
		report_log_history rlh
	where
		--CRITERIA: Populate script
		rlh.routine = 'Populate_Package'
		and
		--CRITERIA: First step
		rlh.order_by = 1
		and
		--CRITERIA: Latest on/before specified date
		rlh.time_stamp = (
			select
				max(rlh2.time_stamp)
			from
				report_log_history rlh2
			where
				--CRITERIA: Populate script
				rlh2.routine = 'Populate_Package'
				and
				--CRITERIA: First step
				rlh2.order_by = 1
--				and
				--CRITERIA: Specific date
--				rlh2.time_stamp <= dbo.make_datetime('15-Aug-2011 20:05:00')
		)
),
-- 2nd With Clause to return log lines
run_logs as (
	-- All logs for specified run
	select
		specific_run.run_started,
		rlh.time_stamp,
		rlh.routine,
		rlh.order_by,
		rlh.step,
		rlh.line
	from
		specific_run
	--JOIN: Run's log entries
	inner join report_log_history rlh
	on rlh.routine = specific_run.routine
	and rlh.time_stamp >= specific_run.run_started
	and rlh.time_stamp < specific_run.next_run_started
),
run_log_derivations as (
	select
		line.run_started,
		line.routine,
		line.order_by,
		line.step,
		line.line,
		line.time_stamp,
		next_line.time_stamp next_time_stamp,
		-- Derive duration in Secs
		case
			when coalesce(line.step, '') = '' then
				cast(null as numeric(9))
			when dbo.secs(line.time_stamp) <= dbo.secs(coalesce(next_line.time_stamp, dbo.now())) then
				-- Same side of midnight
				(dbo.secs(coalesce(next_line.time_stamp, dbo.now())) - dbo.secs(line.time_stamp))
			else
				-- Straddles midnight; 86400 = secs per day
				86400 + (dbo.secs(coalesce(next_line.time_stamp, dbo.now())) - dbo.secs(line.time_stamp))
		end secs,
		-- Derive status
		case
			when line.line  like 'Error:%'
			or   next_line.line like 'Error:%' then
				'Error'
			when line.line  like 'Warning:%'
			or   next_line.line like 'Warning:%'then
				'Warning'
			when next_line.time_stamp is null
			and  coalesce(line.step, '') != '' then
				'Time so far (if still running)'
		end status
	from
		run_logs line
	--JOIN: Next log_line, for timing info
	left outer join run_logs next_line
	on next_line.order_by = line.order_by + 1
)
select
	rld.time_stamp	"TimeStamp",
	rld.secs		"Secs",
	rld.status		"Status",
	rld.step		"Step",
	rld.line		"Line"
from
	run_log_derivations rld
-- Optional filters
--where
	--rld."Status" is not null -- Rows without a null status
	--rld."Status" = 'Error' -- Errors only
order by
	-- Perfomance testing:
	--rld.secs desc
	-- Chronological, oldest first:
	--rld.order_by
	-- Reverse Chronological, most recent first:
	-- (Can use PL/SQL Developer's Auto Refresh)
	rld.order_by desc 

