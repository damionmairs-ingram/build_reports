-- NB. Failed/aborted runs should have null 'ended', etc.
select
	summary.day_of_week,
	summary.started,
	summary.ended,
	case
		when summary.ended is null then
			cast(null as numeric(9))
		when dbo.secs(summary.started) <= dbo.secs(summary.ended) then
			-- Same side of midnight
			(dbo.secs(summary.ended) - dbo.secs(summary.started))
		else
			-- Straddles midnight; 86400 = secs per day
			86400 + (dbo.secs(summary.ended) - dbo.secs(summary.started))
	end secs,
	case
		when summary.ended is null then
			null
		else
			dbo.to_hrs_mins(
				case
					when dbo.secs(summary.started) <= dbo.secs(summary.ended) then
						-- Same side of midnight
						(dbo.secs(summary.ended) - dbo.secs(summary.started))
					else
						-- Straddles midnight; 86400 = secs per day
						86400 + (dbo.secs(summary.ended) - dbo.secs(summary.started))
				end
			)
	end hrs_mins
from (
	-- Inline view of run summaries
	select
		to_char(rlh.time_stamp, 'DY') day_of_week,
		rlh.time_stamp started,
		(
			-- First end after start
			select
				min(rlh2.time_stamp)
			from
				report_log_history rlh2
			where
				--CRITERIA: Populate script
				rlh2.routine = 'Populate_Package'
				and
				--CRITERIA: First step
				rlh2.line like 'Time to complete = %'
				and
				--CRITERIA: After start
				rlh2.time_stamp > rlh.time_stamp
				and
				--CRITERIA: No intervening starts
				not exists (
					select 'x'
					from
						report_log_history rlh3
					where
						--CRITERIA: Populate script
						rlh3.routine = 'Populate_Package'
						and
						--CRITERIA: First step
						rlh3.order_by = 1
						and
						--CRITERIA: Between start and end
						rlh3.time_stamp > rlh.time_stamp
						and
						rlh3.time_stamp < rlh2.time_stamp
				)
		) ended
	from
		report_log_history rlh
	where
		--CRITERIA: Populate script
		rlh.routine = 'Populate_Package'
		and
		--CRITERIA: First step
		rlh.order_by = 1
) summary
order by
	2 desc