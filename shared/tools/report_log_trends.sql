select
	rls.script "Script",
	rls.step "Step",
	case
		when dbo.secs(rls3.end_date) >= dbo.secs(rls3.start_date) then
			round((dbo.secs(rls3.end_date) - dbo.secs(rls3.start_date)) / 60, 0)
		else
			round(((dbo.secs(dbo.midnight(rls3.start_date)) - dbo.secs(rls3.start_date)) + dbo.secs(rls3.end_date)) / 60, 0)
	end "Oldest Duration Mins",
	case
		when dbo.secs(rls2.end_date) >= dbo.secs(rls2.start_date) then
			round((dbo.secs(rls2.end_date) - dbo.secs(rls2.start_date)) / 60, 0)
		else
			round(((dbo.secs(dbo.midnight(rls2.start_date)) - dbo.secs(rls2.start_date)) + dbo.secs(rls2.end_date)) / 60, 0)
	end "Old Duration Mins",
	case
		when dbo.secs(rls.end_date) >= dbo.secs(rls.start_date) then
			round((dbo.secs(rls.end_date) - dbo.secs(rls.start_date)) / 60, 0)
		else
			round(((dbo.secs(dbo.midnight(rls.start_date)) - dbo.secs(rls.start_date)) + dbo.secs(rls.end_date)) / 60, 0)
	end "Current Duration Mins"

from
	report_log_summaries rls
--JOIN: Previous run
inner join report_log_summaries rls2
on rls2.script = rls.script
and rls2.step = rls.step
and rls2.start_date < rls.start_date
and rls2.start_date = (
	select
		max(rls2a.start_date)
	from
		report_log_summaries rls2a
	where
		rls2a.script = rls.script
		and
		rls2a.step = rls.step
		and
		rls2a.start_date < rls.start_date
)
--JOIN: Run before previous
inner join report_log_summaries rls3
on rls3.script = rls2.script
and rls3.step = rls2.step
and rls3.start_date < rls2.start_date
and rls3.start_date = (
	select
		max(rls3a.start_date)
	from
		report_log_summaries rls3a
	where
		rls3a.script = rls2.script
		and
		rls3a.step = rls2.step
		and
		rls3a.start_date < rls2.start_date
)
--JOIN: Run before that one !
inner join report_log_summaries rls4
on rls4.script = rls3.script
and rls4.step = rls3.step
and rls4.start_date < rls3.start_date
and rls4.start_date = (
	select
		max(rls4a.start_date)
	from
		report_log_summaries rls4a
	where
		rls4a.script = rls3.script
		and
		rls4a.step = rls3.step
		and
		rls4a.start_date < rls3.start_date
)
where
	rls.script = 'Populate_Package'
	and
	--CRITERIA: Latest run for step
	rls.start_date = (
		select
			max(rls1a.start_date)
		from
			report_log_summaries rls1a
		where
			rls1a.script = rls.script
			and
			rls1a.step = rls.step
	)
	and
	--CRITERIA: In latest Report Log
	-- (excludes obsolete steps)
	exists (
		select 'x'
		from
			report_logs rlo
		where
			rlo.routine = rls.script
			and
			rlo.step = rls.step
	)
order by
	rls.step