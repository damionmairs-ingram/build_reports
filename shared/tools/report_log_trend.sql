select
	rls.*,
	case
		when dbo.secs(rls.end_date) >= dbo.secs(rls.start_date) then
			round((dbo.secs(rls.end_date) - dbo.secs(rls.start_date)) / 60, 0)
		else
			round(((dbo.secs(dbo.midnight(rls.start_date)) - dbo.secs(rls.start_date)) + dbo.secs(rls.end_date)) / 60, 0)
	end "Duration Mins",
	case
		when dbo.secs(rls.end_date) >= dbo.secs(rls.start_date) then
			dbo.secs(rls.end_date) - dbo.secs(rls.start_date)
		else
			(dbo.secs(dbo.midnight(rls.start_date)) - dbo.secs(rls.start_date)) + dbo.secs(rls.end_date)
	end "Duration Secs",
	(
		select
			rh2.version
		from
			reports_history rh2
		where
			rh2.install_started = (
				select
					max(rh.install_started)
				from
					reports_history rh
				where
					--CRITERIA: Latest completed Repository Upload
					rh.install_started < rls.start_date
					and
					rh.install_finished is not null
			)
	) repository_version
from
	report_log_summaries rls
where
	rls.step = 'Dm_Rap_Infrastructure'
order by
	rls.start_date desc