set define off
set echo off
set feedback off
set heading off
set linesize 4000
set pagesize 0
set termout off
set timing off
set trimspool on
set wrap on
spool report_filters.txt
select
/*
 * NAME:		export_filters.sql
 * DESCRIPTION:	Exports Report_Filters' definitions
 * HISTORY:     1.0 djm 22/04/2010 Initial version, based on IMU's get_report_and_ics_filters.sql
 */
	-- Format the data from the Inline view
	--
	-- report_filter_mappings are not exported, but are in imu_report_filters
	filters.rfm_id||'^'||
	filters.rfm_mapped_value||'^'||
	filters.rfm_report_filter_id||'^'||
	-- report_filters
	filters.rf_context||'^'||
	filters.rf_description||'^'||
	filters.rf_display_column||'^'||
	cast(filters.rf_form_page_number as varchar(9))||'^'||
	cast(filters.rf_form_type_id as varchar(9))||'^'||
	cast(filters.rf_id as varchar(10))||'^'||
	cast(filters.rf_item_id as varchar(9))||'^'||
	filters.rf_name||'^'||
	cast(filters.rf_question_id as varchar(9))||'^'||
	cast(filters.rf_questionnaire_id as varchar(9))||'^'||
	filters.rf_selection_column||'^'||
	filters.rf_selection_value||'^'||
	filters.rf_source_column||'^'||
	filters.rf_source_table||'^'
from (
	select
		-- Inline view based on IMU's get_report_and_ics_filters.sql
		--
		-- report_filter_mappings are not exported, but are in imu_report_filters
		null				rfm_id,
		null				rfm_mapped_value,
		null				rfm_report_filter_id,
		-- report_filters
		rf.context			rf_context,
		f_convert_to_symbols(rf.description)
							rf_description,
		rf.display_column	rf_display_column,
		case
			when rf.source_table != 'ITEMS' then
				rf.form_page_number
		end					rf_form_page_number,
		case
			when rf.source_table != 'ITEMS' then
				rf.form_type_id
		end					rf_form_type_id,
		rf.id				rf_id,
		rf.item_id			rf_item_id,
		rf.name				rf_name,
		case
			when rf.source_table != 'QUESTIONS' then
				rf.question_id
		end					rf_question_id,
		case
			when rf.source_table != 'QUESTIONS' then
				rf.questionnaire_id
		end					rf_questionnaire_id,
		case
			when rf.source_table not in ('FORM_TYPES', 'FORM_TYPES_ACTIVE') then
				rf.selection_column			
		end					rf_selection_column,
		case
			when rf.source_table not in ('FORM_TYPES', 'FORM_TYPES_ACTIVE') then
				rf.selection_value				
		end 				rf_selection_value,
		rf.source_column	rf_source_column,
		rf.source_table		rf_source_table
	from
		report_filters rf
	-- Consistent ordering is useful for checking changes !
	-- Removed for comparison with IMU file
--	order by
--		rf.name
) filters
/
spool off
EXIT

