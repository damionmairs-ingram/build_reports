/*
 * NAME:		swap_tags
 * DESCRIPTION: Used to re-link broken/inconsistent Tags
 *              For reporting purposes, old Tag is effectively renamed to new Tag
 *              An old Tag can only be renamed to one new Tag (for obvious reasons !)
 *
 *              Use report_form_question_versions to check details first !
 * 
 *              Occassionally, two Tags will need to be swapped (ie. answers transposed is some Form versions)
 *              This requires two lines in Swap_Tag so run the script twice, swapping the Tags between runs
 * 
 *              This script may be run during Office hours
 *              But changes will not be seen until report_form_questions and report_form_answers have run
 * HISTORY:     1.0 djm 06/03/2009 (Internal) Initial version
 */
insert into swap_tag_id (
	section_id,
	old_tag_id,
	new_tag_id
)
select
	details.section_id,
	details.old_tag_id,
	details.new_tag_id
from (
	-- Inline view so Constants only in one place
	select
		rfqv.section_id,
		rfqv.section_item_tag_id old_tag_id,
		'xxx' new_tag_id -- New (correct) Tag ID
	from
		report_form_question_versions rfqv
	where
		--CRITERIA: Form Type ID_Code
		rfqv.form_type_id_code = 'xxx'
		and
		--CRITERIA: Versions of Form Type with incorrect Tag_ID
		-- NB. Optional: reinstate if only specific versions need to be fixed
		--rfqv.version in (999, 999, 999)
		--and
		--CRITERIA: Old (incorrect) Tag ID
		rfqv.section_item_tag_id = 'xxx'
) details
where
	--CRITERIA: Ensure it's not already Swapped
	not exists (
		select 'x'
		from
			swap_tag_id swp
		where
			swp.section_id = details.section_id
			and
			swp.old_tag_id = details.old_tag_id
	)
