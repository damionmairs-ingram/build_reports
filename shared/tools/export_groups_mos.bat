@echo off

rem export_groups.bat
rem Exports report_groups.txt
rem DJM 22/04/2010 Initial version

REM This is a constant
SET TOOLS_DIR=C:\projects\build_reports\reports\shared\tools

REM Parse arguments
SET release_series=%1

REM Prompt for any missing params
IF "%release_series%"=="" (
	set automated=NO
	echo Please enter the Repository Release Series
	set /p release_series=eg. 3.3 ... 
) else (
	set automated=YES
)

IF "%release_series%"=="" (
	echo Error: No Release Series supplied
	GOTO :END
)

SET EXPORT_DIR=C:\projects\reports
echo Export Directory = %EXPORT_DIR%

REM Check specified source directory exists
IF NOT EXIST "%EXPORT_DIR%" (
	echo Error: Aborted copy as Export Directory missing
	echo Please check release series and rerun
	GOTO :END
)

rem Change directory to export Data files
rem Need to change directory, as SQL won't know which RR version to export to
cd %EXPORT_DIR%\oracle\framework\Data

echo Export Latest Filters from mosrel to Oracle source directory
SQLPLUS fw/fw@reports.mos.bpc2 @%TOOLS_DIR%\export_groups.sql

echo Copy Latest Filters to SQLServer Source directory
XCOPY /Q /R /Y report_groups.txt "%EXPORT_DIR%\sql_server\framework\Data"

echo Seems to have exported OK.
echo Please check export files.

:END

IF "%automated%"=="NO" (
	REM Ensure all messages can be read
	pause
)
GOTO :EOF

:EOF
