select
/*
 * NAME:		Upload Script Log Summary
 * DESCRIPTION:	Displays Summary of preserved RR Upload logs
 *      		*** Do *NOT* convert to DBO ***
 *      		DBO may be unavailable if Upload failed !
 * HISTORY:		1.0 djm 15/02/2011	FRA-7569: Initial version
 */
	ups.version "RR Version",
	ups.script "Script name",
	ups.end_datetime "Script Finished",
	(
		select
			count(1)
		from
			report_upload_logs upl
		where
			upl.upload_script_id = ups.upload_script_id
	) "No. Log Lines",
	(
		select
			count(1)
		from
			report_upload_logs upl
		where
			upl.upload_script_id = ups.upload_script_id
			and
			--CRITERIA: Exclude known lines that look like Errors/Warnings
			upl.line not like 'Errors allowed: %'
			and
			upl.line not like '%***%Please ignore%'
			and
			upl.line not like 'grant execute %'
			and
			upl.line not in (
				'Dropping procedure dbo.log_error',
				'Warning: Null value is eliminated by an aggregate or other SET operation.',
				'Please ignore warning messages about Null values being eliminated',
				'Please ignore compilation errors from the function list_ppl_ppl_attributes() unless using Frameworki 3, or greater',
				'*** Please "Null value is eliminated" warnings ***'
			)
			and
			(
				--CRITERIA: RR Error/Warning formats
				upl.line like '%Error%'
				or
				upl.line like '%Warning%'
				or
				--CRITERIA: Oracle Error formats
				upl.line like '%ORA-%'
				or
				upl.line like '%PLS-%'
				or
				upl.line like 'SQL*Loader-%'
				or
				--CRITERIA: SS message format
				upl.line like '%Msg%Level%State%'
			)
	) "No. Likely Issues"
from
	report_upload_scripts ups
where
	--CRITERIA: On/after specified p_create_datamart_objects
	ups.upload_script_id >= '&P1&'
--	and
	--CRITERIA: Optionally restrict to specified script
--	'&P2&' in (' All Scripts', ups.script)
	and
	--CRITERIA: Stop at next p_create_datamart_object
	ups.upload_script_id < (
		select
			coalesce(min(ups2.upload_script_id), 999999999)
		from
			report_upload_scripts ups2
		where
			--CRITERIA: First step in next Upload
			ups2.upload_script_id > '&P1&'
			and
			ups2.script in ('P_CREATE_DATAMART_OBJECTS', 'P_CREATE_DATAMART_OBJECTS_SS')
	)
order by
	ups.upload_script_id