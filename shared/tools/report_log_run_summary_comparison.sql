select
	raw_data.step,
	raw_data.run_1_mins,
	raw_data.run_2_mins,
	case
		when coalesce(raw_data.run_1_mins, 0) >= coalesce(raw_data.run_2_mins, 0) then
			coalesce(raw_data.run_1_mins - raw_data.run_2_mins, 0)
	end good_mins,
	case
		when coalesce(raw_data.run_1_mins, 0) < coalesce(raw_data.run_2_mins, 0) then
			coalesce(raw_data.run_1_mins - raw_data.run_2_mins, 0)
	end bad_mins,
--	abs(coalesce(raw_data.run_1_mins - raw_data.run_2_mins, 0)) abs_diff_mins,
--	coalesce(raw_data.run_1_mins - raw_data.run_2_mins, 0) diff_mins,
	raw_data.run_1_start,
	raw_data.run_1_end,
	raw_data.run_2_start,
	raw_data.run_2_end,
	raw_data.run_1_version,
	raw_data.run_2_version
from (
	-- Inline view of raw data
	select
		rls1.step,
		rls1.start_date run_1_start,
		rls1.end_date run_1_end,
		case
			when rls1.start_date is null then
				-- dbo.midnight() does not handle nulls - log Jira
				cast(null as numeric(9))
			when dbo.secs(rls1.end_date) >= dbo.secs(rls1.start_date) then
				round((dbo.secs(rls1.end_date) - dbo.secs(rls1.start_date)) / 60, 0)
			else
				round(((dbo.secs(dbo.midnight(rls1.start_date)) - dbo.secs(rls1.start_date)) + dbo.secs(rls1.end_date)) / 60, 0)
		end run_1_mins,
		rls2.start_date run_2_start,
		rls2.end_date run_2_end,
		case
			when rls2.start_date is null then
				-- dbo.midnight() does not handle nulls - log Jira
				cast(null as numeric(9))
			when dbo.secs(rls2.end_date) >= dbo.secs(rls2.start_date) then
				round((dbo.secs(rls2.end_date) - dbo.secs(rls2.start_date)) / 60, 0)
			else
				round(((dbo.secs(dbo.midnight(rls2.start_date)) - dbo.secs(rls2.start_date)) + dbo.secs(rls2.end_date)) / 60, 0)
		end run_2_mins,
		(
			select
				rh2.version
			from
				reports_history rh2
			where
				rh2.install_started = (
					select
						max(rh.install_started)
					from
						reports_history rh
					where
						--CRITERIA: Latest completed Repository Upload
						rh.install_started < rls1.start_date
						and
						rh.install_finished is not null
				)
		) run_1_version,
		(
			select
				rh2.version
			from
				reports_history rh2
			where
				rh2.install_started = (
					select
						max(rh.install_started)
					from
						reports_history rh
					where
						--CRITERIA: Latest completed Repository Upload
						rh.install_started < rls2.start_date
						and
						rh.install_finished is not null
				)
		) run_2_version
	from
		report_log_summaries rls1
	--JOIN: Run 2's Steps
	left outer join report_log_summaries rls2
	on rls2.script = rls1.script
	and rls2.step = rls1.step
	and rls2.start_date between to_date('13-MAY-2010 03:10:00', 'DD-MON-YYYY HH24:MI:SS')
		                and     to_date('13-MAY-2010 10:06:00', 'DD-MON-YYYY HH24:MI:SS')
	where
		--CRITERIA: Run 1's Steps
		rls1.start_date between to_date('12-MAY-2010 03:10:00', 'DD-MON-YYYY HH24:MI:SS')
		                and     to_date('12-MAY-2010 08:04:00', 'DD-MON-YYYY HH24:MI:SS')
) raw_data
order by
	-- Absolute Difference
	abs(coalesce(raw_data.run_1_mins - raw_data.run_2_mins, 0)) desc
	
