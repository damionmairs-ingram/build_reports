set define off
set echo off
set feedback off
set heading off
set linesize 4000
set pagesize 0
set termout off
set timing off
set trimspool on
set wrap on
spool report_groups.txt
select
/*
 * NAME:		export_groups.sql
 * DESCRIPTION:	Exports Report_Groups' definitions
 * HISTORY:     1.0 djm 22/04/2010 Initial version, based on IMU's get_report_and_ics_groups.sql
 */
	-- Format the data from the Inline view
	--
	-- report_group_categories
	cast(-- Suppress category_order zero, to match original IMU file
		case
			when groups.rgc_category_order != 0 then
				groups.rgc_category_order
		end as varchar(10))||'^'||
	groups.rgc_category_value||'^'||
	cast(groups.rgc_id as varchar(10))||'^'||
	cast(groups.rgc_report_group_id as varchar(10))||'^'||
	-- report_group_mappings are not exported, but are in imu_report_groups
	groups.rgm_id||'^'||
	groups.rgm_mapped_value||'^'||
	groups.rgm_report_group_category_id||'^'||
	-- report_groups
	groups.rg_context||'^'||
	groups.rg_description||'^'||
	groups.rg_display_column||'^'||
	cast(groups.rg_form_page_number as varchar(10))||'^'||
	cast(groups.rg_form_type_id as varchar(10))||'^'||
	cast(groups.rg_id as varchar(10))||'^'||
	cast(groups.rg_item_id as varchar(10))||'^'||
	groups.rg_name||'^'||
	cast(groups.rg_question_id as varchar(10))||'^'||
	cast(groups.rg_questionnaire_id as varchar(10))||'^'||
	groups.rg_selection_column||'^'||
	groups.rg_selection_value||'^'||
	groups.rg_source_column||'^'||
	groups.rg_source_table||'^'
from (
	select
		-- report_group_categories
		rgc.category_order		rgc_category_order,
		rgc.category_value		rgc_category_value,
		rgc.id					rgc_id,
		rgc.report_group_id		rgc_report_group_id,
		-- report_group_mappings are not exported, but are in imu_report_groups
		null					rgm_id,
		null					rgm_mapped_value,
		null 					rgm_report_group_category_id,
		-- report_groups
		rg.context				rg_context,
		f_convert_to_symbols(rg.description)
								rg_description,
		rg.display_column		rg_display_column,
		case
			when rg.source_table != 'ITEMS' then
				rg.form_page_number
		end						rg_form_page_number,
		case
			when rg.source_table != 'ITEMS' then
				rg.form_type_id
		end						rg_form_type_id,
		rg.id					rg_id,
		rg.item_id				rg_item_id,
		rg.name					rg_name,
		case
			when rg.source_table != 'QUESTIONS' then
				rg.question_id
		end						rg_question_id,
		case
			when rg.source_table != 'QUESTIONS' then
				rg.questionnaire_id
		end						rg_questionnaire_id,
		case
			when rg.source_table not in ('form_types', 'form_types_active') then
				rg.selection_column			
		end						rg_selection_column,
		case
			when rg.source_table not in ('FORM_TYPES', 'FORM_TYPES_ACTIVE') then
				rg.selection_value				
		end 					rg_selection_value,
		rg.source_column		rg_source_column,
		rg.source_table			rg_source_table
	from 
		report_groups rg
	--JOIN: Categories for the Group
	inner join report_group_categories rgc
	on rgc.report_group_id = rg.id
	-- Consistent ordering is useful for checking changes !
	-- Removed for comparison with IMU file
--	order by
--		rg.name, rgc.category_value
) groups
/
spool off
EXIT

