select
	rls.*,
	case
		when dbo.secs(rls.end_date) >= dbo.secs(rls.start_date) then
			round((dbo.secs(rls.end_date) - dbo.secs(rls.start_date)) / 60, 0)
		else
			round(((dbo.secs(dbo.midnight(rls.start_date)) - dbo.secs(rls.start_date)) + dbo.secs(rls.end_date)) / 60, 0)
	end "Duration Mins"
from
	report_log_summaries rls
where
	rls.script = 'Populate_Package'
	and
	rls.start_date >= (
		select
			min(rlo.time_stamp)
		from
			report_logs rlo
		where
			rlo.routine = 'Populate_Package'
	)
order by
	5  desc--rls.start_date