-- Step Log trend
-- Returns log entries for a specific step from multiple runs
with run_logs as (
	select
		(
			-- First log entry for that run
			select
				-- Latest matching
				max(rlh2.time_stamp)
			from
				report_log_history rlh2
			where
				--CRITERIA: First log entries for same script type
				rlh2.routine = rlh.routine
				and
				rlh2.order_by = 1
				and
				--CRITERIA: Starting before current entry
				rlh2.time_stamp <= rlh.time_stamp
		) run_started,
		rlh.time_stamp,
		rlh.routine,
		rlh.order_by,
		rlh.step,
		rlh.line
	from
		report_log_history rlh
	where
		rlh.routine = 'Populate_Package'
		and
		rlh.step = 'Populate_Adults.Pop_Structure'
),
run_log_derivations as (
	select
		line.run_started,
		line.routine,
		line.order_by,
		line.step,
		line.line,
		line.time_stamp,
		next_line.time_stamp next_time_stamp,
		-- Derive duration in Secs
		case
			when line.run_started != dbo.past_time(next_line.run_started) then
				cast(null as numeric(9))
			when coalesce(line.step, '') = '' then
				cast(null as numeric(9))
			when dbo.secs(line.time_stamp) <= dbo.secs(coalesce(next_line.time_stamp, dbo.now())) then
				-- Same side of midnight
				(dbo.secs(coalesce(next_line.time_stamp, dbo.now())) - dbo.secs(line.time_stamp))
			else
				-- Straddles midnight; 86400 = secs per day
				86400 + (dbo.secs(coalesce(next_line.time_stamp, dbo.now())) - dbo.secs(line.time_stamp))
		end secs,
		-- Derive status
		case
			when line.line  like 'Error:%'
			or   next_line.line like 'Error:%' then
				'Error'
			when line.line  like 'Warning:%'
			or   next_line.line like 'Warning:%'then
				'Warning'
			when next_line.time_stamp is null
			and  coalesce(line.step, '') != '' then
				'Time so far (if still running)'
		end status
	from
		run_logs line
	--JOIN: Next log_line, for timing info
	left outer join run_logs next_line
	on next_line.order_by = line.order_by + 1
	and next_line.run_started = line.run_started
)
select
	rld.run_started "Run Started",
	rld.time_stamp	"TimeStamp",
	rld.secs		"Secs",
	rld.status		"Status",
	rld.step		"Step",
	rld.line		"Line"
from
	run_log_derivations rld
-- Optional filters
--where
	--rld."Status" is not null -- Rows without a null status
	--rld."Status" = 'Error' -- Errors only
order by
	rld.run_started,
	-- Perfomance testing:
--	rld.secs desc
	-- Chronological, oldest first:
	rld.order_by
	-- Reverse Chronological, most recent first:
	-- (Can use PL/SQL Developer's Auto Refresh)
	--rld.order_by desc 

