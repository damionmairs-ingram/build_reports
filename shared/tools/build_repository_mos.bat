@echo off

rem build_repository.bat
rem Build Repository directories for release
rem DJM 15/02/2010 Initial version
rem DJM 15/02/2010 Added check for Repository version
rem DJM 22/04/2010 Extract report_filters.txt and report_groups.txt
rem DJM 17/11/2010 Added shared_bo_universes (Requires reports-3.4 or later; previous releases must be built using old versions of BAT)
rem DJM 24/11/2010 Amended to use export_reports.BAT instead of IMU

REM This is a constant
SET TOOLS_DIR=C:\projects\build_reports\reports\shared\tools

REM Parse arguments
 SET release_version=%1

REM Prompt for any missing params
 IF "%release_version%"=="" (
 	echo Please enter the full Repository Release version
 	set /p release_version=eg. 3.3.1.0 ... 
 )

 IF "%release_version%"=="" (
	echo Error: No Release Version supplied
	GOTO :END
 )

REM Extract Repository Series from Release Version
REM NB. Assume Series is first three characters of version string
CALL SET release_series=%%release_version:~0,4%%
echo Repository Series = %release_series%

SET SOURCE_DIR=C:\projects\reports
echo Source = %SOURCE_DIR%

REM Check specified source directory exists
IF NOT EXIST "%SOURCE_DIR%" (
	echo Error: Aborted copy as Source Directory missing
	echo Please check that the source directory exists 
	GOTO :END
)

REM Check Release Version matches p_create_datamart_objects' version
find /i "repository_version varchar(8) := '%release_version%'" "%SOURCE_DIR%\oracle\framework\Programs\p_create_datamart_objects.sql" >nul
IF errorlevel 1 (
	echo Error: Release version does not match %SOURCE_DIR%\oracle\framework\Programs\p_create_datamart_objects.sql
	echo Please rerun when corrected
	GOTO :END
)
find /i "set @repository_version = '%release_version%'" "%SOURCE_DIR%\sql_server\framework\Programs\p_create_datamart_objects_ss.sql" >nul
IF errorlevel 1 (
	echo Error: Release version does not match %SOURCE_DIR%\sql_server\framework\Programs\p_create_datamart_objects_ss.sql
	echo Please rerun when corrected
	GOTO :END
)

REM Check Release Version matches reports.properties
find /i "version=%release_version%" "%SOURCE_DIR%\oracle\framework\Programs\reports.properties" >nul
IF errorlevel 1 (
	echo Error: Release version does not match %SOURCE_DIR%\oracle\framework\Programs\reports.properties
	echo Please rerun when corrected
	GOTO :END
)
find /i "version=%release_version%" "%SOURCE_DIR%\sql_server\framework\Programs\reports.properties" >nul
IF errorlevel 1 (
	echo Error: Release version does not match %SOURCE_DIR%\sql_server\framework\Programs\reports.properties
	echo Please rerun when corrected
	GOTO :END
)

SET DEST_DIR=S:\SQL Team\Mosaic_Reports_Repository
echo Destination = %DEST_DIR%

set /p confirmation=Please confirm these details are correct (Y/N) 
IF /i "%confirmation%"=="Y" (
	echo Parameters correct; continuing
) ELSE (
	echo Error: Parameters not correct
	echo Please rerun with correct details
	GOTO :END
)

IF EXIST "%DEST_DIR%" (
	echo Remove old destination directory
	RMDIR /S /Q "%DEST_DIR%"
)

IF EXIST "%DEST_DIR%" (
	echo Error: Old destination directory not removed
	echo Please ensure it is not in use by anyone, or delete manually and rerun
	GOTO :END
)

echo Call Batch file to Extract Filters
call %TOOLS_DIR%\export_filters_mos.bat %release_series%

echo Call Batch file to Extract Groups
call %TOOLS_DIR%\export_groups_mos.bat %release_series%

echo Call Batch file to Extract Reports
call %TOOLS_DIR%\export_reports_mos.bat %release_series%

echo Recreate destination directory
MKDIR "%DEST_DIR%\oracle\framework"
MKDIR "%DEST_DIR%\sql_server\framework"

echo Copy Oracle-specific files
REM Does not copy hidden files (eg. ".svn" directories)
XCOPY /I /E /Q /R /Y "%SOURCE_DIR%\oracle\framework" "%DEST_DIR%\oracle\framework"

echo Copy SQLServer-specific files
REM Does not copy hidden files (eg. ".svn" directories)
XCOPY /I /E /Q /R /Y "%SOURCE_DIR%\sql_server\framework" "%DEST_DIR%\sql_server\framework"

echo Copy shared documentation files
XCOPY /Q /R /Y "%SOURCE_DIR%\shared_documentation" "%DEST_DIR%\oracle\framework\Documentation"
XCOPY /Q /R /Y "%SOURCE_DIR%\shared_documentation" "%DEST_DIR%\sql_server\framework\Documentation"

echo Copy shared BO Universes
XCOPY /Q /R /Y "%SOURCE_DIR%\shared_bo_universes" "%DEST_DIR%\oracle\framework\Business Objects"
XCOPY /Q /R /Y "%SOURCE_DIR%\shared_bo_universes" "%DEST_DIR%\sql_server\framework\Business Objects"

echo Empty Log directories
ERASE /q "%DEST_DIR%\oracle\framework\Logs\*"
ERASE /q "%DEST_DIR%\sql_server\framework\Logs\*"

echo Remove backup files
rem NB. Tilde is a real pain, as it's ignored. FOR loop won't allow piped commands.
rem NB. Therefore need convoluted work-around.
DIR /B /S "%DEST_DIR%" | find "~" >"%DEST_DIR%\tilde_backups.txt"
FOR /F "delims=" %%f IN ('type "%DEST_DIR%\tilde_backups.txt"') DO ERASE /F %%f
ERASE "%DEST_DIR%\tilde_backups.txt"

echo Seems to have copied OK.
echo Please check destination directories.

:END
REM Ensure all messages can be read
pause
GOTO :EOF

:EOF