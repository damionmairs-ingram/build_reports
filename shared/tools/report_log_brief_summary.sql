select
/*
 * NAME:		report_log_brief_summary.sql
 * DESCRIPTION:	One-line summary of Populate script run
 *         		Will run on both Oracle and SQLServer databases that have Reports Repository installed
 * HISTORY:		1.0 djm 27/04/2009	HLP-1464: Initial Version
 *         		1.1 djm 28/04/2010	Amended to include a numeric severity
 *         		1.2 djm 28/04/2010	Added useful info from log
 */
	case
		when issues.unknown_not_run_since_upload is not null
		or   issues.unknown_no_log_found is not null
		or   issues.unknown_not_run_in_over_1_day is not null then
			3
		when issues.critical_run_incomplete is not null
		or   issues.critical_error_encountered is not null then
			2
		when issues.warning_warning_encountered is not null then
			1
		else
			0
	end compare_value,
	case
		when issues.unknown_not_run_since_upload is not null
		or   issues.unknown_no_log_found is not null
		or   issues.unknown_not_run_in_over_1_day is not null
		or   issues.critical_run_incomplete is not null
		or   issues.critical_error_encountered is not null
		or   issues.warning_warning_encountered is not null then
			-- Concatentate all messages
			dbo.append5(issues.unknown_not_run_since_upload,
						issues.unknown_no_log_found,
						issues.unknown_not_run_in_over_1_day,
						issues.critical_run_incomplete,
						issues.critical_error_encountered,
						issues.warning_warning_encountered)
		else
			'OK'
	end message,
	(
		select
			cast(replace(replace(rlo.line, 'Time to complete = ', ''), ' seconds', '') as numeric(9))
		from
			report_logs rlo
		where
			rlo.routine = 'Populate_Package'
			and
			coalesce(rlo.line, 'x') like 'Time to complete = % seconds'
	) elapsed_secs,
	(
		select
			count(1)
		from
			report_logs rlo
		where
			rlo.routine = 'Populate_Package'
			and
			rlo.line like 'Error:%'
	) num_errors,
	(
		select
			count(1)
		from
			report_logs rlo
		where
			rlo.routine = 'Populate_Package'
			and
			rlo.line like 'Warning:%'
	) num_warnings,
	dbo.repository_version() repository_version
from (
	-- Inline view identifying all issues
	select
		--
		-- Service State = 'Unknown'
		--
		-- Not run since Upload
		case
			when exists (
				select 'x'
				from
					report_logs rlo
				where
					rlo.routine = 'Populate_Package'
					and
					--CRITERIA: First log entry
					rlo.order_by = (
						select
							min(latest_rlo.order_by)
						from
							report_logs latest_rlo
						where
							latest_rlo.routine = 'Populate_Package'
					)
					and
					coalesce(rlo.line, 'x') != 'Starting Populate_Package'
			) then
				'Not run since Upload: needs scheduling ?; '
		end unknown_not_run_since_upload,
		-- No Log found: needs scheduling ?
		case
			when not exists (
				select 'x'
				from
					report_logs rlo
				where
					rlo.routine = 'Populate_Package'
			) then
				'No Log found: needs scheduling ?; '
		end unknown_no_log_found,
		-- Has not run in over 1 day
		case
			when exists (
				select max(rlo.time_stamp)
				from
					report_logs rlo
				where
					rlo.routine = 'Populate_Package'
				having max(rlo.time_stamp) < (dbo.now() - 1)
			) then
				'Has not run in over 1 day: needs scheduling ?; '
		end unknown_not_run_in_over_1_day,
		--
		-- Service State = 'CRITICAL'
		--
		-- Run Incomplete
		-- NB. May have been killed externally or by untrappable error !
		case
			when exists (
				select 'x'
				from
					report_logs rlo
				where
					rlo.routine = 'Populate_Package'
					and
					--CRITERIA: Latest log entry
					rlo.order_by = (
						select
							max(latest_rlo.order_by)
						from
							report_logs latest_rlo
						where
							latest_rlo.routine = 'Populate_Package'
					)
					and
					coalesce(rlo.line, 'x') not like 'Time to complete = % seconds'
			) then
				'Run Incomplete (may still be running); '
		end critical_run_incomplete,
		-- Error encountered
		(
			select dbo.append(cast(count(1) as varchar(9)), ' Error(s) Encountered; ')
			from
				report_logs rlo
			where
				rlo.routine = 'Populate_Package'
				and
				rlo.line like 'Error:%'
			having count(1) > 0
		) critical_error_encountered,
		--
		-- Service State = 'WARNING'
		--
		(
			select dbo.append(cast(count(1) as varchar(9)), ' Warning(s) Encountered; ')
			from
				report_logs rlo
			where
				rlo.routine = 'Populate_Package'
				and
				rlo.line like 'Warning:%'
			having count(1) > 0
		) warning_warning_encountered
	from
		reports_dual
) issues
