@echo off

rem export_reports.bat
rem Exports repository_extract.txt and ss_repository_extract.txt
rem DJM 24/11/2010 Initial version
rem DJM 02/12/2010 Corrected comment

REM This is a constant
SET TOOLS_DIR=C:\projects\build_reports\reports\shared\tools

REM Parse arguments
SET release_series=%1

REM Prompt for any missing params
IF "%release_series%"=="" (
	set automated=NO
	echo Please enter the Repository Release Series
	set /p release_series=eg. 3.3 ... 
) else (
	set automated=YES
)

IF "%release_series%"=="" (
	echo Error: No Release Series supplied
	GOTO :END
)

SET EXPORT_DIR=C:\projects\reports


echo Export Directory = %EXPORT_DIR%

REM Check specified source directory exists
IF NOT EXIST "%EXPORT_DIR%" (
	echo Error: Aborted copy as Export Directory missing
	echo Please check release series and rerun
	GOTO :END
)

rem Change directory to export Data files
rem Need to change directory, as SQL won't know which RR version to export to
cd %EXPORT_DIR%\oracle\framework\Data

REM Connection constants
set HOST=reportsvm-2
set DATABASE=reportsrepository_maint
set USERNAME=fw
set PASSWORD=fw

echo Export latest Oracle Reports
%TOOLS_DIR%\export_reports.exe %HOST% %DATABASE% %USERNAME% %PASSWORD% export_oracle_reports_mosaic %EXPORT_DIR%\oracle\framework\Data\repository_extract.txt

echo Export latest SQLServer Reports
%TOOLS_DIR%\export_reports.exe %HOST% %DATABASE% %USERNAME% %PASSWORD% export_sqlserver_reports_mosaic %EXPORT_DIR%\sql_server\framework\Data\ss_repository_extract.txt

echo Seems to have exported OK.
echo Please check export files.

:END

IF "%automated%"=="NO" (
	REM Ensure all messages can be read
	pause
)
GOTO :EOF

:EOF