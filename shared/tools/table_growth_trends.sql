-- Table Growth stats
select
	details.table_name,
	details.rundate,
	details.prev_rundate,
	details.num_rows,
	round((details.num_rows - details.prev_num_rows) / (details.rundate - details.prev_rundate)) growth_per_day,
	-- Growth for "Average" month
	round((details.num_rows - details.prev_num_rows) / ((details.rundate - details.prev_rundate) / 30.5)) growth_per_month
from (
	select
		rtt.table_name,
		rtt.rundate,
		(
			select
				max(rtt2.rundate)
			from
				report_table_trends rtt2
			where
				rtt2.table_name = rtt.table_name
				and
				rtt2.rundate < rtt.rundate
		) prev_rundate,
		rtt.num_rows,
		(
			select
				rtt3.num_rows
			from
				report_table_trends rtt3
			where
				--CRITERIA: Same table name
				rtt3.table_name = rtt.table_name
				and
				--CRITERIA: Previous Run Date
				rtt3.rundate = (
					select
						max(rtt2.rundate)
					from
						report_table_trends rtt2
					where
						rtt2.table_name = rtt.table_name
						and
						rtt2.rundate < rtt.rundate
				)
		) prev_num_rows
	from
		report_table_trends rtt
	where
		rtt.table_name = 'FORM_ANSWERS'
) details
order by
	details.rundate desc