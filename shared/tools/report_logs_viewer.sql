select
/*
 * NAME:		report_logs_viewer.sql
 * DESCRIPTION:	Displays logs from p_populate_report_tables
 * HISTORY:		1.0 djm 23/03/2006	Inititial Version
 *         		1.1 djm 15/05/2006	Include Populate_Package
 *         		1.2 djm 12/06/2006	SQLServer-friendly
 *         		1.3 djm 13/12/2006	Allow for running over midnight
 *         		2.0 djm 16/11/2007	ykap 18760: Converted to use Report_Logs_vw
 *
 *         		DEPRECATED: Use report_log_history.sql for RR 3.4.2.0+
 */
	rlo."TimeStamp",
	rlo."Secs",
	rlo."Status",
	rlo."Step",
	rlo."Line"
from
	report_logs_vw rlo
where
	rlo.routine = 'Populate_Package'
--	and
--	rlo."TimeStamp" >= to_date('31-Oct-2008 16:49:13', 'DD-MON-YYYY HH24:MI:SS')
--	and rlo."Status" is not null -- Rows without a null status
--	and rlo."Status" = 'Error' -- Errors only
--order by 2 desc -- for Perfomance testing
--order by rlo.order_by -- Chronological, oldest first
order by rlo.order_by desc -- Reverse Chronological, most recent first. Can use PL/SQL Developer's Auto Refresh