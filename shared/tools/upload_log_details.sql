select
/*
 * NAME:		Upload Script Details
 * DESCRIPTION:	Displays preserved RR Upload logs
 *      		*** Do *NOT* convert to DBO ***
 *      		DBO may be unavailable if Upload failed !
 * HISTORY:		1.0 djm 15/02/2011	FRA-7569: Initial version
 */
	ups.version "RR Version",
	ups.script "Script name",
	ups.end_datetime "Script Finished",
	upl.line "Log"
from
	report_upload_scripts ups
--JOIN: Script's log lines
left outer join report_upload_logs upl
on upl.upload_script_id = ups.upload_script_id
where
	--CRITERIA: On/after specified p_create_datamart_objects
	ups.upload_script_id >= '&P1&'
	and
	--CRITERIA: Optionally restrict to specified script
--	'&P2&' in (' All Scripts', ups.script)
--	and
	--CRITERIA: Stop at next p_create_datamart_object
	ups.upload_script_id < (
		select
			coalesce(min(ups2.upload_script_id), 999999999)
		from
			report_upload_scripts ups2
		where
			--CRITERIA: First step in next Upload
			ups2.upload_script_id > '&P1&'
			and
			ups2.script in ('P_CREATE_DATAMART_OBJECTS', 'P_CREATE_DATAMART_OBJECTS_SS')
	)
order by
	ups.upload_script_id,
	upl.upload_log_id