set serverout on size 1000000
/*
 * NAME:		start_populate_ora.sql
 * DESCRIPTION:	Runs Populate script then exits (for Automated Testing)
 * HISTORY:     1.0 djm 17/12/2010 Initial version
 */
begin
	populate.main;
end;
/
exit
/
