@ECHO OFF
REM start_upload_populate_ora.bat
REM Starts RR Upload/Populate on specified Oracle database
REM NB. Called by automated_test_controller.bat
REM DJM 17/12/2010 Created
REM DJM 24/02/2011 Upload logs now preserved in DB: removed copy to T:\
REM DJM 24/02/2011 Populate logs now have history: no need for separate history on T:\

REM Cannot create log entry until params grabbed (otherwise writes to wrong log !)
REM NB. LOG is to for basic BAT output. Upload logs must still be checked
SET USERNAME=%1
SET PASSWORD=%2
SET DATABASE=%3
SET SOURCE_FOLDER=%4
SET LOG=%5

ECHO ------------------------------------------------------------ >>%LOG%

ECHO %DATE% %TIME% Started  Params >>%LOG%
ECHO username=%USERNAME%
ECHO password=%PASSWORD%
ECHO database=%DATABASE%
ECHO source_folder=%SOURCE_FOLDER%
ECHO log=%LOG%
ECHO %DATE% %TIME% Finished Params >>%LOG%

SET POPULATE_LOG=%SOURCE_FOLDER%\oracle\framework\Logs\Populate_%DATABASE%.log
ECHO populate_log=%POPULATE_LOG%

ECHO %DATE% %TIME% Started  Empty Upload Logs >>%LOG%
ERASE /q "%SOURCE_FOLDER%\oracle\framework\Logs\*"
ECHO %DATE% %TIME% Finished Empty Upload Logs >>%LOG%

ECHO %DATE% %TIME% Started  Ensure Skip table exists >>%LOG%
SQLPLUS %USERNAME%/%PASSWORD%@%DATABASE% @%TEST_TOOLS%\setup_skip_automated_test_ora.sql >>%LOG%
ECHO %DATE% %TIME% Finished Ensure Skip table exists >>%LOG%

REM Ensure database does not have skip-run recorded
ECHO %DATE% %TIME% Started  Check whether to skip test >>%LOG%
SQLPLUS -s  %USERNAME%/%PASSWORD%@%DATABASE% @%TEST_TOOLS%\check_skip_automated_test_ora.sql >C:\Temp\check_skip_%DATABASE%.bat
CALL "C:\Temp\check_skip_%DATABASE%.bat"
ECHO %skip_test%
if "%skip_test%"=="Y" (
	ECHO %DATE% %TIME% Result   Test should be SKIPPED >>%LOG%
	GOTO :END
) else (
	ECHO %DATE% %TIME% Result   Test should PROCEED >>%LOG%
)
ECHO %DATE% %TIME% Finished Check whether to skip test >>%LOG%

ECHO %DATE% %TIME% Started  Upload >>%LOG%
CD %SOURCE_FOLDER%\oracle\framework
CALL Upload_Reports_Repository %USERNAME% %PASSWORD% %DATABASE% AUTO >>%LOG%
ECHO %DATE% %TIME% Finished Upload >>%LOG%

REM Initial Population
ECHO %DATE% %TIME% Started  1st Populate >>%LOG%
SQLPLUS %USERNAME%/%PASSWORD%@%DATABASE% @%TEST_TOOLS%\start_populate_ora.sql >>%POPULATE_LOG%
ECHO %DATE% %TIME% Finished 1st Populate >>%LOG%

ECHO ------------------------------------------------------------ >>%POPULATE_LOG%

REM 2nd Population (to catch tricky issues)
ECHO %DATE% %TIME% Started  2nd Populate >>%LOG%
SQLPLUS %USERNAME%/%PASSWORD%@%DATABASE% @%TEST_TOOLS%\start_populate_ora.sql >>%POPULATE_LOG%
ECHO %DATE% %TIME% Finished 2nd Populate >>%LOG%

GOTO :END

REM End of script ------------------------------
:END

ECHO Test finished >>%LOG%

GOTO :EOF