-- check_skip_automated_test_ora.sql
-- Checks whether automated test should be skipped
-- NB. Do not change this to a standard comment or output will be corrupted !
-- DJM 06/01/2011 Initial version
set define off
set feedback off
set heading off
-- Returns dynamic variable set to whether test should be skipped
-- NB. Avoids DBO functions in case problem uploading
select
	'set skip_test='||
	case
		when exists (
			select 'x'
			from
				report_skip_automated_test sat
			where
				trunc(sat.run_date) = trunc(sysdate)
		) then
			'Y'
		else
			'N'
	end
from
	dual
/
exit
/
