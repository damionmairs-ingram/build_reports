@ECHO OFF
REM start_upload_populate_ss.bat
REM Starts RR Upload/Populate on specified SQLServer database
REM NB. Called by automated_test_controller.bat
REM DJM 17/12/2010 Created
REM DJM 24/02/2011 Upload logs now preserved in DB: removed copy to T:\
REM DJM 24/02/2011 Populate logs now have history: no need for separate history on T:\

REM Cannot create log entry until params grabbed (otherwise writes to wrong log !)
REM NB. LOG is to for basic BAT output. Upload logs must still be checked
SET USERNAME=%1
SET PASSWORD=%2
SET HOST=%3
SET DATABASE=%4
SET SOURCE_FOLDER=%5
SET LOG=%6

ECHO ------------------------------------------------------------ >>%LOG%

ECHO %DATE% %TIME% Started  Params >>%LOG%
ECHO username=%USERNAME%
ECHO password=%PASSWORD%
ECHO host=%HOST%
ECHO database=%DATABASE%
ECHO source_folder=%SOURCE_FOLDER%
ECHO log=%LOG%
ECHO %DATE% %TIME% Finished Params >>%LOG%

SET POPULATE_LOG=%SOURCE_FOLDER%\sql_server\framework\Logs\Populate_%DATABASE%.log
ECHO populate_log=%POPULATE_LOG%

ECHO %DATE% %TIME% Started  Empty Upload Logs >>%LOG%
ERASE /q "%SOURCE_FOLDER%\sql_server\framework\Logs\*"
ECHO %DATE% %TIME% Finished Empty Upload Logs >>%LOG%

ECHO %DATE% %TIME% Started  Ensure Skip table exists >>%LOG%
SQLCMD -U %username% -P %password% -S %host% -d %database% -w 132 -W -i %TEST_TOOLS%\setup_skip_automated_test_ss.sql >>%LOG%
ECHO %DATE% %TIME% Finished Ensure Skip table exists >>%LOG%

REM Ensure database does not have skip-run recorded
ECHO %DATE% %TIME% Started  Check whether to skip test >>%LOG%
SQLCMD -U %username% -P %password% -S %host% -d %database% -h-1 -Q "set nocount on select 'set skip_test=' + case when exists (select 'x' from report_skip_automated_test sat where convert(datetime, convert(varchar, sat.run_date, 103), 103) = convert(datetime, convert(varchar, getdate(), 103), 103)) then 'Y' else 'N' end from dual" >C:\Temp\check_skip_%DATABASE%.bat
CALL "C:\Temp\check_skip_%DATABASE%.bat"
ECHO %skip_test%
REM Some versions of OSQL append a space !
if "%skip_test%"=="Y" (
	ECHO %DATE% %TIME% Result   Test should be SKIPPED >>%LOG%
	GOTO :END
) else if "%skip_test%"=="Y " (
	ECHO %DATE% %TIME% Result   Test should be SKIPPED >>%LOG%
	GOTO :END
) else (
	ECHO %DATE% %TIME% Result   Test should PROCEED >>%LOG%
)
ECHO %DATE% %TIME% Finished Check whether to skip test >>%LOG%

ECHO %DATE% %TIME% Started  Upload >>%LOG%
CD %SOURCE_FOLDER%\sql_server\framework
CALL Upload_Reports_Repository_ss %USERNAME% %PASSWORD% %HOST% %DATABASE% AUTO >>%LOG%
ECHO %DATE% %TIME% Finished Upload >>%LOG%

REM Initial Population
ECHO %DATE% %TIME% Started  1st Populate >>%LOG%
SQLCMD -U %username% -P %password% -S %host% -d %database% -w 132 -W -Q "set nocount on begin execute dbo.pop_main; end;" >>%POPULATE_LOG%
ECHO %DATE% %TIME% Finished 1st Populate >>%LOG%

ECHO ------------------------------------------------------------ >>%POPULATE_LOG%

REM 2nd Population (to catch tricky issues)
ECHO %DATE% %TIME% Started  2nd Populate >>%LOG%
SQLCMD -U %username% -P %password% -S %host% -d %database% -w 132 -W -Q "set nocount on begin execute dbo.pop_main; end;" >>%POPULATE_LOG%
ECHO %DATE% %TIME% Finished 2nd Populate >>%LOG%

GOTO :END

REM End of script ------------------------------
:END

ECHO Test finished >>%LOG%

GOTO :EOF