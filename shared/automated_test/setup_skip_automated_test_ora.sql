set serverout on size 1000000
/*
 * NAME:		setup_skip_automated_test_ora.sql
 * DESCRIPTION:	Sets up objects to skip Automated Test
 * HISTORY:     1.0 djm 21/12/2010 Initial version
 */
declare
	v_already_exists varchar(1);
begin
	-- Check if table already exists
	select
		case
			when exists (
				select
					'x'
				from
					user_tables tab
				where
					tab.table_name = 'REPORT_SKIP_AUTOMATED_TEST'
			) then
				'Y'
			else
				'N'
		end
	into v_already_exists
	from
		dual;
	--
	if v_already_exists = 'Y' then
		dbms_output.put_line('Table report_skip_automated_test already exists');
	else
		dbms_output.put_line('Creating table report_skip_automated_test');
		-- NB. One Row per day to skip
		execute immediate 'create table report_skip_automated_test (
							run_date	date			default trunc(sysdate) not null,
							db_user		varchar(30)		default user not null,
							reason		varchar(255)	not null,
							constraint report_skip_automated_test_pk
							primary key (run_date)
						)';
		dbms_output.put_line('Created report_skip_automated_test Table');
	end if;
end;
/
exit
/
