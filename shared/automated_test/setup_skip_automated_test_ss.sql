/*
 * NAME:		setup_skip_automated_test_ss.sql
 * DESCRIPTION:	Sets up objects to skip Automated Test
 * HISTORY:     1.0 djm 23/12/2010 Initial version
 */
begin
	declare @v_already_exists varchar(1);
	--
	-- Check if table already exists
	select
		@v_already_exists =
		case
			when exists (
				select
					'x'
				from
					dbo.sysobjects tab
				where
					tab.name = 'REPORT_SKIP_AUTOMATED_TEST'
			) then
				'Y'
			else
				'N'
		end
	from
		dual;
	--
	if @v_already_exists = 'Y'
	begin
		print 'Table report_skip_automated_test already exists';
	end
	else
	begin
		print 'Creating table report_skip_automated_test';
		-- NB. One Row per day to skip
		execute('create table report_skip_automated_test (
							run_date	date			default convert(datetime, convert(varchar, getdate(), 103), 103) not null,
							db_user		varchar(30)		default system_user not null,
							reason		varchar(255)	not null,
							constraint report_skip_automated_test_pk
							primary key (run_date)
						)');
		print 'Created report_skip_automated_test Table';
	end;
end;
go
exit
