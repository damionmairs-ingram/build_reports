@ECHO OFF
REM automated_test_controller.bat
REM Controls Automated RR Test
REM DJM 17/12/2010 Created
REM DJM 24/02/2011 Upload logs now preserved in DB: removed copy to T:\
REM DJM 24/02/2011 Populate logs now have history: no need for separate history on T:\

REM Constants
SET TEST_HOME=C:\SVN_Automated_Testing
SET LOG_HOME=T:\Automated_Testing_Logs
SET LOG=%LOG_HOME%\automated_test_controller.log
SET REPORTS_HOME=%TEST_HOME%\Branch_reports
SET TEST_TOOLS=%REPORTS_HOME%\internal\shared\automated_test
SET RR_DIR_PREFIX=reports-

ECHO Parse Date components
REM NB. Assume %DATE% is dd/mm/yyyy
ECHO %DATE% %TIME% Started  Parse Date components >>%LOG%
CALL SET DD=%%DATE:~0,2%%
CALL SET MM=%%DATE:~3,2%%
CALL SET YYYY=%%DATE:~6,4%%
SET YYYYMMDD=%YYYY%%MM%%DD%
ECHO %YYYYMMDD% >>%LOG%
ECHO %DATE% %TIME% Finished Parse Date components >>%LOG%

CD %REPORTS_HOME%

ECHO Refresh SVN
REM Currently could refresh this BAT while running !
ECHO %DATE% %TIME% Started  SVN Refresh >>%LOG%
svn update --accept theirs-full >>%LOG%
IF errorlevel 1 (
	ECHO Return Code >= 1 >>%LOG%
	ECHO Return Code = %ERRORLEVEL% >>%LOG%
) else (
	ECHO Return Code = 0 >>%LOG%
)
ECHO %DATE% %TIME% Finished SVN Refresh >>%LOG%

ECHO Find latest RR
REM NB. Based on foldername
ECHO %DATE% %TIME% Started  Find latest folder >>%LOG%
FOR /F %%d IN ('DIR /B /O N "%RR_DIR_PREFIX%*"') DO SET LATEST_RR_DIR=%%d
ECHO %LATEST_RR_DIR% >>%LOG%
ECHO %DATE% %TIME% Finished Find latest folder >>%LOG%

CALL :TEST_ORACLE fw fw local_brent
CALL :TEST_SQLSERVER fw d3v3lop ss-lon1.dev.corelogic.local reports_worcs

rem CALL :TEST_ORACLE fwrich2 fwrich2 fwtest
rem CALL :TEST_SQLSERVER fw fw ss2005-test1 blackpoolnew

ECHO --- >>%LOG%

GOTO :END

REM Run Oracle Upload/Populate ------------------------------------
:TEST_ORACLE
REM Params: username password database
ECHO Run Oracle Upload/Populate
ECHO %DATE% %TIME% Started  Call Oracle Upload >>%LOG%
SET username=%1%
SET password=%2%
SET database=%3%

REM Create log directories
IF NOT EXIST %LOG_HOME%\%database% (
	MKDIR %LOG_HOME%\%database%
)

START %TEST_TOOLS%\start_upload_populate_ora.bat %username% %password% %database% %REPORTS_HOME%\%LATEST_RR_DIR% %LOG_HOME%\%database%.log
ECHO %DATE% %TIME% Finished Call Oracle Upload >>%LOG%
GOTO :EOF

REM Run SQLServer Upload/Populate ------------------------------------
:TEST_SQLSERVER
REM Params: username password host database
ECHO Run SQLServer Upload/Populate
ECHO %DATE% %TIME% Started  Call SQLServer Upload >>%LOG%
SET username=%1%
SET password=%2%
SET host=%3%
SET database=%4%

REM Create log directories
IF NOT EXIST %LOG_HOME%\%database% (
	MKDIR %LOG_HOME%\%database%
)

START %TEST_TOOLS%\start_upload_populate_ss.bat %username% %password% %host% %database% %REPORTS_HOME%\%LATEST_RR_DIR% %LOG_HOME%\%database%.log
ECHO %DATE% %TIME% Finished Call SQLServer Upload >>%LOG%
GOTO :EOF

REM End of Automated Test script ------------------------------
:END
GOTO :EOF