drop view export_oracle_reports
go
create view [dbo].[export_oracle_reports] as
select
/*
 * NAME:		Export Oracle Repository Reports
 * DESCRIPTION:	This script will extract all Oracle reports from the repository in the 'Mosaic Reports' Node
 * HISTORY:		1.0  kt  23/11/2004	initial version
 *				1.1  kt  12/1/2005	added sql to extract rap reports
 *				1.2  kt  15/3/2005	removed children in need census reports
 *				1.3  kt  20/4/2005	amended to deal with new tables (report_instances, etc)
 *				2.0  djm 04/04/2008	ykap 20898: Amended script so IMU not required for export
 *				2.1  djm 10/04/2008	ykap 21035: Reports for 'Both Oracle and SQL Server'
 *				2.2  tw  20/10/2008	removed condition to exlude 'Children in Need Census' directory
 *				3.0  djm 22/04/2010	Tidied-up for use with build_repository.bat
 *				3.1  djm 31/07/2010	Separated columns, due to varchar(8000) cap on SS 2000;
 *				                    Embedded separator to simplify export code
 *				3.2  AC  09/09/2013 added condition to exclude 'Mosaic Reports' node
 */
	-- report_classes
	coalesce(export_data.rc_description, '') + '^'							rc_description,			--  1
	coalesce(cast(export_data.rc_id as varchar), '') + '^'					rc_id,					--  2
	coalesce(cast(export_data.rc_parent_class_id as varchar), '') + '^'		rc_parent_class_id,		--  3
	-- report_parameters
	coalesce(export_data.rp_data_type, '') + '^'							rp_data_type,			--  4
	coalesce(export_data.rp_default_value, '') + '^'						rp_default_value,		--  5
	coalesce(cast(export_data.rp_id as varchar), '') + '^'					rp_id,					--  6
	coalesce(export_data.rp_label, '') + '^'								rp_label,				--  7
	coalesce(export_data.rp_list_sql, '') + '^'								rp_list_sql,			--  8
	coalesce(export_data.rp_optional_flag, '') + '^'						rp_optional_flag,		--  9
	-- report_sections
	coalesce(export_data.rs_definition, '') + '^'							rs_definition,			-- 10
	coalesce(export_data.rs_has_sub_section, '') + '^'						rs_has_sub_section,		-- 11
	coalesce(cast(export_data.rs_id as varchar), '') + '^'					rs_id,					-- 12
	coalesce(export_data.rs_no_results_flag, '') + '^'						rs_no_results_flag,		-- 13
	coalesce(export_data.rs_parent_column_name, '') + '^'					rs_parent_column_name,	-- 14
	coalesce(export_data.rs_sql_text1, '') + '^'							rs_sql_text1,			-- 15	chars     1- 4000
	coalesce(export_data.rs_sql_text2, '') + '^'							rs_sql_text2,			-- 16	chars  4001- 8000
	coalesce(export_data.rs_sql_text3, '') + '^'							rs_sql_text3,			-- 17	chars  8001-12000
	coalesce(export_data.rs_sql_text4, '') + '^'							rs_sql_text4,			-- 18	chars 12001-16000
	coalesce(export_data.rs_sql_text5, '') + '^'							rs_sql_text5,			-- 19	chars 16001-20000
	coalesce(export_data.rs_sub_report_name, '') + '^'						rs_sub_report_name,		-- 20
	coalesce(export_data.rs_title, '') + '^'								rs_title,				-- 21
	-- reports
	coalesce(export_data.r_definition, '') + '^'							r_definition,			-- 22
	coalesce(cast(export_data.r_id as varchar), '') + '^'					r_id,					-- 23
	-- (convert from char to varchar)
	coalesce(cast(export_data.r_no_direct_request as varchar), '') + '^'	r_no_direct_request,	-- 24
	coalesce(export_data.r_template_filename, '') + '^'						r_template_filename,	-- 25
	coalesce(export_data.r_title, '') + '^'									r_title					-- 26
from (
	-- Inline view separating criteria from formatting
	-- The inline view SQL is unchanged from v1.*; it should be tidied-up once comparison with IMU version is no longer necessary
	 select
		rc.description	"RC_DESCRIPTION"
		,rc.id 			"RC_ID"
		,rc.parent_class_id "RC_PARENT_CLASS_ID"
		,null	"RP_DATA_TYPE"
		,null	"RP_DEFAULT_VALUE"
		,null 	"RP_ID"
		,null	"RP_LABEL"
		,null	"RP_LIST_SQL"
		,null	"RP_OPTIONAL_FLAG"
		,null	"RS_DEFINITION"
		,null	"RS_HAS_SUB_SECTION"
		,null 	"RS_ID"
		,null	"RS_NO_RESULTS_FLAG"
		,null	"RS_PARENT_COLUMN_NAME"
		,null	"RS_SQL_TEXT1"
		,null	"RS_SQL_TEXT2"
		,null	"RS_SQL_TEXT3"
		,null	"RS_SQL_TEXT4"
		,null	"RS_SQL_TEXT5"
		,null	"RS_SUB_REPORT_NAME"
		,null	"RS_TITLE"
		,null	"R_DEFINITION"
		,null 	"R_ID"	
		,null	"R_NO_DIRECT_REQUEST"
		,null	"R_TEMPLATE_FILENAME"
		,null	"R_TITLE"
	
	from report_classes rc

	where
		rc.id = 1

	union all 

	--Level 2 (of 5)
	select 
		rc.description								"RC_DESCRIPTION"
		,rc.id 										"RC_ID"
		,rc.parent_class_id 						"RC_PARENT_CLASS_ID"
		,null	"RP_DATA_TYPE"
		,null	"RP_DEFAULT_VALUE"
		,null 	"RP_ID"
		,null	"RP_LABEL"
		,null	"RP_LIST_SQL"
		,null	"RP_OPTIONAL_FLAG"
		,null	"RS_DEFINITION"
		,null	"RS_HAS_SUB_SECTION"
		,null 	"RS_ID"
		,null	"RS_NO_RESULTS_FLAG"
		,null	"RS_PARENT_COLUMN_NAME"
		,null	"RS_SQL_TEXT1"
		,null	"RS_SQL_TEXT2"
		,null	"RS_SQL_TEXT3"
		,null	"RS_SQL_TEXT4"
		,null	"RS_SQL_TEXT5"
		,null	"RS_SUB_REPORT_NAME"
		,null	"RS_TITLE"
		,null	"R_DEFINITION"
		,null 	"R_ID"	
		,null	"R_NO_DIRECT_REQUEST"
		,null	"R_TEMPLATE_FILENAME"
		,null	"R_TITLE"
	
	from report_classes rc
	where 
		rc.parent_class_id = 1
		--CRITERIA: Only select 'Mosaic Reports' node
		and 
		rc.description != 'Mosaic Reports'
	
	union all
	
	--Level 3 (of 5)
	select 
		rc1.description								"RC_DESCRIPTION"
		,rc1.id 										"RC_ID"
		,rc1.parent_class_id 						"RC_PARENT_CLASS_ID"
		,null	"RP_DATA_TYPE"
		,null	"RP_DEFAULT_VALUE"
		,null 	"RP_ID"
		,null	"RP_LABEL"
		,null	"RP_LIST_SQL"
		,null	"RP_OPTIONAL_FLAG"
		,null	"RS_DEFINITION"
		,null	"RS_HAS_SUB_SECTION"
		,null 	"RS_ID"
		,null	"RS_NO_RESULTS_FLAG"
		,null	"RS_PARENT_COLUMN_NAME"
		,null	"RS_SQL_TEXT1"
		,null	"RS_SQL_TEXT2"
		,null	"RS_SQL_TEXT3"
		,null	"RS_SQL_TEXT4"
		,null	"RS_SQL_TEXT5"
		,null	"RS_SUB_REPORT_NAME"
		,null	"RS_TITLE"
		,null	"R_DEFINITION"
		,null 	"R_ID"	
		,null	"R_NO_DIRECT_REQUEST"
		,null	"R_TEMPLATE_FILENAME"
		,null	"R_TITLE"
	
	from report_classes rc1
	
	--JOIN: find parent report class 
	inner join report_classes rc2
	on rc2.id = rc1.parent_class_id
	
	where
	   --CRITERIA: Parent class of parent class is 'Frameworki Reporting Repository'
	   rc2.parent_class_id = 1
	   and
	   rc2.description != 'Mosaic Reports'
	
	union all
	
	--Level 4 (of 5)
	select 
		rc1.description								"RC_DESCRIPTION"
		,rc1.id 									"RC_ID"
		,rc1.parent_class_id 						"RC_PARENT_CLASS_ID"
		,rp.data_type								"RP_DATA_TYPE"
		,rp.default_value							"RP_DEFAULT_VALUE"
		,rp.id 										"RP_ID"
		,rp.label									"RP_LABEL"
		,dbo.f_convert_to_symbols(rp.list_sql)		"RP_LIST_SQL"
		,rp.optional_flag							"RP_OPTIONAL_FLAG"
		,dbo.f_convert_to_symbols(rs.definition)	"RS_DEFINITION"
		,rs.has_sub_section							"RS_HAS_SUB_SECTION"
		,rs.id 										"RS_ID"
		,rs.no_results_flag							"RS_NO_RESULTS_FLAG"
		,rs.parent_column_name						"RS_PARENT_COLUMN_NAME"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,1,4000))		"RS_SQL_TEXT1"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,4001,4000))		"RS_SQL_TEXT2"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,8001,4000))		"RS_SQL_TEXT3"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,12001,4000))		"RS_SQL_TEXT4"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,16001,4000))		"RS_SQL_TEXT5"
		,rs.sub_report_name							"RS_SUB_REPORT_NAME"
		,rs.title									"RS_TITLE"
		,dbo.f_convert_to_symbols(r.definition)		"R_DEFINITION"
		,r.id 										"R_ID"	
		,r.no_direct_request						"R_NO_DIRECT_REQUEST"
		,r.template_filename						"R_TEMPLATE_FILENAME"
		,dbo.f_convert_to_symbols(r.title)			"R_TITLE"
	
	from report_classes rc1
	
	--JOIN: find reports
	left outer join reports r
	on r.report_class_id = rc1.id
	
	--NEW
	left outer join report_instances ri
	on ri.report_id = r.id
		and
		ri.current_version = 'Y'
		and
		ri.database_type in ('ORACLE', 'BOTH')
	
	--NEW
	left outer join report_instance_sections rs
	on rs.report_instance_id = ri.id
	
	--NEW
	left outer join report_instance_parameters rp
	on rp.report_instance_id = ri.id
	
	-- --JOIN: find report_sections rs
	-- left outer join report_sections rs
	-- on rs.report_id = r.id
	-- 
	-- --JOIN: find report parmeters rp
	-- left outer join report_parameters rp
	-- on rp.report_id = r.id
	
	--JOIN: find parent report class 
	inner join report_classes rc2
	on rc2.id = rc1.parent_class_id
	
	--JOIN: find grandparent report class 
	inner join report_classes rc3
	on rc3.id = rc2.parent_class_id
	
	where
	   --CRITERIA: Parent class of grandparent class is 'Frameworki Reporting Repository'
	   rc3.parent_class_id = 1
	   --CRITERIA: Only select 'Mosaic Reports' node
	   and rc3.description !=   'Mosaic Reports'
	
	union all
	
	--Level 5 (of 5)
	select 
		rc1.description								"RC_DESCRIPTION"
		,rc1.id 									"RC_ID"
		,rc1.parent_class_id 						"RC_PARENT_CLASS_ID"
		,rp.data_type								"RP_DATA_TYPE"
		,rp.default_value							"RP_DEFAULT_VALUE"
		,rp.id 										"RP_ID"
		,rp.label									"RP_LABEL"
		,dbo.f_convert_to_symbols(rp.list_sql)		"RP_LIST_SQL"
		,rp.optional_flag							"RP_OPTIONAL_FLAG"
		,dbo.f_convert_to_symbols(rs.definition)	"RS_DEFINITION"
		,rs.has_sub_section							"RS_HAS_SUB_SECTION"
		,rs.id 										"RS_ID"
		,rs.no_results_flag							"RS_NO_RESULTS_FLAG"
		,rs.parent_column_name						"RS_PARENT_COLUMN_NAME"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,1,4000))		"RS_SQL_TEXT1"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,4001,4000))		"RS_SQL_TEXT2"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,8001,4000))		"RS_SQL_TEXT3"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,12001,4000))		"RS_SQL_TEXT4"
		,dbo.f_convert_to_symbols(substring(rs.sql_text,16001,4000))		"RS_SQL_TEXT5"
		,rs.sub_report_name							"RS_SUB_REPORT_NAME"
		,rs.title									"RS_TITLE"
		,dbo.f_convert_to_symbols(r.definition)		"R_DEFINITION"
		,r.id 										"R_ID"	
		,r.no_direct_request						"R_NO_DIRECT_REQUEST"
		,r.template_filename						"R_TEMPLATE_FILENAME"
		,dbo.f_convert_to_symbols(r.title)			"R_TITLE"
	
	from report_classes rc1
	
	--JOIN: find reports
	inner join reports r
	on r.report_class_id = rc1.id
	
	--NEW
	left outer join report_instances ri
	on ri.report_id = r.id
		and
		ri.current_version = 'Y'
		and
		ri.database_type in ('ORACLE', 'BOTH')
	
	--NEW
	left outer join report_instance_sections rs
	on rs.report_instance_id = ri.id
	
	--NEW
	left outer join report_instance_parameters rp
	on rp.report_instance_id = ri.id
	
	-- --JOIN: find report_sections rs
	-- left outer join report_sections rs
	-- on rs.report_id = r.id
	-- 
	-- --JOIN: find report parmeters rp
	-- left outer join report_parameters rp
	-- on rp.report_id = r.id
	
	--JOIN: find parent report class 
	inner join report_classes rc2
	on rc2.id = rc1.parent_class_id
	
	--JOIN: find grandparent report class 
	inner join report_classes rc3
	on rc3.id = rc2.parent_class_id
	
	--JOIN: find great-grandparent report class 
	inner join report_classes rc4
	on rc4.id = rc3.parent_class_id
	
	where
	   --CRITERIA: Parent class of great-grandparent class is 'Frameworki Reporting Repository'
	   rc4.parent_class_id = 1
	   --CRITERIA: Only select 'Mosaic Reports' node
	   and rc4.description !=   'Mosaic Reports'
	
) export_data


GO


