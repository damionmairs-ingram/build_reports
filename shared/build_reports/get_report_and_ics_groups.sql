select
	rgc.CATEGORY_ORDER		"RGC_CATEGORY_ORDER"
	,rgc.CATEGORY_VALUE		"RGC_CATEGORY_VALUE"
	,rgc.ID					"RGC_ID"
	,rgc.REPORT_GROUP_ID	"RGC_REPORT_GROUP_ID"
	,null					"RGM_ID"
	,null					"RGM_MAPPED_VALUE"
	,null 					"RGM_REPORT_GROUP_CATEGORY_ID"
	,rg.CONTEXT				"RG_CONTEXT"
	,f_convert_to_symbols(rg.DESCRIPTION)	"RG_DESCRIPTION"
	,rg.DISPLAY_COLUMN		"RG_DISPLAY_COLUMN"
	,case
		when rg.source_table not in 
			(
			'ITEMS'
			)
		then
			rg.form_page_number
	end						"RG_FORM_PAGE_NUMBER"
	,case
		when rg.source_table not in 
			(
			'ITEMS'
			)
		then
			rg.form_type_id
	end						"RG_FORM_TYPE_ID"
	,rg.ID					"RG_ID"
	,rg.item_id				"RG_ITEM_ID"
	,rg.NAME				"RG_NAME"
	,case
		when rg.source_table not in
			(
			'QUESTIONS'
			)
		then rg.question_id
	end						"RG_QUESTION_ID"
	,case
		when rg.source_table not in
			(
			'QUESTIONS'
			)
		then rg.QUESTIONNAIRE_ID
	end						"RG_QUESTIONNAIRE_ID"
	,case
		when rg.source_table not in 
			(
			'FORM_TYPES'
			,'FORM_TYPES_ACTIVE'
			)
		then rg.SELECTION_COLUMN			
	end						"RG_SELECTION_COLUMN"
	,case
		when rg.source_table not in 
			(
			'FORM_TYPES'
			,'FORM_TYPES_ACTIVE'
			)
		then rg.SELECTION_VALUE				
	end 					"RG_SELECTION_VALUE"
	,rg.SOURCE_COLUMN		"RG_SOURCE_COLUMN"
	,rg.SOURCE_TABLE		"RG_SOURCE_TABLE"

from 
	report_groups rg

inner join report_group_categories rgc
on rgc.report_group_id = rg.id
