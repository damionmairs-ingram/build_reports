select

/*
 * NAME:		Extract all repository reports
 * DESCRIPTION:	This script will extract all reports from the repository
 * HISTORY:		1.0  kt  23/11/2004	initial version
 *				1.1  kt  12/1/2005	added sql to extract rap reports
 *				1.2  kt  15/3/2005	removed children in need census reports
 *				1.3  kt  20/4/2005	ameded to deal with new tables (report_instances, etc)
 *				1.4  djm 10/04/2008	ykap 21035: Reports for 'Both Oracle and SQL Server'
 *				1.5  tw  20/10/2008 removed condition to exlude 'Children in Need Census' directory
 */

	rc.description	"RC_DESCRIPTION"
	,rc.id 			"RC_ID"
	,rc.parent_class_id "RC_PARENT_CLASS_ID"
	,null	"RP_DATA_TYPE"
	,null	"RP_DEFAULT_VALUE"
	,null 	"RP_ID"
	,null	"RP_LABEL"
	,null	"RP_LIST_SQL"
	,null	"RP_OPTIONAL_FLAG"
	,null	"RS_DEFINITION"
	,null	"RS_HAS_SUB_SECTION"
	,null 	"RS_ID"
	,null	"RS_NO_RESULTS_FLAG"
	,null	"RS_PARENT_COLUMN_NAME"
	,null	"RS_SQL_TEXT1"
	,null	"RS_SQL_TEXT2"
	,null	"RS_SQL_TEXT3"
	,null	"RS_SQL_TEXT4"
	,null	"RS_SQL_TEXT5"
	,null	"RS_SUB_REPORT_NAME"
	,null	"RS_TITLE"
	,null	"R_DEFINITION"
	,null 	"R_ID"	
	,null	"R_NO_DIRECT_REQUEST"
	,null	"R_TEMPLATE_FILENAME"
	,null	"R_TITLE"

from report_classes rc

where
	rc.id = 1

union all

--Level 2 (of 5)
select 
	rc.description								"RC_DESCRIPTION"
	,rc.id 										"RC_ID"
	,rc.parent_class_id 						"RC_PARENT_CLASS_ID"
	,null	"RP_DATA_TYPE"
	,null	"RP_DEFAULT_VALUE"
	,null 	"RP_ID"
	,null	"RP_LABEL"
	,null	"RP_LIST_SQL"
	,null	"RP_OPTIONAL_FLAG"
	,null	"RS_DEFINITION"
	,null	"RS_HAS_SUB_SECTION"
	,null 	"RS_ID"
	,null	"RS_NO_RESULTS_FLAG"
	,null	"RS_PARENT_COLUMN_NAME"
	,null	"RS_SQL_TEXT1"
	,null	"RS_SQL_TEXT2"
	,null	"RS_SQL_TEXT3"
	,null	"RS_SQL_TEXT4"
	,null	"RS_SQL_TEXT5"
	,null	"RS_SUB_REPORT_NAME"
	,null	"RS_TITLE"
	,null	"R_DEFINITION"
	,null 	"R_ID"	
	,null	"R_NO_DIRECT_REQUEST"
	,null	"R_TEMPLATE_FILENAME"
	,null	"R_TITLE"

from report_classes rc
where 
	rc.parent_class_id = 1

union all

--Level 3 (of 5)
select 
	rc1.description								"RC_DESCRIPTION"
	,rc1.id 										"RC_ID"
	,rc1.parent_class_id 						"RC_PARENT_CLASS_ID"
	,null	"RP_DATA_TYPE"
	,null	"RP_DEFAULT_VALUE"
	,null 	"RP_ID"
	,null	"RP_LABEL"
	,null	"RP_LIST_SQL"
	,null	"RP_OPTIONAL_FLAG"
	,null	"RS_DEFINITION"
	,null	"RS_HAS_SUB_SECTION"
	,null 	"RS_ID"
	,null	"RS_NO_RESULTS_FLAG"
	,null	"RS_PARENT_COLUMN_NAME"
	,null	"RS_SQL_TEXT1"
	,null	"RS_SQL_TEXT2"
	,null	"RS_SQL_TEXT3"
	,null	"RS_SQL_TEXT4"
	,null	"RS_SQL_TEXT5"
	,null	"RS_SUB_REPORT_NAME"
	,null	"RS_TITLE"
	,null	"R_DEFINITION"
	,null 	"R_ID"	
	,null	"R_NO_DIRECT_REQUEST"
	,null	"R_TEMPLATE_FILENAME"
	,null	"R_TITLE"

from report_classes rc1

--JOIN: find parent report class 
inner join report_classes rc2
on rc2.id = rc1.parent_class_id

where
   --CRITERIA: Parent class of parent class is 'Frameworki Reporting Repository'
   rc2.parent_class_id = 1

union all

--Level 4 (of 5)
select 
	rc1.description								"RC_DESCRIPTION"
	,rc1.id 									"RC_ID"
	,rc1.parent_class_id 						"RC_PARENT_CLASS_ID"
	,rp.data_type								"RP_DATA_TYPE"
	,rp.default_value							"RP_DEFAULT_VALUE"
	,rp.id 										"RP_ID"
	,rp.label									"RP_LABEL"
	,dbo.f_convert_to_symbols(rp.list_sql)		"RP_LIST_SQL"
	,rp.optional_flag							"RP_OPTIONAL_FLAG"
	,dbo.f_convert_to_symbols(rs.definition)	"RS_DEFINITION"
	,rs.has_sub_section							"RS_HAS_SUB_SECTION"
	,rs.id 										"RS_ID"
	,rs.no_results_flag							"RS_NO_RESULTS_FLAG"
	,rs.parent_column_name						"RS_PARENT_COLUMN_NAME"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,1,4000))		"RS_SQL_TEXT1"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,4001,4000))		"RS_SQL_TEXT2"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,8001,4000))		"RS_SQL_TEXT3"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,12001,4000))		"RS_SQL_TEXT4"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,16001,4000))		"RS_SQL_TEXT5"
	,rs.sub_report_name							"RS_SUB_REPORT_NAME"
	,rs.title									"RS_TITLE"
	,dbo.f_convert_to_symbols(r.definition)		"R_DEFINITION"
	,r.id 										"R_ID"	
	,r.no_direct_request						"R_NO_DIRECT_REQUEST"
	,r.template_filename						"R_TEMPLATE_FILENAME"
	,dbo.f_convert_to_symbols(r.title)			"R_TITLE"

from report_classes rc1

--JOIN: find reports
left outer join reports r
on r.report_class_id = rc1.id

--NEW
left outer join report_instances ri
on ri.report_id = r.id
	and
	ri.current_version = 'Y'
	and
	ri.database_type in ('ORACLE', 'BOTH')

--NEW
left outer join report_instance_sections rs
on rs.report_instance_id = ri.id

--NEW
left outer join report_instance_parameters rp
on rp.report_instance_id = ri.id

-- --JOIN: find report_sections rs
-- left outer join report_sections rs
-- on rs.report_id = r.id
-- 
-- --JOIN: find report parmeters rp
-- left outer join report_parameters rp
-- on rp.report_id = r.id

--JOIN: find parent report class 
inner join report_classes rc2
on rc2.id = rc1.parent_class_id

--JOIN: find grandparent report class 
inner join report_classes rc3
on rc3.id = rc2.parent_class_id

where
   --CRITERIA: Parent class of grandparent class is 'Frameworki Reporting Repository'
   rc3.parent_class_id = 1

union all

--Level 5 (of 5)
select 
	rc1.description								"RC_DESCRIPTION"
	,rc1.id 									"RC_ID"
	,rc1.parent_class_id 						"RC_PARENT_CLASS_ID"
	,rp.data_type								"RP_DATA_TYPE"
	,rp.default_value							"RP_DEFAULT_VALUE"
	,rp.id 										"RP_ID"
	,rp.label									"RP_LABEL"
	,dbo.f_convert_to_symbols(rp.list_sql)		"RP_LIST_SQL"
	,rp.optional_flag							"RP_OPTIONAL_FLAG"
	,dbo.f_convert_to_symbols(rs.definition)	"RS_DEFINITION"
	,rs.has_sub_section							"RS_HAS_SUB_SECTION"
	,rs.id 										"RS_ID"
	,rs.no_results_flag							"RS_NO_RESULTS_FLAG"
	,rs.parent_column_name						"RS_PARENT_COLUMN_NAME"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,1,4000))		"RS_SQL_TEXT1"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,4001,4000))		"RS_SQL_TEXT2"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,8001,4000))		"RS_SQL_TEXT3"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,12001,4000))		"RS_SQL_TEXT4"
	,dbo.f_convert_to_symbols(substring(rs.sql_text,16001,4000))		"RS_SQL_TEXT5"
	,rs.sub_report_name							"RS_SUB_REPORT_NAME"
	,rs.title									"RS_TITLE"
	,dbo.f_convert_to_symbols(r.definition)		"R_DEFINITION"
	,r.id 										"R_ID"	
	,r.no_direct_request						"R_NO_DIRECT_REQUEST"
	,r.template_filename						"R_TEMPLATE_FILENAME"
	,dbo.f_convert_to_symbols(r.title)			"R_TITLE"

from report_classes rc1

--JOIN: find reports
inner join reports r
on r.report_class_id = rc1.id

--NEW
left outer join report_instances ri
on ri.report_id = r.id
	and
	ri.current_version = 'Y'
	and
	ri.database_type in ('ORACLE', 'BOTH')

--NEW
left outer join report_instance_sections rs
on rs.report_instance_id = ri.id

--NEW
left outer join report_instance_parameters rp
on rp.report_instance_id = ri.id

-- --JOIN: find report_sections rs
-- left outer join report_sections rs
-- on rs.report_id = r.id
-- 
-- --JOIN: find report parmeters rp
-- left outer join report_parameters rp
-- on rp.report_id = r.id

--JOIN: find parent report class 
inner join report_classes rc2
on rc2.id = rc1.parent_class_id

--JOIN: find grandparent report class 
inner join report_classes rc3
on rc3.id = rc2.parent_class_id

--JOIN: find great-grandparent report class 
inner join report_classes rc4
on rc4.id = rc3.parent_class_id

where
   --CRITERIA: Parent class of great-grandparent class is 'Frameworki Reporting Repository'
   rc4.parent_class_id = 1
