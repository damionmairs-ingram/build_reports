select 

/*
 * NAME:			Export customer reference data tables
 * DESCRIPTION:		This SQL query extracts the relevant data from customer reference data tables in order
 *					that they can be submitted to Corelogic who will set up the mappings
 * HISTORY:			1.0  kt  22/5/2005	initial version
 */

	--EPISODE_TYPES
	f_convert_to_symbols(type)				"CODE"
	,context_flag		"CONTEXT"
	,f_convert_to_symbols(description)		"DESCRIPTION"
	,null				"SELECTION_COLUMN"
	,'EPISODE_TYPES'	"TABLE_NAME"
from EPISODE_TYPES

union

select 

	--OUTCOME_TYPES
	f_convert_to_symbols(outcome_type)		"CODE"
	,null				"CONTEXT"
	,f_convert_to_symbols(description)		"DESCRIPTION"
	,null				"SELECTION_COLUMN"
	,'OUTCOME_TYPES'	"TABLE_NAME"
from OUTCOME_TYPES

union

select 
	--SERVICES
	service_code		"CODE"
	,null				"CONTEXT"
	,description		"DESCRIPTION"
	,null				"SELECTION_COLUMN"
	,'SERVICES'			"TABLE_NAME"
from SERVICES

union

select 
	--SERVICE_TASKS
	to_char(id)			"CODE"
	,null				"CONTEXT"
	,description		"DESCRIPTION"
	,service_code		"SELECTION_COLUMN"
	,'SERVICE_TASKS'	"TABLE_NAME"
from SERVICE_TASKS

union

select 
	--LOOKED_AFTER_CODES
	code					"CODE"
	,null					"CONTEXT"
	,description			"DESCRIPTION"
	,code_type				"SELECTION_COLUMN"
	,'LOOKED_AFTER_CODES'	"TABLE_NAME"
from LOOKED_AFTER_CODES

union

select 
	--SERVICE_USER_GROUPS
	id						"CODE"
	,context_flag			"CONTEXT"
	,description			"DESCRIPTION"
	,null					"SELECTION_COLUMN"
	,'SERVICE_USER_GROUPS'	"TABLE_NAME"
from SERVICE_USER_GROUPS

union

select 
	--SERVICE_USER_SUB_GROUPS
	id						"CODE"
	,null					"CONTEXT"
	,description			"DESCRIPTION"
	,group_id				"SELECTION_COLUMN"
	,'SERVICE_USER_SUB_GROUPS'	"TABLE_NAME"
from SERVICE_USER_SUB_GROUPS

union

select 
	--REFERENCE_DATA
	ref_code				"CODE"
	,context_flag			"CONTEXT"
	,f_convert_to_symbols(ref_description)		"DESCRIPTION"
	,ref_domain				"SELECTION_COLUMN"
	,'REFERENCE_DATA'		"TABLE_NAME"
from REFERENCE_DATA

union

select 
	--TOPICS
	to_char(id)				"CODE"
	,context_flag			"CONTEXT"
	,f_convert_to_symbols(description)			"DESCRIPTION"
	,null					"SELECTION_COLUMN"
	,'TOPICS'				"TABLE_NAME"
from TOPICS

union

select 
	--QUESTIONS
	to_char(id)			"CODE"
	,null				"CONTEXT"
	,f_convert_to_symbols(question_text)			"DESCRIPTION"
	,answer_type		"SELECTION_COLUMN"
	,'QUESTIONS'		"TABLE_NAME"
from QUESTIONS

union

select 
	--ORGANISATIONS
	to_char(id)				"CODE"
	,null					"CONTEXT"
	,f_convert_to_symbols(name)			"DESCRIPTION"
	,null					"SELECTION_COLUMN"
	,'ORGANISATIONS'		"TABLE_NAME"
from ORGANISATIONS

union

select 
	--LA_ADDRESSES
	pcd					"CODE"
	,null				"CONTEXT"
	,pcd				"DESCRIPTION"
	,null				"SELECTION_COLUMN"
	,'LA_ADDRESSES'		"TABLE_NAME"
from LA_ADDRESSES

union

--QUESTION_ANSWERS
--
--leave this for now
--
select 
	--PROF_RELATIONSHIP_TYPES
	rel_type				"CODE"
	,null					"CONTEXT"
	,description			"DESCRIPTION"
	,null					"SELECTION_COLUMN"
	,'PROF_RELATIONSHIP_TYPES'		"TABLE_NAME"
from PROF_RELATIONSHIP_TYPES

union

select 
	--RELATIONSHIP_TYPES
	to_char(id)				"CODE"
	,null					"CONTEXT"
	,description			"DESCRIPTION"
	,relationship_code		"SELECTION_COLUMN"  -- needed for foreign key constraint
	,'RELATIONSHIP_TYPES'	"TABLE_NAME"
from RELATIONSHIP_TYPES

union

select 
	--FORM_TYPES
	to_char(id)			"CODE"
	,null				"CONTEXT"
	,description		"DESCRIPTION"
	,null				"SELECTION_COLUMN"
	,'FORM_TYPES'		"TABLE_NAME"
from FORM_TYPES

union

select 
	--QUESTIONNAIRES
	to_char(id)			"CODE"
	,null				"CONTEXT"
	,f_convert_to_symbols(description)		"DESCRIPTION"
	,null				"SELECTION_COLUMN"
	,'QUESTIONNAIRES'	"TABLE_NAME"
from QUESTIONNAIRES

union

select 
	--QUESTIONNAIRE_QUESTION
	to_char(qst_id)				"CODE"
	,null						"CONTEXT"
	,to_char(qnr_id)			"DESCRIPTION"
	,null						"SELECTION_COLUMN"
	,'QUESTIONNAIRE_QUESTION'	"TABLE_NAME"
from QUESTIONNAIRE_QUESTION
