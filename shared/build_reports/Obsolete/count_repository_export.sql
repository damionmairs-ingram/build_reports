select 

/*
 * NAME:		Count exported repository records
 * DESCRIPTION:	This report counts how many records from the various reports tables
 *				are included the export.
 *				It should be used to verify that all the relevant records have been exported in
 *				conjunction with checking the imu_reports table on the customer database
 * HISTORY:		1.0  kt  07/01/2005	intitial version
 *				1.1  kt  12/01/2005 amended for changes in get_all_repository_reports
 *
 */

	count(distinct rc1.id)	"Report Classes"
	,count(distinct r.id) 	"Reports"
	,count(distinct rp.id)	"Report Parameters"
	,count(distinct rs.id)	"Report Sections"

from 
	report_classes rc1

--JOIN: find reports
left outer join reports r
on r.report_class_id = rc1.id

--JOIN: find report_sections rs
left outer join report_sections rs
on rs.report_id = r.id

--JOIN: find report parmeters rp
left outer join report_parameters rp
on rp.report_id = r.id

--JOIN: find parent report class 
inner join report_classes rc2
on rc2.id = rc1.parent_class_id

--JOIN: find grandparent report class 
left outer join report_classes rc3
on rc3.id = rc2.parent_class_id

--JOIN: find great-grandparent report class 
left outer join report_classes rc4
on rc4.id = rc3.parent_class_id

where

	(
	--CRITERIA: Parent class of grandparent class is 'Frameworki Reporting Repository'
	rc3.parent_class_id = 1
	or
	--CRITERIA: Parent class of great-grandparent class is 'Frameworki Reporting Repository'
	rc4.parent_class_id = 1
	)