select 

/*
 * NAME:		Counts exported repository records loaded
 * DESCRIPTION:	This report counts how many records from various ID's have been imported into the customer
 *		database from IMU_Reports table in conjuction with count_repository_export.sql		
 *				
 * HISTORY:		1.0  SS  07/01/2005	intitial version
 *
 */


 count(Distinct RC_ID) "Report Classes" 
 ,count(Distinct R_ID) "Reports"
 ,count(Distinct RP_ID) "Report Parameters"
 count(Distinct RS_ID) "Report Sections"
 

from IMU_reports


