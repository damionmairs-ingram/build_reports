CREATE OR REPLACE FUNCTION f_convert_from_symbols (p_string varchar) RETURN VARCHAR IS

/*
 * NAME:		f_convert_from_symbols
 * DESCRIPTION:	This function replaces characters and formatting that were
 *				removed from various fields during the extract of reports
 *				from the Corelogic Reporting Repository in order to create a
 *				valid text file.
 *              The following codes are converted
 *                  chr(200)    chr(10)
 *                  chr(201)    chr(13)
 *                  chr(202)    chr(39)
 *                  chr(203)    chr(9)
 *                  chr(204)    chr(34)
 *                  chr(205)    chr(32)
 *					chr(206)	chr(38)
 *					chr(207)	chr(94)
 *					chr(208)	chr(96)
 * FUNCTIONS:
 * HISTORY:		1.0  kt  03/09/2004  initial version
 *              1.1  kt  23/11/2004  repository version
 *              1.2  kt  08/12/2004  added chr(32)
 *              1.3  kt  22/05/05  added chr(38), chr(94), chr(96)
 */

  v_return          VARCHAR2(4000) := p_string;

BEGIN

    v_return :=

		replace
			(
			replace
				(
				replace
					(
					replace
						(
						replace
							(
							replace
								(
								replace
									(
									replace
										(
										replace
											(
											v_return, chr(200), chr(10)
											)
										,chr(206), chr(38)
										)
									,chr(207), chr(94)
									)
								,chr(208), chr(96)
								)
							,chr(201), chr(13)
							)
						,chr(202), chr(39)
						)
					,chr(203), chr(9)
					)
				,chr(204), chr(34)
				)
			,chr(205), chr(32)
			);

  RETURN v_return;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      Null;
    WHEN OTHERS THEN
      Null;

END F_CONVERT_FROM_SYMBOLS;
/
