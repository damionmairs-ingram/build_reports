select 
	null									"RFM_ID"
	,null									"RFM_MAPPED_VALUE"
	,null									"RFM_REPORT_FILTER_ID"
	,rf.CONTEXT								"RF_CONTEXT"
	,f_convert_to_symbols(rf.DESCRIPTION)	"RF_DESCRIPTION"
	,rf.DISPLAY_COLUMN						"RF_DISPLAY_COLUMN"
	,case
		when rf.source_table not in 
			(
			'ITEMS'
			)
		then
			rf.form_page_number
	end										"RF_FORM_PAGE_NUMBER"
	,case
		when rf.source_table not in 
			(
			'ITEMS'
			)
		then
			rf.form_type_id
	end									"RF_FORM_TYPE_ID"
	,rf.id									"RF_ID"
	,rf.item_id								"RF_ITEM_ID"
	,rf.NAME								"RF_NAME"
	,case
		when rf.source_table not in
			(
			'QUESTIONS'
			)
		then rf.question_id
	end										"RF_QUESTION_ID"
	,case
		when rf.source_table not in
			(
			'QUESTIONS'
			)
		then rf.QUESTIONNAIRE_ID
	end										"RF_QUESTIONNAIRE_ID"
	,case
		when rf.source_table not in 
			(
			'FORM_TYPES'
			,'FORM_TYPES_ACTIVE'
			)
		then rf.SELECTION_COLUMN			
	end										"RF_SELECTION_COLUMN"
	,case
		when rf.source_table not in 
			(
			'FORM_TYPES'
			,'FORM_TYPES_ACTIVE'
			)
		then rf.SELECTION_VALUE				
	end 									"RF_SELECTION_VALUE"
	,rf.SOURCE_COLUMN						"RF_SOURCE_COLUMN"
	,rf.SOURCE_TABLE						"RF_SOURCE_TABLE"
from 
	report_filters rf
