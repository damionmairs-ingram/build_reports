		select
sum(
			case
				when len(substring(ris.sql_text,16001,4000)) > 0 then
					5 * 4000
				when len(substring(ris.sql_text,12001,4000)) > 0 then
					4 * 4000
				when len(substring(ris.sql_text, 8001,4000)) > 0 then
					3 * 4000
				when len(substring(ris.sql_text, 4001,4000)) > 0 then
					2 * 4000
				when len(substring(ris.sql_text,    1,4000)) > 0 then
					1 * 4000
			end)
		from   reports rep
		--JOIN: Find Instances
		inner join report_instances rin
		on rin.report_id = rep.id
		and rin.current_version = 'Y'
		and rin.database_type = 'ORACLE'
		--JOIN: Find Instance Sections
		inner join report_instance_sections ris
		on ris.report_instance_id = rin.id
